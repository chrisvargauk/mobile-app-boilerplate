define(['config',
        'websql',
        'Collection',
        'onsen',
        'CtrlApp',
        'pageManager',
        'directiveAlert',
        'directiveTime',
        'directiveNgSelect',
        'directiveProgressbar',
        'directiveSchedule',
        'directiveGetStructuredWidget',
        'directiveMiniReviewWidget',
        'directiveQuote',
        'directiveHabitStreak',
        'directiveProfileWidget',
        'directiveRewardPopup',
        'directiveStrength',
        'directiveStepDateAndTime',
        'directiveTrackTimeArchiveList',
        'directiveEstimate',

        'directiveManageTables',

        'profileService',
        'timeService',
        'settingService',
        'trackTimeService',

        'CtrlHome',
        'CtrlIntroduction',
        'CtrlListReason',
        'CtrlDefinition',
        'CtrlListStep',
        'CtrlListStepDetailed',
        'CtrlListTimeframe',
        'CtrlGoalDetail',
        'CtrlTrackRecord',
        'CtrlReview',
        'CtrlSetting',
        'CtrlAbout',
        'CtrlConceptIntroduction',
        'CtrlConceptReasons',
        'CtrlConceptRewards',
        'CtrlConceptSepofspace',
        'CtrlConceptSteps',
        'CtrlConceptTimeframe',
        'CtrlConceptComfortChallenge',
        'CtrlClassroom',
        'CtrlComfortChallenge',
        'CtrlComfortChallengeDetail',
        'CtrlNoteList',
        'CtrlNoteSuggestionList',
        'CtrlNoteAdd',
        'CtrlIntellectualProperty',
        'CtrlMain',
        'CtrlGetStructured',
        'CtrlMiniReview',
        'CtrlAchievement',
        'CtrlDev',
        'CtrlDaily',
        'CtrlLanding',
        'CtrlConceptHabitAndRoutine',
        'resolveReward',

        'listIdea',
        'listIdeaFull',
        'listComfortChallenge',
        'listConcept',

        'util',

        'ng',
        'ngSanitize',
        'underscore',
        'moment',
        'uiRouter',
        'deferredListCollection',
        'scenarioRunner'],
  function(config,
           websql,
           Collection,
           onsen,
           CtrlApp,
           pageManager,
           directiveAlert,
           directiveTime,
           directiveNgSelect,
           directiveProgressbar,
           directiveSchedule,
           directiveGetStructuredWidget,
           directiveMiniReviewWidget,
           directiveQuote,
           directiveHabitStreak,
           directiveProfileWidget,
           directiveRewardPopup,
           directiveStrength,
           directiveStepDateAndTime,
           directiveTrackTimeArchiveList,
           directiveEstimate,

           directiveManageTables,

           profileService,
           timeService,
           settingService,
           trackTimeService,

           CtrlHome,
           CtrlIntroduction,
           CtrlListReason,
           CtrlDefinition,
           CtrlListStep,
           CtrlListStepDetailed,
           CtrlListTimeframe,
           CtrlGoalDetail,
           CtrlTrackRecord,
           CtrlReview,
           CtrlSetting,
           CtrlAbout,
           CtrlConceptIntroduction,
           CtrlConceptReasons,
           CtrlConceptRewards,
           CtrlConceptSepofspace,
           CtrlConceptSteps,
           CtrlConceptTimeframe,
           CtrlConceptComfortChallenge,
           CtrlClassroom,
           CtrlComfortChallenge,
           CtrlComfortChallengeDetail,
           CtrlNoteList,
           CtrlNoteSuggestionList,
           CtrlNoteAdd,
           CtrlIntellectualProperty,
           CtrlMain,
           CtrlGetStructured,
           CtrlMiniReview,
           CtrlAchievement,
           CtrlDev,
           CtrlDaily,
           CtrlLanding,
           CtrlConceptHabitAndRoutine,
           resolveReward,

           listIdea,
           listIdeaFull,
           listComfortChallenge,
           listConcept,

           util,

           ng,
           ngSanitizee,
           underscore,
           momentjs,
           uiRouter,
           deferredListCollection,
           scenarioRunner) {

    app = angular.module('app', ['onsen', 'ui.router', 'ngSanitize']);

    app.factory('sharedDataAddNewGoal', [function () {
      return {};
    }]);

    window.resetApp = util.resetApp;

    app.directive('elastic', [
      '$timeout',
      function($timeout) {
        return {
          restrict: 'A',
          link: function($scope, element) {
            var resize = function() {
              return element[0].style.height = "" + element[0].scrollHeight + "px";
            };
            element.on("blur keyup change", resize);
            $timeout(resize, 0);
          }
        };
      }
    ]);

    app.run(function ($rootScope, $log) {
      $rootScope.$log = $log;
    });

    app.controller('CtrlApp', CtrlApp);
    app.factory('pageManager', pageManager);
    app.service('trackTimeService', trackTimeService);
    app.service('timeService', timeService);
    app.service('settingService', settingService);
    app.service('profileService', profileService);
    app.directive('alert', directiveAlert);
    app.directive('time', directiveTime);
    app.directive('ngSelect', directiveNgSelect);
    app.directive('progressbar', directiveProgressbar);
    app.directive('schedule', directiveSchedule);
    app.directive('getStructuredWidget', directiveGetStructuredWidget);
    app.directive('miniReviewWidget', directiveMiniReviewWidget);
    app.directive('quote', directiveQuote);
    app.directive('habitStreak', directiveHabitStreak);
    app.directive('profileWidget', directiveProfileWidget);
    app.directive('rewardPopup', directiveRewardPopup);
    app.directive('strength', directiveStrength);
    app.directive('stepDateAndTime', directiveStepDateAndTime);
    app.directive('trackTimeArchiveList', directiveTrackTimeArchiveList);
    app.directive('estimate', directiveEstimate);

    app.directive('manageTables', directiveManageTables);

    app.directive('contenteditable', ['$sce', function($sce) {
      return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModel) {
          if (!ngModel) return; // do nothing if no ng-model

          // Specify how UI should be updated
          ngModel.$render = function() {
            element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
          };

          // Listen for change events to enable binding
          element.on('blur keyup change', function() {
            scope.$apply(read);
          });
          read(); // initialize

          // Write data to the model
          function read() {
            var html = element.html();
            // When we clear the content editable the browser leaves a <br> behind
            // If strip-br attribute is provided then we strip this out
            if ( attrs.stripBr && html == '<br>' ) {
              html = '';
            }
            ngModel.$setViewValue(html);
          }
        }
      };
    }]);

    app.value('listIdea', listIdea);
    app.value('listIdeaFull', listIdeaFull);
    app.value('listComfortChallenge', listComfortChallenge);
    app.value('listConcept', listConcept);

    app.config(config);

    app.filter('range', function() {
      return function(input, total) {
          total = parseInt(total);

          for (var i=0; i<total; i++) {
              input.push(i);
          }

          return input;
      };
    });

    window.menuDisabled = false;
    window.highlightMenu = function(target) {
      // var btnData = {
      //   main: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   classroom: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   noteList: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   home: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   comfortChallenge: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   underlyingConcept: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   setting: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   },
      //   about: {
      //     isHighlighted: false,
      //     isDisabled: false
      //   }
      // };
      //
      // if (target === 'main' ||
      //   target === 'classroom' ||
      //   target === 'noteList' ||
      //   target === 'home' ||
      //   target === 'comfortChallenge' ||
      //   target === 'underlyingConcept' ||
      //   target === 'setting' ||
      //   target === 'about'
      // ) {
      //   //document.querySelectorAll('.navigation-bar--android__left')[1].style.backgroundColor = 'lightgreen';
      //   btnData[target].isHighlighted = true;
      //   window.isMenuIconFlashing = true;
      //   makeMenuIconFlashing();
      // } else {
      //   //document.querySelectorAll('.navigation-bar--android__left')[1].style.backgroundColor = '';
      //   window.isMenuIconFlashing = false;
      //   makeMenuIconFlashing('off');
      // }
      //
      // Object.keys(btnData).forEach(function(key) {
      //   var dTarget = document.querySelector('ons-sliding-menu ons-list-item.'+key);
      //   if (dTarget !== null && btnData[key].isHighlighted) {
      //     dTarget.style.color = 'lightgreen';
      //   } else if (dTarget !== null) {
      //     dTarget.style.color = 'white';
      //   }
      //
      //   if (!btnData[key].isHighlighted && target !== 'none') {
      //     document.querySelector('.disableBtn.'+key).classList.add('active');
      //   } else {
      //     document.querySelector('.disableBtn.'+key).classList.remove('active');
      //   }
      //
      //   document.querySelector('.disableBtn.'+key).addEventListener('click',
      //     function (evt) {
      //       evt.stopImmediatePropagation()
      //     }, false);
      // });
      //
      // if (target === 'disabled') {
      //   window.menuDisabled = true;
      // } else {
      //   window.menuDisabled = false;
      // }
    }

    window.makeMenuIconFlashing = function (command) {
      var dTarget = document.querySelector('.ion-navicon');

      if (command === 'off') {
        dTarget.classList.remove('light-up');
      }

      if (!window.isMenuIconFlashing)
        return;

      dTarget.classList.toggle('light-up');

      setTimeout(makeMenuIconFlashing, 500);
    }
  }
);