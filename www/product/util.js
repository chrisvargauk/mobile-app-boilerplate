define(function () {
    window.isItToday = function(timestamp) {
        var timestampDayStart = moment(moment( parseInt(timestamp ))
                .format('YYYY-MM-DD')+' 00:00')
                .format('x');

        var timestampTodayStart = moment(moment( parseInt(timeService.get().format('x') ))
                .format('YYYY-MM-DD')+' 00:00')
                .format('x');

        return timestampDayStart == timestampTodayStart;
    };

    (function() {
        /**
         * Decimal adjustment of a number.
         *
         * @param {String}  type  The type of adjustment.
         * @param {Number}  value The number.
         * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
         * @returns {Number} The adjusted value.
         */
        function decimalAdjust(type, value, exp) {
            // If the exp is undefined or zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // If the value is not a number or the exp is not an integer...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Shift
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        // Decimal round
        if (!Math.round10) {
            Math.round10 = function(value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }
        // Decimal floor
        if (!Math.floor10) {
            Math.floor10 = function(value, exp) {
                return decimalAdjust('floor', value, exp);
            };
        }
        // Decimal ceil
        if (!Math.ceil10) {
            Math.ceil10 = function(value, exp) {
                return decimalAdjust('ceil', value, exp);
            };
        }
    })();

    window.scrollToElement = function (idLocationStep) {
        var dContainer = document.querySelector('.page .content'),
          dContent = document.querySelector('.page .content .grid-whole');

        if (idLocationStep) {
            var dStep = document.querySelector('[id-location='+idLocationStep+']');
        }

        if (dStep) {
            var heightHeader = 44;
            dContainer.scrollTop = dContainer.scrollTop + parseInt(dStep.getBoundingClientRect().top) - heightHeader;
        } else {
            dContainer.scrollTop = dContent.offsetHeight;
        }
    };

    window.cursorTo = function (idLocation) {
        console.log('idLocation', idLocation);
        setTimeout(function () {
            document.querySelector('[id-location='+idLocation+']').focus();
        }, 0);
    };

    window.getWeekRepresentation = function (mUserGoal, timeService, cTrackRecord, timeFrame) {
        var timeFrame = timeFrame || 'oneHour';

        var listDay = [
            {
                label: 'M',
                value: 'Monday'
            },
            {
                label: 'T',
                value: 'Tuesday'
            },
            {
                label: 'W',
                value: 'Wednesday'
            },
            {
                label: 'T',
                value: 'Thursday'
            },
            {
                label: 'F',
                value: 'Friday'
            },
            {
                label: 'S',
                value: 'Saturday'
            },
            {
                label: 'S',
                value: 'Sunday'
            }
        ];

        var listDaySimple = _.pluck(listDay, 'value');

        var listDayAccomplished = makeListDayAccomplished(cTrackRecord, timeService);
        var listDayHourSpent = listDayAccomplished.listDayHourCtr;

        var getDayStyle = function (dayBreaf, timeService) {
            var listClass = [];

            // Mark up day if given day is scheduled
            if (isScheduled(dayBreaf.value)) {
                listClass.push('scheduled');
            }

            // Mark up day if given day is today
            if (isToday(dayBreaf.value)) {
                listClass.push('today');
            }

            listClass.push(isAccomplished(dayBreaf.value, timeService));

            return listClass;
        };

        function makeListDayAccomplished(cTrackRecord, timeService) {
            var listDayHourCtr = {},
              listDayAccomplished = [];

            cTrackRecord.JSON.forEach(function (mTrackRecord) {
                if (mTrackRecord.weekOfYear == timeService.get().isoWeek() &&
                  listDayAccomplished.indexOf(mTrackRecord.dayOfWeek) === -1
                ) {
                    listDayHourCtr[mTrackRecord.dayOfWeek] = listDayHourCtr[mTrackRecord.dayOfWeek] || 0;
                    var timeElapsedMillisec = parseInt(mTrackRecord.timeEnd) - parseInt(mTrackRecord.timeStart);
                    listDayHourCtr[mTrackRecord.dayOfWeek] += Math.round( timeElapsedMillisec / 1000);
                    listDayAccomplished.push(mTrackRecord.dayOfWeek);
                }
            });

            return {
                listDayHourCtr: listDayHourCtr,
                listDayAccomplished: listDayAccomplished
            };
        }

        function isScheduled(dayOfWeek) {
            return _.pluck(mUserGoal.cTimeAndLocation.JSON, 'day').indexOf(dayOfWeek) !== -1;
        }

        function isAccomplished(dayOfWeek, timeService) {
            var numberDayOfWeekIter = listDaySimple.indexOf(dayOfWeek),
              numberDayOfWeekCurrent = timeService.get().isoWeekday() - 1;

            // If day is not yet here
            if (numberDayOfWeekIter > numberDayOfWeekCurrent) {
                return 'coming';
            }

            var timeSpentHour = listDayHourSpent[dayOfWeek] / 3600;

            if (!timeSpentHour) {
                return 'failed';
            }
            
            // Compare time spent against on hour
            if (timeFrame === 'oneHour') {
                if (0 < timeSpentHour && timeSpentHour < 1) {
                    return 'in-progress';
                } else if (1 <= timeSpentHour) {
                    return 'done';
                }
                
            // Compare time spent against what scheduled for the day
            } else if(timeFrame === 'scheduled') {
                var scheduleForThedDay;
                mUserGoal.cTimeAndLocation.JSON.forEach(function (schedule) {
                    if (schedule.day === dayOfWeek) {
                        scheduleForThedDay = schedule;
                    }
                });

                // Calculate how many seconds are scheduled for the day if any
                if (scheduleForThedDay) {
                    var fromSecond = parseInt(scheduleForThedDay.fromHour) * 3600 + parseInt(scheduleForThedDay.fromMin) * 60;

                    var toSecond = parseInt(scheduleForThedDay.toHour) * 3600 + parseInt(scheduleForThedDay.toMin) * 60;

                    var scheduledSecond = toSecond - fromSecond;
                    var scheduledHour = scheduledSecond / 3600;

                    if (0 < timeSpentHour < scheduledHour) {
                        return 'in-progress';
                    } else if (scheduledHour <= timeSpentHour) {
                        return 'done';
                    }

                // If there is no time frame scheduled for the day but there is time spent on that day, mark it as done
                } else if (!scheduleForThedDay && timeSpentHour) {
                    return 'done';
                }
            }
        }

        function isToday(dayOfWeek) {
            var numberDayOfWeekIter = listDaySimple.indexOf(dayOfWeek),
              numberDayOfWeekCurrent = timeService.get().isoWeekday() - 1;

            return  numberDayOfWeekIter === numberDayOfWeekCurrent;
        }

        return listDay.map(function (dayBreaf) {
            return {
                className: getDayStyle(dayBreaf, timeService),
                dayNameShort: dayBreaf.label,
                dayName: dayBreaf.value
            }
        });
    };

    return {
       resetApp: function () {
           location.hash = "#/main";

           localStorage.userPropCredit = '0';
           websql.emptyTable('collectionType');
           websql.deleteTable('collectionType');
           websql.deleteTable('c_concept');
           websql.deleteTable('c_comfortChallenge');
           websql.deleteTable('c_userGoal');
           websql.deleteTable('c_trackRecord');
           websql.deleteTable('c_timeAndLocation');
           websql.deleteTable('c_epic');
           websql.deleteTable('c_step');
           websql.deleteTable('c_review');
           websql.deleteTable('c_reason');
           websql.deleteTable('c_note');
           websql.deleteTable('c_guide');
           websql.deleteTable('c_comfortChallenge');
           websql.deleteTable('c_getStructured');
           websql.deleteTable('c_dailyPriority');
           websql.deleteTable('c_rewardAndSkill');
           websql.deleteTable('c_achievement');
           websql.deleteTable('c_time');

           console.warn('Multiple tables are deleted from WebSQL. Reload the app NOW!');
       }
    }
});