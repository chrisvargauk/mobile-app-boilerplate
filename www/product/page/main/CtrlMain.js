var CtrlMain = function ($scope, pageManager, sharedDataAddNewGoal, cComfortChallenge, cGuide, cGetStructured, cDailyPriority, cNote, cUserGoal, time, listComfortChallenge, cTrackRecord) {
  pageManager.setTitle('Overview');

  window.cGuide = cGuide;
  window.scope = $scope;

  $scope.cNote = cNote;
  $scope.cUserGoal = cUserGoal;
  $scope.cComfortChallenge = cComfortChallenge;
  $scope.cGetStructured = cGetStructured;
  $scope.cDailyPriority = cDailyPriority;
  $scope.time = time;
  $scope.listComfortChallenge = listComfortChallenge;
  $scope.cTrackRecord = cTrackRecord;

  $scope.isDisabledBtnClassroom = false;
  $scope.isHighlitedBtnClassroom = false;
  $scope.isDisabledBtnIntellectualProperty = false;
  $scope.isHighlitedBtnIntellectualProperty = false;
  $scope.isDisabledBtnPlans = false;
  $scope.isHighlitedBtnPlans = false;
  $scope.isDisabledBtnComfortChallenge = false;
  $scope.isHighlitedBtnComfortChallenge = false;
  $scope.isDisabledBtnBack = false;
  $scope.isHighlitedBtnBack = false;
  $scope.isDisabledBtnTaskDone = false;
  $scope.isHighlitedBtnTaskDone = false;
  $scope.isDisabledBtnGetStructured = false;
  $scope.isHighlitedBtnGetStructured = false;
  $scope.isDisabledBtnIntellectualPropertyOpen = false;
  $scope.isHighlitedBtnIntellectualPropertyOpen = false;

  window.isMenuIconFlashing = false;

  localStorage.userPropCredit = localStorage.userPropCredit ? localStorage.userPropCredit : 0;
  $scope.userPropCredit = parseInt(localStorage.userPropCredit);
  $scope.creditManagement = {
    add: function (amount) {
      $scope.userPropCredit = $scope.userPropCredit + amount;
      localStorage.userPropCredit = $scope.userPropCredit;
    }
  };

  //var dateNow = moment();
  //var dateNow = moment().subtract(3, 'day');
  //var dateNow = moment().subtract(2, 'day');
  //var dateNow = moment().subtract(1, 'day');
  var dateNow = moment({hour: 9, minute: 1});

  $scope.getNumOfCompletedComfChallange = function () {
    var cComfortChallengeCompleted = cComfortChallenge.JSON.filter(function (mComfortChallenge) {
      return mComfortChallenge.status === 'completed';
    });

    return cComfortChallengeCompleted.length;
  };

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      history.go(-1);
    }
  };

  if (cGuide.JSON.length === 0) {
    cGuide.add({}, function (mGuide) {
      sharedDataAddNewGoal.guide = mGuide;
      sharedDataAddNewGoal.cGuide = cGuide;
      initGuide();
    });
  } else {
    sharedDataAddNewGoal.guide = cGuide.JSON[0];
    sharedDataAddNewGoal.cGuide = cGuide;
    initGuide();
  }

  function initGuide() {
    return;

    // todo: remove for prod
    //sharedDataAddNewGoal.guide.mainComfortChallenge = 'active';

  //   if (sharedDataAddNewGoal.guide.main) {
  //     $scope.isDisabledBtnClassroom = false;
  //     $scope.isHighlitedBtnClassroom = true;
  //     $scope.isDisabledBtnIntellectualProperty = true;
  //     $scope.isHighlitedBtnIntellectualProperty = false;
  //     $scope.isDisabledBtnPlans = true;
  //     $scope.isHighlitedBtnPlans = false;
  //     $scope.isDisabledBtnComfortChallenge = true;
  //     $scope.isHighlitedBtnComfortChallenge = false;
  //     $scope.isDisabledBtnBack = true;
  //     $scope.isHighlitedBtnBack = false;
  //     $scope.isDisabledBtnGetStructured = true;
  //     $scope.isHighlitedBtnGetStructured = false;
  //
  //     setTimeout(function () {
  //       $rootScope.$emit('alert', {
  //         isTutorial: true,
  //         title: 'Hub is where you start',
  //         text: 'It provides you with an overview of your progress. Take a look around here then tap on "Classroom" where you can learn about your first challenge.',
  //         type: 'simple'
  //       });
  //     }, 500);
  //
  //     sharedDataAddNewGoal.guide.main = false;
  //     sharedDataAddNewGoal.cGuide.check();
  //   }
  //
  //   if (sharedDataAddNewGoal.guide &&
  //       sharedDataAddNewGoal.guide.mainGetStructuredPreview) {
  //
  //     // Disable & Highlight buttons for tutorial
  //     highlightMenu('disabled');
  //     $scope.isDisabledBtnClassroom = false;
  //     $scope.isHighlitedBtnClassroom = true;
  //     $scope.isDisabledBtnIntellectualProperty = true;
  //     $scope.isHighlitedBtnIntellectualProperty = false;
  //     $scope.isDisabledBtnPlans = true;
  //     $scope.isHighlitedBtnPlans = false;
  //     $scope.isDisabledBtnComfortChallenge = true;
  //     $scope.isHighlitedBtnComfortChallenge = false;
  //     $scope.isDisabledBtnBack = true;
  //     $scope.isHighlitedBtnBack = false;
  //     $scope.isDisabledBtnTaskDone = true;
  //     $scope.isHighlitedBtnTaskDone = false;
  //     $scope.isDisabledBtnGetStructured = true;
  //     $scope.isHighlitedBtnGetStructured = false;
  //
  //     // Turn off completed Tut.
  //     sharedDataAddNewGoal.guide.mainGetStructuredPreview = false;
  //     // Turn on next Tut.
  //     sharedDataAddNewGoal.guide.classroomIntellectualProperty = true;
  //     // Save Tut. changes
  //     sharedDataAddNewGoal.cGuide.check();
  //
  //     setTimeout(function () {
  //       $rootScope.$emit('alert', {
  //         isTutorial: true,
  //         title: 'Daily Focus in Hub',
  //         text: 'Keep an eye on your "Most Important Task" and "What to Do Better Today" in the Hub. Also mark "Most Important Task" as complete when you are done with it. Take a look, then go back to "Classroom" when ready.',
  //         type: 'simple'
  //       });
  //     }, 500);
  //   }
  //
  //   if (sharedDataAddNewGoal.guide &&
  //       sharedDataAddNewGoal.guide.mainComfortChallenge === 'active') {
  //
  //     // Disable & Highlight buttons for tutorial
  //     highlightMenu('disabled')F;
  //     $scope.isDisabledBtnClassroom = false;
  //     $scope.isHighlitedBtnClassroom = true;
  //     $scope.isDisabledBtnIntellectualProperty = true;
  //     $scope.isHighlitedBtnIntellectualProperty = false;
  //     $scope.isDisabledBtnPlans = true;
  //     $scope.isHighlitedBtnPlans = false;
  //     $scope.isDisabledBtnComfortChallenge = true;
  //     $scope.isHighlitedBtnComfortChallenge = false;
  //     $scope.isDisabledBtnBack = true;
  //     $scope.isHighlitedBtnBack = false;
  //     $scope.isDisabledBtnTaskDone = true;
  //     $scope.isHighlitedBtnTaskDone = false;
  //     $scope.isDisabledBtnGetStructured = true;
  //     $scope.isHighlitedBtnGetStructured = false;
  //     $scope.isDisabledBtnIntellectualPropertyOpen = true;
  //     $scope.isHighlitedBtnIntellectualPropertyOpen = false;
  //
  //     // Turn off completed Tut.
  //     sharedDataAddNewGoal.guide.mainComfortChallenge = false;
  //     // Turn on next Tut.
  //     sharedDataAddNewGoal.guide.classroomComfortChallenge = true;
  //     // Save Tut. changes
  //     sharedDataAddNewGoal.cGuide.check();
  //
  //     setTimeout(function () {
  //       $rootScope.$emit('alert', {
  //         isTutorial: true,
  //         title: 'Ready for a Challenge?',
  //         text: 'Okay, we will have fun here :) Go to "Classroom" and select "Comfort Challenge" concept.',
  //         type: 'simple'
  //       });
  //     }, 500);
  //   }
  //
  //   if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.mainPlan) {
  //     highlightMenu('disabled');
  //
  //     $scope.isDisabledBtnClassroom = true;
  //     $scope.isHighlitedBtnClassroom = false;
  //     $scope.isDisabledBtnIntellectualProperty = true;
  //     $scope.isHighlitedBtnIntellectualProperty = false;
  //     $scope.isDisabledBtnPlans = false;
  //     $scope.isHighlitedBtnPlans = true;
  //     $scope.isDisabledBtnComfortChallenge = true;
  //     $scope.isHighlitedBtnComfortChallenge = false;
  //     $scope.isDisabledBtnBack = true;
  //     $scope.isHighlitedBtnBack = false;
  //     $scope.isDisabledBtnGetStructured = true;
  //     $scope.isHighlitedBtnGetStructured = false;
  //
  //     setTimeout(function () {
  //       $rootScope.$emit('alert', {
  //         isTutorial: true,
  //         title: 'Project..',
  //         text: 'Now tap on Project..',
  //         type: 'simple'
  //       });
  //     }, 500);
  //
  //     sharedDataAddNewGoal.guide.mainPlan = false;
  //     sharedDataAddNewGoal.cGuide.check();
  //   }
  }

  function hidrateDbIfNecesarry() {
    if (cComfortChallenge.JSON.length !== 0) {
      return;
    }
  }

  if (cComfortChallenge.JSON.length === 0) {
    cComfortChallenge.addArray($scope.listComfortChallenge);
  }

  $scope.goBack = function () {
    history.go(-1);
  };

  function init() {
    $rootScope.$emit('starting-app', {});
  }

  init();

  $scope.goToNoteDetail = function (idNote, condition) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    location.hash = '#/note-add/'+idNote;
  };

  $scope.goToComfortChallengeDetail = function (index) {
    location.hash = '#/comfort-challenge-detail/'+index;
  };

  $scope.listChallengeCurrent = $scope.cComfortChallenge.JSON.filter(function (mComfortChallenge) {
    return mComfortChallenge.status === 'started';
  });

  $scope.html2text = function (html) {
    return html.replace(/<\/?[^>]+(>|$)/g, " ").replace('&nbsp;', '');
  };

  $scope.getAge = function (mComfortChallenge) {
    var timestampDayStart = moment(moment( parseInt(mComfortChallenge.timestampStart) )
        .format('YYYY-MM-DD')+' 00:00')
      .format('x');

    var timestampTodayStart = moment(moment( parseInt($scope.time.get().format('x') ))
        .format('YYYY-MM-DD')+' 00:00')
      .format('x');

    return (parseInt(timestampTodayStart) - parseInt(timestampDayStart)) / (60*60*24*1000);
  };

  $scope.addNewNote = function (mComfortChallenge) {
    location.hash = '#/note-add-comfort-challenge-experience/'+mComfortChallenge.id+'/'+mComfortChallenge.name;
  };

  $scope.getProgress = function (mUserGoal) {
    var complexityDone = 0;
    var complecityTotal = 0;
    mUserGoal.cEpic.JSON.forEach(function (mEpic) {
      mEpic.cStep.JSON.forEach(function (mStep) {
        var comlexity = parseInt(mStep.complexity);
        if (mStep.status === 'done' &&
            comlexity !== -1) {
          complexityDone += comlexity;
        }

        if (comlexity !== -1) {
          complecityTotal += comlexity;
        }
      });
    });

    if (complecityTotal) {
      return Math.round(complexityDone / (complecityTotal / 100)) + '%';
    } else {
      return '0%';
    }
  };

  $scope.goToUserGoalDetail = function (indexUserGoal) {
    location.hash = '#/goal-details/'+indexUserGoal;
  };
};