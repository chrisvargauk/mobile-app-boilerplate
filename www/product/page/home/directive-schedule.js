define(function () {
  var directiveSchedule = function () {
    return {
      restrict: 'E',
      templateUrl: 'product/page/home/tmpl-schedule.html',
      scope: {
        mUserGoal: '=',
        cTrackRecord: '=',
        timeService: '='
      },
      controller: function ($scope) {
        $scope.getWeekRepresentation = getWeekRepresentation($scope.mUserGoal, $scope.timeService, $scope.cTrackRecord, 'scheduled');
      }
    };
  };

    return directiveSchedule;
});