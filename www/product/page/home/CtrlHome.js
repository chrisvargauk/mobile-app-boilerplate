var CtrlHome = function ($scope, cUserGoal, $rootScope, pageManager, cTrackRecord, timeService) {
  pageManager.setTitle('Projects');

  $rootScope.$emit('starting-app', {});

  $scope.timeService = timeService;
  $scope.model = {};
  $scope.model.cUserGoal = cUserGoal;
  $scope.model.cTrackRecord = cTrackRecord;

  //todo: delete for prod.
  window.cUserGoal = cUserGoal;

  highlightMenu('none');

  this.goToUserGoalDetail = function (indexUserGoal) {
    location.hash = '#/goal-details/'+indexUserGoal;
  };

  $scope.removeUserGoal = function (id) {
    $rootScope.$emit('alert', {
      title: 'DELETE',
      text: 'Do you really want to delete this project?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.model.cUserGoal.removeById('userGoal', id, function() {
          $scope.$apply(function () {});
        });
      }
    });
  };

  $scope.getProgress = function (mUserGoal) {
    var complexityDone = 0;
    var complecityTotal = 0;
    mUserGoal.cEpic.JSON.forEach(function (mEpic) {
      mEpic.cStep.JSON.forEach(function (mStep) {
        var comlexity = parseInt(mStep.complexity);
        if (mStep.status === 'done' &&
          comlexity !== -1) {
          complexityDone += comlexity;
        }

        if (comlexity !== -1) {
          complecityTotal += comlexity;
        }
      });
    });

    if (complecityTotal) {
      return Math.round(complexityDone / (complecityTotal / 100)) + '%';
    } else {
      return '0%';
    }
  };

  $scope.getCurrentStep = function (mUserGoal) {
    var allMStepList = [];
    mUserGoal.cEpic.JSON.forEach(function (mEpic) {
      allMStepList = allMStepList.concat(mEpic.cStep.JSON);
    });

    return allMStepList.filter(function (mStep) {
      return mStep.status === 'inProgress'
    })[0];
  };

  $scope.goToUserGoalDetail = function (indexUserGoal) {
    location.hash = '#/goal-details/'+indexUserGoal;
  };
};