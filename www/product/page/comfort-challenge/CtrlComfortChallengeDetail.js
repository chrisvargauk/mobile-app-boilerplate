var CtrlComfortChallengeDetail = function ($scope, $stateParams, pageManager, sharedDataAddNewGoal, cComfortChallenge, cNote, time) {
  pageManager.setTitle('Comfort Challenges Details');

  // If there is no comfort challenge in the database, go to main view, where database will be hydrated.
  if(cComfortChallenge.JSON.length === 0) {
    location.hash = '#/main';
    return;
  }

  $scope.cComfortChallenge = cComfortChallenge;
  $scope.cNote = cNote;
  $scope.$stateParams = $stateParams;

  //$scope.model.cUserGoal = cUserGoal;
  $scope.mComfortChallenge = $scope.cComfortChallenge.getById($stateParams.indexComfortChallenge);

  $scope.isDisabledItemFirst = false;
  $scope.isHighlitedItemFirst = false;

  document.querySelector('.desc').innerHTML = $scope.mComfortChallenge.description;

  $scope.acceptChallenge = function () {
    $scope.mComfortChallenge.status = 'started';
    $scope.mComfortChallenge.timestampStart = time.get().format('x');
    $scope.cComfortChallenge.check();
    location.hash = '#/comfort-challenge';
  };

  $scope.declineChallenge = function () {
    $scope.mComfortChallenge.status = 'declined';
    $scope.cComfortChallenge.check();
    location.hash = '#/comfort-challenge';
  };

  $scope.addNewNote = function () {
    location.hash = '#/note-add-comfort-to-challenge/new/'+$stateParams.indexComfortChallenge+'/'+$scope.mComfortChallenge.name;
  };

  // Open or Edit
  $scope.goToNoteDetail = function (idNote, condition) {
    location.hash = '#/note-add-comfort-to-challenge/'+idNote+'/'+$stateParams.indexComfortChallenge+'/'+$scope.mComfortChallenge.name;
  };

  // Show note only if its related to Comfort Challenge
  $scope.isNoteVisible = function (mNote) {
    // console.log(mNote.idComfortChallenge+' === '+$scope.mComfortChallenge.id);
    // console.log(mNote);
    return parseInt(mNote.idComfortChallenge) === parseInt($scope.mComfortChallenge.id);
  };

  $scope.isNoteListEmpty = function () {
    var isEmpty = true;
    $scope.cNote.JSON.forEach(function (mNote) {
      if ($scope.isNoteVisible(mNote)){
        isEmpty = false;
      }
    });
    console.log('isNoteListEmpty', isEmpty);
    return isEmpty;
  };

  $scope.completeChallenge = function (id) {
    $rootScope.$emit('alert', {
      title: 'COMPLETED',
      text: 'Have you completed the challenge?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.mComfortChallenge.status = 'completed';
        $scope.mComfortChallenge.timestampEnd = time.get().format('x');
        $scope.cComfortChallenge.check(function () {
          $rootScope.$emit('alert', {
            title: 'CAPTURE YOUR INSIGHTS',
            text: 'Go ahead and take a note on your experience completing this comfort challenge. It will be interesting to check back on this note when you complete the challenge second time. Just like with a good book, the experience matures by completing it again later on.',
            type: 'cancelOk',
            fnOk: function () {
              $scope.mComfortChallenge.status = 'completed';
              $scope.mComfortChallenge.timestampEnd = time.get().format('x');
              $scope.cComfortChallenge.check();
              location.hash = '#/comfort-challenge';
            }
          });
        });
      }
    });
  };

  //$scope.completeChallenge = function () {
  //  $scope.mComfortChallenge.status = 'completed';
  //  $scope.mComfortChallenge.timestampEnd = time.get().format('x');
  //  $scope.cComfortChallenge.check();
  //  location.hash = '#/comfort-challenge';
  //};

  $scope.html2text = function (html) {
    return html.replace(/<\/?[^>]+(>|$)/g, " ").replace('&nbsp;', '');
  };

  $scope.goBack = function () {
    //if (sharedDataAddNewGoal.guide) {
    //  sharedDataAddNewGoal.guide.comfortChallengeBack = true;
    //}

    history.go(-1);
  }
};