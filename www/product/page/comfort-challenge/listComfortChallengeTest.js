define(function () {
    return [
        {
          name: 'Current comfort challenge name',
          shortDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          timestampStart: Date.now(),
          timestampEnd: -1,
          status: 'started' // 'unlocked', 'started', 'declined', 'completed'
        },
        {
          name: 'Unlocked comfort challenge name',
          shortDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          timestampStart: -1,
          timestampEnd: -1,
          status: 'unlocked' // 'unlocked', 'started', 'declined', 'completed'
        },
        {
          name: 'Completed comfort challenge name',
          shortDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          timestampStart: Date.now(),
          timestampEnd: Date.now() + 10000,
          status: 'completed' // 'unlocked', 'started', 'declined', 'completed'
        },
        {
          name: 'Completed comfort challenge name',
          shortDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          timestampStart: Date.now(),
          timestampEnd: Date.now() + 10000,
          status: 'completed' // 'unlocked', 'started', 'declined', 'completed'
        },
        {
          name: 'Completed comfort challenge name',
          shortDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          timestampStart: Date.now(),
          timestampEnd: Date.now() + 10000,
          status: 'completed' // 'unlocked', 'started', 'declined', 'completed'
        },
        {
          name: 'Declined comfort challenge name',
          shortDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus, amet aperiam autem cupiditate distinctio, dolore impedit in libero maxime nam, neque nobis perferendis possimus quaerat reprehenderit temporibus unde voluptatibus.',
          timestampStart: -1,
          timestampEnd: -1,
          status: 'declined' // 'unlocked', 'started', 'declined', 'completed'
        }
    ];
});