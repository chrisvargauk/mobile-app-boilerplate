var CtrlComfortChallenge = function ($scope, pageManager, sharedDataAddNewGoal, cComfortChallenge, listComfortChallenge, time) {
  pageManager.setTitle('Comfort Challenges');

  $scope.time = time;
  $scope.listComfortChallenge = listComfortChallenge;

  // If there is no comfort challenge in the database database will be hydrated.
  if (cComfortChallenge.JSON.length === 0) {
    cComfortChallenge.addArray($scope.listComfortChallenge, function () {
      $scope.$apply(function () {});
    });
  }

  $scope.cComfortChallenge = cComfortChallenge;

  $scope.listChallengeCurrent = $scope.cComfortChallenge.JSON.filter(function (mComfortChallenge) {
    return mComfortChallenge.status === 'started';
  });

  $scope.listChallengeUnlocked = $scope.cComfortChallenge.JSON.filter(function (mComfortChallenge) {
    return mComfortChallenge.status === 'unlocked';
  });

  $scope.listChallengeCompleted = $scope.cComfortChallenge.JSON.filter(function (mComfortChallenge) {
    return mComfortChallenge.status === 'completed';
  });

  $scope.listChallengeDeclined = $scope.cComfortChallenge.JSON.filter(function (mComfortChallenge) {
    return mComfortChallenge.status === 'declined';
  });

  $scope.getAge = function (mComfortChallenge) {
    var timestampDayStart = moment(moment( parseInt(mComfortChallenge.timestampStart) )
            .format('YYYY-MM-DD')+' 00:00')
        .format('x');

    var timestampTodayStart = moment(moment( parseInt($scope.time.get().format('x') ))
            .format('YYYY-MM-DD')+' 00:00')
        .format('x');

    return (parseInt(timestampTodayStart) - parseInt(timestampDayStart)) / (60*60*24*1000);
  };

  $scope.complete = function (id) {
    $rootScope.$emit('alert', {
      title: 'Completed?',
      text: 'Have you completed the challenge?',
      type: 'cancelOk',
      fnOk: function () {
        //$scope.model.cUserGoal.removeById('userGoal', id, function() {
        //  $scope.$apply(function () {});
        //});
      }
    });
  };

  $scope.goToComfortChallengeDetail = function (index) {
    location.hash = '#/comfort-challenge-detail/'+index;
  };

  $scope.isDoneComfortChallenge = function (mComfortChallenge) {
    return parseInt(mComfortChallenge.duration) - parseInt($scope.getAge(mComfortChallenge)) <= 0;
  };

  cComfortChallenge.JSON.forEach(function (mComfortChallenge, index) {
    if (mComfortChallenge.status !== 'started') {
      return;
    }

    console.log('diff ('+index+'):' + $scope.getAge(mComfortChallenge));

    if ($scope.isDoneComfortChallenge(mComfortChallenge)) {
      var status = 'done';

      mComfortChallenge.status = 'completed';
      mComfortChallenge.timestampEnd = time.get().format('x');
      cComfortChallenge.check(function () {
        $rootScope.$emit('alert', {
          title: 'COMFORT CHALLENGE COMPLETED',
          text: 'Go ahead and take note of your experience completing this comfort challenge. It will be interesting to check back on this note when you complete the challenge for the second time. Just like with a good book, experience matures by completing it again later on.',
          type: 'cancelOk',
          fnOk: function () {
            location.hash = '#/note-add/new';
          }
        });

        setTimeout(function () {
          $rootScope.$emit('comfortChallenge.completed', {});
        }, 0);
      });
    }
  });

  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.comfortChallenge) {
  //
  //  highlightMenu('disabled');
  //
  //  $scope.isDisabledItemFirst = false;
  //  $scope.isHighlitedItemFirst = true;
  //  $scope.isDisabledBtnBack = true;
  //  $scope.isHighlitedBtnBack = false;
  //
  //  setTimeout(function () {
  //    $rootScope.$emit('alert', {
  //      isTutorial: true,
  //      title: 'List of Comfort Challenges',
  //      text: 'You have one unlocked Comfort Challenge for now. Later you will unlock more and more by developing other Entrepreneurial Skills.',
  //      type: 'simple'
  //    });
  //  }, 500);
  //
  //  sharedDataAddNewGoal.guide.comfortChallenge = false;
  //  sharedDataAddNewGoal.cGuide.check();
  //} else {
  //  highlightMenu('none');
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.comfortChallengeAccepted) {
  //  setTimeout(function () {
  //    $rootScope.$emit('alert', {
  //      isTutorial: true,
  //      title: 'Accepted! Nice!!',
  //      text: 'Im sure you will complete it. If you dont finish it with one go, no problem, take your time, the end result is what matters. Now let`s go back to Hub.',
  //      type: 'simple'
  //    });
  //  }, 500);
  //
  //  highlightMenu('main');
  //  sharedDataAddNewGoal.guide.comfortChallengeAccepted = false;
  //  sharedDataAddNewGoal.guide.mainPlan = true;
  //
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.comfortChallengeDeclined) {
  //  setTimeout(function () {
  //    $rootScope.$emit('alert', {
  //      isTutorial: true,
  //      title: 'No problem',
  //      text: 'You can come back to any challenge any time. Now let`s go back to Hub.',
  //      type: 'simple'
  //    });
  //  }, 500);
  //
  //  highlightMenu('main');
  //  sharedDataAddNewGoal.guide.comfortChallengeDeclined = false;
  //  sharedDataAddNewGoal.guide.mainPlan = true;
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.comfortChallengeCompleted) {
  //  setTimeout(function () {
  //    $rootScope.$emit('alert', {
  //      isTutorial: true,
  //      title: 'Good Job!',
  //      text: 'You will receive more challenges soon. Now let`s go back to Hub.',
  //      type: 'simple'
  //    });
  //  }, 500);
  //
  //  highlightMenu('main');
  //  sharedDataAddNewGoal.guide.comfortChallengeCompleted = false;
  //  sharedDataAddNewGoal.guide.mainPlan = true;
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //  sharedDataAddNewGoal.guide.comfortChallengeBack) {
  //  setTimeout(function () {
  //    $rootScope.$emit('alert', {
  //      isTutorial: true,
  //      title: 'No problem',
  //      text: 'You can come back to any challenge any time. Now let`s go back to Hub.',
  //      type: 'simple'
  //    });
  //  }, 500);
  //
  //  window.isMenuIconFlashing = false;
  //  makeMenuIconFlashing();
  //  highlightMenu('main');
  //  sharedDataAddNewGoal.guide.comfortChallengeBack = false;
  //  sharedDataAddNewGoal.guide.mainPlan = true;
  //}

  $scope.goBack = function () {
    history.go(-1);
  }
};