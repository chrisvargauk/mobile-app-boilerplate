define(function () {
  var directiveEstimate = function () {
    return {
      restrict: 'E',
      templateUrl: 'product/page/goal-detail/directive-estimate-tmpl.html',
      scope: {
        trackTimeService: '=',
        idUserGoal: '='
      },
      controller: function ($scope) {
        $scope.model = {
          velocity: '?',
          deadlineInHour: '?',
          costTotal: '0',
          costCompleted: '0'
        };

        function addZero(digit) {
          if (digit == 0) {
            return '00';
          } else if (digit < 10) {
            return '0'+digit;
          } else {
            return digit;
          }
        };

        $scope.formatTime = function(timeElapsedSec) {
          var timeElapsedMin, timeElapsedMinRem, timeElapsedSecRem, timeElapsedHour;
          timeElapsedMin = Math.floor((timeElapsedSec) / 60),
            timeElapsedMinRem = timeElapsedMin % 60,
            timeElapsedSecRem = (timeElapsedSec) % 60,
            timeElapsedHour = Math.floor(timeElapsedMin / 60);

          var timeElapsed = '';

          if (timeElapsedHour) {
            timeElapsed += addZero(timeElapsedHour);
          } else {
            timeElapsed += '00'
          }

          if (timeElapsedMinRem) {
            timeElapsed += ':' + addZero(timeElapsedMinRem);
          } else {
            timeElapsed += ':00'
          }

          timeElapsed += ':' + addZero(timeElapsedSecRem);

          return timeElapsed;
        };

        $scope.trackTimeService.getTotalComplexityOfCompleted($scope.idUserGoal, function (data) {
          // console.log('totalComplexityOfCompleted: ', data.totalComplexityOfCompleted);
          // console.log('totalSecondSpentOnCompleted: ', $scope.formatTime(data.totalSecondSpentOnCompleted));
          // console.log('velocityPerSecond: ', data.velocityPerSecond);
          // console.log('totalComplexityOfNotCompleted: ', data.totalComplexityOfNotCompleted);
          // console.log('deadlineInSecond: ', $scope.formatTime(data.deadlineInSecond));
          // console.log('Inhours: ', Math.round(data.deadlineInSecond/3600));

          if (data.velocityPerSecond) {
            $scope.model.velocity = Math.round10((data.velocityPerSecond * 3600), -2);
          }

          if (data.deadlineInSecond) {
            $scope.model.deadlineInHour = Math.round10((data.deadlineInSecond / 3600), -1);
          }

          if (data.costTotal) {
            $scope.model.costTotal = data.costTotal;
          }

          if (data.costCompleted) {
            $scope.model.costCompleted = data.costCompleted;
          }

          $scope.$apply(function () {});
        });
      }
    };
  };

  return directiveEstimate;
});