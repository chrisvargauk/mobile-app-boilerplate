var CtrlGoalDetail = function ($scope, $stateParams, cUserGoal, sharedDataAddNewGoal, pageManager, $rootScope, cNote, time, cTrackRecord, trackTimeService, $q) {
  console.log('Ctrl Goal Details is loaded');
  pageManager.setTitle('Project Details');
  $rootScope.$emit('goal-details-view', {});

  console.log('trackTimeService', trackTimeService);

  $scope.cNote = cNote;
  $scope.time = time;
  $scope.trackTimeService = trackTimeService;
  $scope.cTrackRecord = cTrackRecord;

  if(sharedDataAddNewGoal.goalJustCreated === true) {
    $rootScope.$emit('alert', {
      isTutorial: true,
      title: 'Project Created',
      type: 'simple',
      text: 'Think about these steps again—whether any of them can be broken down into multiple steps. If so, click on the step details <i class="fa fa-tasks"></i> icon and do it!',
      fnOk: function () {
        sharedDataAddNewGoal.goalJustCreated = false;
      }
    });
  }

  $scope.model = {
    done: false,
    inProgress: false
  };
  $scope.model.cUserGoal = cUserGoal;
  $scope.model.mUserGoal = cUserGoal.getById($stateParams.indexUserGoal);
  $scope.model.mEpicRoot = $scope.model.mUserGoal.cEpic.JSON[0];

  $scope.model.timeElapsedMStepInProgress = -1;

  sharedDataAddNewGoal.cUserGoal = $scope.model.cUserGoal;
  sharedDataAddNewGoal.mUserGoal = $scope.model.mUserGoal;

  $scope.listLabel = {
    startStop: 'Start'
  };

  $scope.listDaySimple = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
  ];
  
  $scope.allMStepList = [];
  $scope.allCStepList = [];
  $scope.model.mUserGoal.cEpic.JSON.forEach(function (mEpic) {
    $scope.allMStepList = $scope.allMStepList.concat(mEpic.cStep.JSON);
    $scope.allCStepList.push(mEpic.cStep);
  });

  $scope.weekRepresentation = getWeekRepresentation($scope.model.mUserGoal, $scope.time, $scope.cTrackRecord, 'scheduled')

  $scope.weekRepresentationObj = {};
  $scope.weekRepresentation.forEach(function (weekRepresentation) {
    $scope.weekRepresentationObj[weekRepresentation.dayName] = weekRepresentation;
  });

  $scope.schedule = {
    listDay: $scope.model.mUserGoal.cTimeAndLocation.JSON.map(function (mTimeAndLocation) {
      var dayRepresentation = {};
      Object.keys(mTimeAndLocation).forEach(function (key) {
        dayRepresentation[key] = mTimeAndLocation[key];
      });

      dayRepresentation.className = $scope.weekRepresentationObj[mTimeAndLocation.day].className;

      return dayRepresentation;
    })
  };

  function updateStepTimer(text, inProgress) {
    $scope.displayTimeElapsed.timeElapsed = text;
    $scope.displayTimeElapsed.inProgress = inProgress;
    $scope.$apply(function () {});
  }

  // Initialize
  function initTimeTracking() {
    var mStepInProgress = getStepInProgress();
    if (mStepInProgress) {
      trackTimeService.init(mStepInProgress, updateStepTimer);
    }
  };

  function compareAscending(a,b) {
    if (a.idOrder < b.idOrder)
      return -1;
    else if (a.idOrder > b.idOrder)
      return 1;
    else
      return 0;
  }

  function compareDescending(a,b) {
    if (a.idOrder < b.idOrder)
      return 1;
    else if (a.idOrder > b.idOrder)
      return -1;
    else
      return 0;
  }

  function getStepInProgress() {
    var mStepInProgress;
    $scope.model.mUserGoal.cEpic.JSON.forEach(function (mEpic) {
      mEpic.cStep.JSON.forEach(function (mStep) {
        if (mStep.status === 'inProgress') {
          mStepInProgress = mStep;
        }
      });
    });

    return mStepInProgress;
  }

  function createWrappedMStepList() {
    $scope.wrappedMStepList = [];
    var idSortWrappedMStepList = 0;
    $scope.model.mUserGoal.cEpic.JSON.sort(compareAscending).forEach(function (mEpic) {
      mEpic.cStep.JSON.sort(compareAscending).forEach(function (mStep) {
        $scope.wrappedMStepList.push({
          idOrder: idSortWrappedMStepList++,
          timestampDone: parseInt(mStep.timestampDone),
          mStep: mStep
        });
      });
    });
  }
  createWrappedMStepList();

  function createWrappedMToBeEstimatedStepList() {
    $scope.wrappedMToBeEstimatedStepList = [];
    var idSortWrappedMStepList = 0,
        ctrNumAllSteps = 0;
    $scope.model.mUserGoal.cEpic.JSON.sort(compareAscending).forEach(function (mEpic) {
      mEpic.cStep.JSON.sort(compareAscending).forEach(function (mStep) {
        ctrNumAllSteps++;

        if (parseInt(mStep.complexity) !== -1 || mStep.status !== 'backlog') {
          return;
        }

        $scope.wrappedMToBeEstimatedStepList.push({
          idOrder: idSortWrappedMStepList++,
          timestampDone: parseInt(mStep.timestampDone),
          mStep: mStep,
          cStep: mEpic.cStep,
        });
      });
    });

    // Highlight steps for estimation only if at least one of the steps has been estimated already
    if ($scope.wrappedMToBeEstimatedStepList.length === ctrNumAllSteps) {
      $scope.wrappedMToBeEstimatedStepList = [];
    }
  }
  createWrappedMToBeEstimatedStepList();

  function createWrappedMHighComplexityStepList() {
    $scope.wrappedHighComplexityMStepList = [];
    var idSortWrappedMStepList = 0,
      ctrNumAllSteps = 0;
    $scope.model.mUserGoal.cEpic.JSON.sort(compareAscending).forEach(function (mEpic) {
      mEpic.cStep.JSON.sort(compareAscending).forEach(function (mStep) {
        ctrNumAllSteps++;

        if (parseInt(mStep.complexity) !== 4 || mStep.status !== 'backlog') {
          return;
        }

        $scope.wrappedHighComplexityMStepList.push({
          idOrder: idSortWrappedMStepList++,
          timestampDone: parseInt(mStep.timestampDone),
          mStep: mStep,
          cStep: mEpic.cStep,
        });
      });
    });
  }
  createWrappedMHighComplexityStepList();

  $scope.setComplexity = function (mStep, complexity, cStep) {
    if (mStep.status !== 'backlog') {
      $rootScope.$emit('alert', {
        title: "CAN’T BE CHANGED!",
        type: 'simple',
        text: "Difficulty can’t be changed once you’ve started working on a step. <br><span class='reasoning'>It is important that you don’'t adjust it to get the estimated end date of the project right.</span>"
      });

      return;
    }

    if(complexity === 4) {
      $rootScope.$emit('alert', {
        isTutorial: true,
        title: 'High Difficulty',
        type: 'simple',
        text: 'When a step seems too difficult, try to break it down into smaller steps; then it’s easier to measure progress.'
      });
    } else {
      if ($scope.showPopupForComlexity) {
        $rootScope.$emit('alert', {
          isTutorial: true,
          title: 'Difficulty',
          type: 'simple',
          text: 'Estimate how hard it is to finish these steps. One star is easy; four is very hard.'
        });

        $scope.showPopupForComlexity = false;
      }
    }

    // Set complexity back to default if complexity one symbol is tapped twice
    if (complexity === 1 && parseInt(mStep.complexity) === complexity) {
      mStep.complexity = -1;
    } else {
      mStep.complexity = complexity;
    }

    cStep.check(createWrappedMToBeEstimatedStepList);
  };

  $scope.listStep = {
    isFolded: true,
    isVisibleFilter: function (listItem) {
      return listItem.mStep.status === 'backlog' || listItem.mStep.status === 'blocked'
    }
  };

  $scope.listStepInProgress = {
    isFolded: true,
    isVisibleFilter: function (listItem) {
      return listItem.status === 'inProgress'
    }
  };

  $scope.listNote = {
    isFolded: true,
    visibleItemLimit: 2,
    isVisibleFilter: function (mNote) {
      return parseInt(mNote.idUserGoal) === parseInt($scope.model.mUserGoal.id);
    }
  };

  $scope.listStepDone = {
    isFolded: true,
    visibleItemLimit: 1,
    isVisibleFilter: function (step) {
      return step.mStep.status === 'done';
    }
  };

  $scope.listStepEstimated = {
    isFolded: true,
    visibleItemLimit: 2,
    isVisibleFilter: function (step) {
      return parseInt(step.mStep.complexity) === -1;
    }
  };

  $scope.listStepHighComplexity = {
    isFolded: true,
    visibleItemLimit: 2,
    isVisibleFilter: function (step) {
      return parseInt(step.mStep.complexity) === 4 && step.mStep.status === 'backlog';
    }
  };

  $scope.listInspiration = {
    isFolded: true,
    isVisibleFilter: function (item) {
      return true;
    }
  };

  $scope.listReview = {
    isFolded: true,
    visibleItemLimit: 1,
    isVisibleFilter: function (item) {
      return true;
    }
  };

  $scope.hideTip = function () {
    $scope.model.mUserGoal.tipVisible = false;
    $scope.model.cUserGoal.check();
  };

  $scope.listDayAccomplished = makeListDayAccomplished();

  $scope.editUserGoal = function () {
    location.hash = '#/add-new-goal/definition/edit';
  };

  $scope.editUserGoalStep = function () {
    var cStep, mStep;
    $scope.allCStepList.forEach(function (cStepIter) {
      cStepIter.JSON.forEach(function (mStepIter) {
        if(mStepIter.status === 'inProgress') {
          cStep = cStepIter;
          mStep = mStepIter;
        }
      });
    });

    if (!mStep || !cStep) {
      location.hash = '#/add-new-goal/steps-detailed';
    } else {
      trackTimeService.check(mStep, cStep, undefined, function () {
        location.hash = '#/add-new-goal/steps-detailed';
      });
    }
  };

  $scope.goToReasons = function () {
    location.hash = '#/add-new-goal/reasons';
  };

  $scope.goToListTimeframe = function () {
    location.hash = '#/add-new-goal/timeframe';
  };

  $scope.goToTrackRecord = function (idUserGoal) {
    location.hash = '#/track-record/' + idUserGoal;
  };

  $scope.showGoToRewards = function () {
    // If there is a single reward set dont show button, otherwise show it.
    for (index in $scope.model.mEpicRoot.cStep.JSON) {
      var mStep = $scope.model.mEpicRoot.cStep.JSON[index];

      if (mStep.reward !== '') {
        return false;
      }
    }

    return true;
  };

  $scope.showCongrat = function (step) {
    if (!step.isDone || !step.reward) {
      return;
    }

    $rootScope.$emit('alert', {
      title: 'Good job!',
      text: 'Now reward yourself: ' + step.reward,
      fnOk: function () {
      }
    });
  };

  $scope.setStatusStep = function (step, status, scope) {
    if (status === 'inProgress') {
      // If you want to move from Backlog to In Progress but there is already a task in there
      if ($scope.wrappedMStepList.filter(function(stepWrapped){
        return stepWrapped.mStep.status === 'inProgress';
      }).length) {
        $rootScope.$emit('alert', {
          title: 'Already working on one!',
          type: 'simple',
          text: 'Finish that one first or move it back to the other steps.',
          fnOk: function () {
            sharedDataAddNewGoal.goalJustCreated = false;
          }
        });

        return;
      }

      if (parseInt(step.timestampStart) === -1) {
        step.timestampStart = $scope.time.get().format('x');
      }
    }

    if (status === 'done') {
      step.isDone = true;
      step.timestampDone = parseInt($scope.time.get().format('x'));
      // order Id has to be migrated to wrapper obj as well
      createWrappedMStepList();

      if (step.reward) {
        $rootScope.$emit('alert', {
          title: 'Good Job',
          text: 'Now reward yourself: ' + step.reward,
          fnOk: function () {
          }
        });
      }
    }

    var listOfDeferred = [];
    step.status = status;
    $scope.allCStepList.forEach(function (cStep) {
      var deferred = $q.defer();
      listOfDeferred.push(deferred.promise);
      cStep.check(function () {
        deferred.resolve();
      });
    });

    $q.all(listOfDeferred).then(function () {
      if (status === 'inProgress') {
        initTimeTracking();
        trackTimeService.start(step);
      }

      if (status === 'done') {
        trackTimeService.stop(step);
      }

      if (status === 'backlog' || status === 'blocked') {
        trackTimeService.stop(step);
      }

      // Update time elapsed on the step that status has changed
      trackTimeService.check(step, $scope.allCStepList, scope);
    });
  };

  $scope.addZero = function (digit) {
    if (digit < 10) {
      return '0'+digit;
    } else {
      return digit;
    }
  };

  function makeListDayAccomplished() {
    var listDayAccomplished = [];

    $scope.model.mUserGoal.cTrackRecord.JSON.forEach(function (record) {
      if (record.weekOfYear ==  dateNow().isoWeek() && // if current week
          listDayAccomplished.indexOf(record.dayOfWeek) === -1 // if not on the list already
      ) {
        listDayAccomplished.push(record.dayOfWeek);
      }
    });

    return listDayAccomplished;
  }

  function dateNow() {
//        return moment().subtract(6, 'days');
//        return moment().subtract(1, 'days');
//        return moment().add(4, 'days');

    return moment();
  }

  function getIsoDay() {
    return (dateNow().day() + 6) % 7;
  }

  // Check if it is Sunday and if it is that whether the review is done or not
  window.isItTimeForReview = function() {
    var isScheduled = false,
        today = time.get(),
        mReviewSceduled;

    // Check if it is sunday
    if (moment(today).day() !== 0) {
      var lastSunday = moment(parseInt(today.format('x'))).weekday(0);
      onSunday(lastSunday);
    } else {
      onSunday(today);
    }

    function onSunday(today) {
      // if Project was created today, dont bother
      if (isItToday($scope.model.mUserGoal.timestamp)) {
        return false;
      }

      // Check whether there is any review scheduled for this Sunday
      $scope.model.mUserGoal.cReview.JSON.forEach(function (mReview) {
//        console.log(mReview.scheduled);
//        console.log(moment(parseInt(mReview.scheduled)).format('WW'));

        if (moment(parseInt(mReview.scheduled)).format('WW') == today.format('WW')) {
//          console.log('scheduled');
          isScheduled = true;
          mReviewSceduled = mReview;
        }
      });

      // if review is scheduled, check whether its done or not
      if(isScheduled) {
        console.log('mReviewSceduled', mReviewSceduled);

        // If review is done than return false
        if (mReviewSceduled.isDone) {
          return false;

          // If scheduled review is not done yet then return true
        } else {
          $rootScope.$emit('alert', {
            title: 'Review',
            text: 'It\'s time to review your week!',
            type: 'cancelOk',
            fnOk: function () {
//              console.log('mReviewSceduled', mReviewSceduled);
              location.hash = '#/review/'+$scope.model.mUserGoal.id+'/'+mReviewSceduled.id;
            }
          });
          return true;
        }

        // if there is no scheduled review then generate one and report true
      } else {
        // if today is earlier date than goal was set, return without generating review
        if (parseInt(today.format('x')) < parseInt($scope.model.mUserGoal.timestamp)) {
          return false;
        }

        generateReview($scope.model.mUserGoal.id, today, function(mReviewSceduled) {
//          console.log('mReviewSceduled', mReviewSceduled);

          $rootScope.$emit('alert', {
            title: 'Review',
            text: 'It\'s time to review your week!',
            type: 'cancelOk',
            fnOk: function () {
//              console.log('mReviewSceduled', mReviewSceduled);
              location.hash = '#/review/'+$scope.model.mUserGoal.id+'/'+mReviewSceduled.id;
            }
          });
        });
        return true;
      }

      return true;
    }

    return 7 - moment(today).day();
  };

  window.generateReview = function (idUserGoal, today, callback) {
    console.log($scope.model.cUserGoal.getById(idUserGoal).cReview);

    var cReview = $scope.model.cUserGoal.getById(idUserGoal).cReview;

    cReview.add({
      scheduled: today.format('x')
    }, callback);
  };

  $scope.addNewNote = function () {
    location.hash = '#/note-add-plan/new/'+$scope.model.mUserGoal.id+'/'+$scope.model.mUserGoal.name;
  };

  $scope.unlinkNote = function (idNote) {
    $rootScope.$emit('alert', {
      title: 'REMOVE FROM LIST',
      text: 'Do you really want to pull this IP out from your project? It is still available in your notes even if it is removed here.',
      type: 'cancelOk',
      fnOk: function () {
        console.log(cNote.getById(idNote));
        var mNote = cNote.getById(idNote);
        mNote.idUserGoal = -1;
        cNote.check(function () {
          $scope.$apply(function () {});
        });
      }
    });
  };

  $scope.html2text = function (html) {
    return html.replace(/<\/?[^>]+(>|$)/g, " ").replace('&nbsp;', '');
  };

  // Show note only if its related to Plan
  $scope.isNoteVisible = function (mNote) {
    return parseInt(mNote.idUserGoal) === parseInt($scope.model.mUserGoal.id);
  };

  $scope.goToNoteDetail = function (idNote) {
    // sharedDataAddNewGoal.mNote = cNote.getById(idNote);
    location.hash = '#/note-add/'+idNote;
  };

  $scope.goToReviewDetail = function (idReview) {
    // sharedDataAddNewGoal.mNote = cNote.getById(idNote);
    location.hash = '#/review/'+$scope.model.mUserGoal.id+'/'+idReview;
  };

  $scope.formatDate = function (timestampStr) {
    return moment(parseInt(timestampStr)).format('ddd, MMMM Do');
  };

  function addZero(digit) {
    if (digit == 0) {
      return '00';
    } else if (digit < 10) {
      return '0'+digit;
    } else {
      return digit;
    }
  };

  $scope.formatTime = function(timeElapsedSec) {
    var timeElapsedMin, timeElapsedMinRem, timeElapsedSecRem, timeElapsedHour;
    timeElapsedMin = Math.floor((timeElapsedSec) / 60),
      timeElapsedMinRem = timeElapsedMin % 60,
      timeElapsedSecRem = (timeElapsedSec) % 60,
      timeElapsedHour = Math.floor(timeElapsedMin / 60);

    var timeElapsed = '';

    if (timeElapsedHour) {
      timeElapsed += addZero(timeElapsedHour);
    } else {
      timeElapsed += '00'
    }

    if (timeElapsedMinRem) {
      timeElapsed += ':' + addZero(timeElapsedMinRem);
    } else {
      timeElapsed += ':00'
    }

    timeElapsed += ':' + addZero(timeElapsedSecRem);

    return timeElapsed;
  };

  $scope.numOfVisibleStep = function () {
    return $scope.model.mEpicRoot.cStep.JSON.filter(function(step){
      return step.status === 'backlog' || step.status === 'blocked';
    }).length;
  };

  $scope.numOfVisibleNote = function () {
    return $scope.cNote.JSON.filter(function(mNote){
      return $scope.isNoteVisible(mNote);
    }).length;
  };

  $scope.isVisibleStepInProgress = function () {
    return $scope.allMStepList.filter(function(step){
      return step.status === 'inProgress'
    }).length;
  };

  $scope.isVisibleStepDone = function () {
    return $scope.model.mEpicRoot.cStep.JSON.filter(function(step){
      return step.status === 'done';
    }).length;
  };

  $scope.isCurrentItemVisible = function (step, index, list, indexNotUsed) {
    var isVisible = false;

    if (index === 0) {
      $scope.ctrNumOfVisibleStep = 0;
    }

    if (list.isVisibleFilter(step)) {
      $scope.ctrNumOfVisibleStep++;
      isVisible = true;
    }

    if (list.isFolded && $scope.ctrNumOfVisibleStep > (list.visibleItemLimit || 3) && !indexNotUsed) {
      isVisible = false;
    }

    return isVisible;
    // listStepFolded
  };

  $scope.numOfVisibleItem = function (collection, listSession) {
    // Use JSON if collection use array itself if not collection
    collection = typeof collection.JSON !== 'undefined' ? collection.JSON : collection;

    return collection.filter(function(listItem, index){
      return $scope.isCurrentItemVisible(listItem, index, listSession, true);
    }).length;
  };

  $scope.toggleAll = function (list) {
    console.log('Toggle');
    list.isFolded = !list.isFolded;
  };

  $scope.displayTimeElapsed = {
    timeElapsed: '00',
    inProgress: false,
    pauseResume: function (mStep) {
      if (this.inProgress) {
        trackTimeService.stop(mStep);
      } else {
        trackTimeService.start(mStep);
      }
    },
  };

  $scope.daysRemainingToReview = isItTimeForReview();
  initTimeTracking();
};