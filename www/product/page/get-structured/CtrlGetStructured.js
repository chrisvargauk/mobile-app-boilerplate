var CtrlGetStructured = function ($scope, $rootScope, pageManager, sharedDataAddNewGoal, cGetStructured, cDailyPriority, cNote, cUserGoal, time) {
  pageManager.setTitle('Daily Priority');

  window.scope = $scope;

  $scope.cDailyPriority = cDailyPriority;
  $scope.cNote = cNote;
  $scope.time = time;

  $scope.dailyPriorityLast = {
    task: '',
    reward: '',
    timestamp: $scope.time.get().format('x')
  };

  getLastDailyPriority();
  isTaskNotComplete ();
  autofocus();

  $scope.isVisibleTick = $scope.dailyPriorityLast.task !== '' && typeof $scope.dailyPriorityLast.task !== 'undefined';
  $scope.taskAndHistoryMixed = $scope.cDailyPriority.JSON;

  $scope.ListOfImportantStepByUserGoal = getListOfImportantStepByUserGoal(cUserGoal);

  function filterUserGoalScheduled(cUserGoal) {
    function isScheduled(mUserGoal, dayOfWeek) {
      return _.pluck(mUserGoal.cTimeAndLocation.JSON, 'day').indexOf(dayOfWeek) !== -1;
    }

    var listUserGoalScheduled = cUserGoal.JSON.filter(function (mUserGoal) {
      return isScheduled(mUserGoal, time.get().format('dddd'))
    });

    return listUserGoalScheduled;
  }

  function getListOfImportantStepByUserGoal(cUserGoal) {
    var listUserGoalScheduled = filterUserGoalScheduled(cUserGoal);

    // Show steps from all projects if nothing is scheduled for today
    if (!listUserGoalScheduled.length) {
      listUserGoalScheduled = cUserGoal.JSON;
    }

    var ListOfImportantStepByUserGoal = [];
    listUserGoalScheduled.forEach(function (mUserGoal) {
      var representationUserGoal = {
        name: mUserGoal.name,
      };

      var indexListOfListStepByPriority = {
        4: [],
        3: [],
        2: [],
        1: [],
      };
      mUserGoal.cEpic.JSON.forEach(function (mEpic) {
        mEpic.cStep.JSON.forEach(function (mStep) {
          if (mStep.status !== 'done' && mStep.status !== 'blocked') {
            indexListOfListStepByPriority[parseInt(mStep.priority)].push(mStep);
          }
        });
      });
      (function () {
        if (indexListOfListStepByPriority[4].length) {
          representationUserGoal.importatntStepList = indexListOfListStepByPriority[4];
          return;
        }

        if (indexListOfListStepByPriority[3].length) {
          representationUserGoal.importatntStepList = indexListOfListStepByPriority[3];
          return;
        }

        if (indexListOfListStepByPriority[2].length) {
          representationUserGoal.importatntStepList = indexListOfListStepByPriority[2];
          return;
        }

        if (indexListOfListStepByPriority[1].length) {
          representationUserGoal.importatntStepList = indexListOfListStepByPriority[1];
          return;
        }
      }());

      ListOfImportantStepByUserGoal.push(representationUserGoal);
    });

    return ListOfImportantStepByUserGoal;
  }

  function autofocus() {
    if ($scope.dailyPriorityLast.task !== '' &&
        typeof $scope.dailyPriorityLast.task !== 'undefined') {
      return;
    }

    setTimeout(function () {
      document.querySelector('textarea').focus();
    }, 500);
  }

  function isTaskNotComplete () {
    var isComplete = true;
    if (!isItToday($scope.dailyPriorityLast.timestamp) &&
        $scope.dailyPriorityLast.isDoneTask === false) {
      isComplete = false;
    }

    if(!isComplete) {
      setTimeout(function () {
        $rootScope.$emit('alert', {
          title: 'UNFINISHED TASK',
          text: 'You have an unfinished task. Tap on <span class="fa fa-check"></span> when it\'s done, then you can set a new one.',
          type: 'simple'
        });
      }, 500);
    }
  };

  $scope.isTaskDone = function() {
    return cDailyPriority.JSON[cDailyPriority.JSON.length-1].task !== '' &&
           cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask;
  };

  // Get last mDailyPriority id any
  function getLastDailyPriority() {
    // Dont show old task if task list is empty
    if(cDailyPriority.JSON.length === 0) {
      return;
    }

    // Dont show old task if last task is done && if last task was created not today
    if(cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask &&
       !isItToday(cDailyPriority.JSON[cDailyPriority.JSON.length-1].timestamp)) {
      return;
    }

    $scope.dailyPriorityLast.task = cDailyPriority.JSON[cDailyPriority.JSON.length-1].task;
    $scope.dailyPriorityLast.reward = cDailyPriority.JSON[cDailyPriority.JSON.length-1].reward;
    $scope.dailyPriorityLast.isDoneTask = cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask;
    $scope.dailyPriorityLast.timestamp = cDailyPriority.JSON[cDailyPriority.JSON.length-1].timestamp;
  }

  function clearTask() {
    // Clear Taks input only if finished task is not created today.
    if (isItToday($scope.dailyPriorityLast.timestamp)) {
      return;
    }

    $scope.dailyPriorityLast = {
      task: '',
      reward: '',
      timestamp: $scope.time.get().format('x')
    };

    $scope.isVisibleTick = false;
    scope.$apply(function() {});

    autofocus();
  }

  $scope.saveTask = function(dailyPriorityLast, form, condition) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    // If form is invalid
    if (!form.$valid) {
      return;
    }

    if ($scope.dailyPriorityLast.isDoneTask) {
      return;
    }

    if(cDailyPriority.JSON.length === 0 ||
       cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask) {

      var dailyPriorityToSave = {
        task: $scope.dailyPriorityLast.task,
        reward: $scope.dailyPriorityLast.reward,
        isDoneTask: $scope.dailyPriorityLast.isDoneTask,
        timestamp: $scope.dailyPriorityLast.timestamp
      };

      cDailyPriority.add(dailyPriorityToSave,
        function (mDailyPriority) {
          var randomNumber = Math.round(Math.random()*100),
              randomIndex = randomNumber % cGetStructured.JSON.length,
              lesson = cGetStructured.JSON.length !== 0 ? cGetStructured.JSON[randomIndex].fix : '';

          if (cGetStructured.JSON.length !== 0) {
            setTimeout(function () {
              $rootScope.$emit('alert', {
                title: 'Lesson Learned Reminder',
                text: lesson,
                type: 'simple',
                fnOk: function () {
                  history.go(-1);
                }
              });
            }, 500);
          } else {
            history.go(-1);
          }
      });
    } else if(cDailyPriority.JSON.length !== 0 &&
              !cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask) {

      var mDailyPriorityLast = cDailyPriority.JSON[cDailyPriority.JSON.length-1];

      mDailyPriorityLast.task = $scope.dailyPriorityLast.task;
      mDailyPriorityLast.reward = $scope.dailyPriorityLast.reward;
      mDailyPriorityLast.timestamp = $scope.dailyPriorityLast.timestamp;

      cDailyPriority.check(function (mDailyPriority) {
        history.go(-1);
      });
    }
  };

  $scope.taskDone = function (condition) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    // Do nothing if task is already done
    if ($scope.dailyPriorityLast.isDoneTask) {
      return;
    }

    var popupCheckTime = function () {
      if ($scope.time.get().format('HH') > 11) {
        setTimeout(function () {
          $rootScope.$emit('alert', {
            title: '<span class="fa fa-gears"></span> Improve your strategy',
            text: 'Next time try to get your most important task done before 11am.',
            type: 'simple',
            fnOk: function () {
              popupReward();
            }
          });
        }, 200);
      } else {
        popupReward();
      }
    };

    var popupReward = function () {
      if ($scope.dailyPriorityLast.reward !== '') {
        setTimeout(function () {
          $rootScope.$emit('alert', {
            title: 'Treat Yourself',
            text: $scope.dailyPriorityLast.reward,
            type: 'simple',
            fnOk: function () {
              $rootScope.$emit('task.completed', {});
              clearTask();
            }
          });
        }, 200);
      } else {
        $rootScope.$emit('task.completed', {});
        clearTask();
      }
    };

    $rootScope.$emit('alert', {
      title: 'Completed',
      text: 'Have you finished the task?',
      type: 'cancelOk',
      fnOk: function () {
        cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask = true;
        $scope.dailyPriorityLast.isDoneTask = true;
        $scope.cDailyPriority.check(function () {
          // Manually trigger the Habit Streak directive to rerender
          $rootScope.$emit('cDailyPriority.change', {});
        });

        popupCheckTime();
      }
    });
  };

  $scope.formatDay = function (timestamp, timeOfDay) {
    var formatStr = typeof timeOfDay === 'undefined' ? "dddd MMM DD" : "dddd ["+timeOfDay+"] MMM DD";
    return moment(parseInt(timestamp)).format(formatStr);
  };

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  $scope.remove = function (id) {
    //if ($scope.isDisabledBtnDeleteNote) {
    //  return;
    //}

    $rootScope.$emit('alert', {
      title: 'DELETE',
      text: 'Do you really want to delete this task?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.cDailyPriority.removeById('dailyPriority', id, function() {
          $scope.$apply(function () {});
        });
      }
    });
  };

  $scope.isVisibleHistory = function () {
    var isVisible = false;
    cDailyPriority.JSON.forEach(function (mDailyPriority) {
      if (mDailyPriority.isDoneTask === true) {
        isVisible = true;
      }
    });

    return isVisible;
  }
};