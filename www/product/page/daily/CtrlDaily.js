var CtrlDaily = function ($scope, pageManager, sharedDataAddNewGoal, cComfortChallenge, cGetStructured, cDailyPriority, cNote, cUserGoal, time, cTrackRecord) {
  pageManager.setTitle('MED');

  window.scope = $scope;

  $scope.cNote = cNote;
  $scope.cUserGoal = cUserGoal;
  $scope.cComfortChallenge = cComfortChallenge;
  $scope.cGetStructured = cGetStructured;
  $scope.cDailyPriority = cDailyPriority;
  $scope.time = time;

  $scope.isDoneDailyPriority =  typeof cDailyPriority.JSON[cDailyPriority.JSON.length-1] !== 'undefined' &&
                                isItToday(cDailyPriority.JSON[cDailyPriority.JSON.length-1].timestamp) &&
                                cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask;

  $scope.getStateDailyPriority = function () {
    if (typeof cDailyPriority.JSON[cDailyPriority.JSON.length-1] !== 'undefined' &&
        isItToday(cDailyPriority.JSON[cDailyPriority.JSON.length-1].timestamp) &&
        cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask) {
      return 'done';
    }

    if (typeof cDailyPriority.JSON[cDailyPriority.JSON.length-1] !== 'undefined' &&
        isItToday(cDailyPriority.JSON[cDailyPriority.JSON.length-1].timestamp) &&
        !cDailyPriority.JSON[cDailyPriority.JSON.length-1].isDoneTask) {
      return 'in-progress';
    }
  };

  $scope.isDoneGetStructured =  typeof cGetStructured.JSON[cGetStructured.JSON.length-1] !== 'undefined' &&
                                isItToday(cGetStructured.JSON[cGetStructured.JSON.length-1].timestamp);

  $rootScope.$emit('starting-app', {});

  $scope.isDoneNote =  function () {
    var isDone = false;

    var listTimestampCreated = $scope.cNote.JSON.forEach(function (mNote) {
      if(isItToday(mNote.timestamp)) {
        isDone = true;
      }
    });

    var listTimestampUpdated = $scope.cNote.JSON.forEach(function (mNote) {
      if(isItToday(mNote.timestampUpdate)) {
        isDone = true;
      }
    });

    return isDone;
  };

  $scope.isDoneComfortChallenge =  function () {
    var isDone = false;

    var listComfortChallenge = $scope.cComfortChallenge.JSON.forEach(function (mComfortChallenge) {
      if(mComfortChallenge.status === 'started') {
        isDone = true;
      }
    });

    return isDone;
  };

  $scope.getAge = function (mComfortChallenge) {
    var timestampDayStart = moment(moment( parseInt(mComfortChallenge.timestampStart) )
            .format('YYYY-MM-DD')+' 00:00')
            .format('x');

    var timestampTodayStart = moment(moment( parseInt($scope.time.get().format('x') ))
            .format('YYYY-MM-DD')+' 00:00')
            .format('x');

    return (parseInt(timestampTodayStart) - parseInt(timestampDayStart)) / (60*60*24*1000);
  };

  $scope.getStateUserGoal = function () {
    var listIdStep = [],
        timeSpentSecond = 0;

    cUserGoal.JSON.forEach(function (mUserGoal) {
      mUserGoal.cEpic.JSON.forEach(function (mEpic) {
        mEpic.cStep.JSON.forEach(function (mStep) {
          listIdStep.push(parseInt(mStep.id));
        });
      });
    });

    cTrackRecord.JSON.forEach(function (mTrackRecord) {
      if (listIdStep.indexOf(parseInt(mTrackRecord.idStep)) !== -1 &&
          isItToday(mTrackRecord.timeStart) &&
          mTrackRecord.timeEnd !== 'timestamp') {
        timeSpentSecond += Math.round( (parseInt(mTrackRecord.timeEnd) - parseInt(mTrackRecord.timeStart)) / 1000);
      }
    });

    var timeSpentHour = timeSpentSecond / 3600;

    if (0 < timeSpentHour && timeSpentHour < 1) {
      return 'in-progress';
    } else if (1 <= timeSpentHour) {
      return 'done';
    }
  };

  $scope.getStateComfortChallenge = function () {
    var status = 'unlocked';
    cComfortChallenge.JSON.forEach(function (mComfortChallenge) {
      if (mComfortChallenge.status === 'completed' &&
          isItToday(mComfortChallenge.timestampEnd)
      ) {
        status = 'done';
      }
    });

    cComfortChallenge.JSON.forEach(function (mComfortChallenge) {
      if (mComfortChallenge.status === 'started') {
        status = 'in-progress';
      }
    });

    cComfortChallenge.JSON.forEach(function (mComfortChallenge, index) {
      if (mComfortChallenge.status !== 'started') {
        return;
      }

      // console.log('diff ('+index+'):' + $scope.getAge(mComfortChallenge));

      if ($scope.getAge(mComfortChallenge) >= mComfortChallenge.duration) {
        console.log('Done :)');
        status = 'done';

        mComfortChallenge.status = 'completed';
        mComfortChallenge.timestampEnd = time.get().format('x');
        cComfortChallenge.check(function () {
          $rootScope.$emit('alert', {
            title: 'COMFORT CHALLENGE COMPLETED',
            text: 'Go ahead and take note of your experience completing this comfort challenge. It will be interesting to check back on your note when you complete the challenge for the second time. Just like with a good book, experience matures by completing it again later on.',
            type: 'cancelOk',
            fnOk: function () {
              location.hash = '#/note-add/new';
            }
          });

          setTimeout(function () {
            $rootScope.$emit('comfortChallenge.completed', {});
          }, 0);
        });
      }
    });

    return status;
  };
};