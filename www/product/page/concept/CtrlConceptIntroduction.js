var CtrlConceptIntroduction = function ($scope, pageManager) {
  pageManager.setTitle('Concepts | Introduction');

  $scope.goBack = function () {
    history.go(-1);
  }
};