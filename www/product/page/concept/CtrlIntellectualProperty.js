var CtrlIntellectualProperty = function ($scope, pageManager, sharedDataAddNewGoal) {
  pageManager.setTitle('Concepts | Intellectual Property');

  $scope.isDisabledBtnTakeAction = false;
  $scope.isHighlitedBtnTakeAction = false;
  $scope.isDisabledBtnBack = false;
  $scope.isHighlitedBtnBack = false;

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  // if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.conceptIntellectualProperty) {
  //
  //   $scope.isDisabledBtnTakeAction = false;
  //   $scope.isHighlitedBtnTakeAction = true;
  //   $scope.isDisabledBtnBack = true;
  //   $scope.isHighlitedBtnBack = false;
  //
  //   setTimeout(function () {
  //     $rootScope.$emit('alert', {
  //       isTutorial: true,
  //       title: 'This is how you start off.',
  //       text: 'The First Steps towards having a product to sell. Always Tap on "Take Action" button when you are done reading.',
  //       type: 'simple'
  //     });
  //   }, 500);
  //
  //   sharedDataAddNewGoal.guide.conceptIntellectualProperty = false;
  //   sharedDataAddNewGoal.cGuide.check();
  // }

  $scope.goBack = function () {
    history.go(-1);
  }
};