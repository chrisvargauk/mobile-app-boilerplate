var CtrlConceptHabitAndRoutine = function ($scope, pageManager, sharedDataAddNewGoal) {
  pageManager.setTitle('Concepts | Habits & Daily Routine');

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  // if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.conceptHabitAndRoutine) {
  //
  //   // Disable & Highlight buttons for tutorial
  //   highlightMenu('disabled');
  //   $scope.isDisabledBtnTakeAction = false;
  //   $scope.isHighlitedBtnTakeAction = true;
  //   $scope.isDisabledBtnBack = true;
  //   $scope.isHighlitedBtnBack = false;
  //
  //   // Turn off completed Tut.
  //   sharedDataAddNewGoal.guide.conceptHabitAndRoutine = false;
  //   // Turn on next Tut.
  //   sharedDataAddNewGoal.guide.getStructuredMain = true;
  //   // Save Tut. changes
  //   sharedDataAddNewGoal.cGuide.check();
  //
  //   setTimeout(function () {
  //     $rootScope.$emit('alert', {
  //       isTutorial: true,
  //       title: 'Understand the Concepts First',
  //       text: 'There is always a bit of fun read before I ask you to do anything. Don\'t forget though, knowledge without action is worthless! That\'s said, tap on "Take Action" button when you are done with reading.',
  //       type: 'simple'
  //     });
  //   }, 500);
  // }

  $scope.goBack = function () {
    history.go(-1);
  }
};