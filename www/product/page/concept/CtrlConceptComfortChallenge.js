var CtrlConceptComfortChallenge = function ($scope, pageManager, sharedDataAddNewGoal) {
  pageManager.setTitle('Concepts | ComfortChallenge');

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  // if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.conceptComfortChallenge) {
  //
  //   highlightMenu('disabled');
  //
  //   $scope.isDisabledBtnTakeAction = false;
  //   $scope.isHighlitedBtnTakeAction = true;
  //   $scope.isDisabledBtnBack = true;
  //   $scope.isHighlitedBtnBack = false;
  //
  //   setTimeout(function () {
  //     $rootScope.$emit('alert', {
  //       isTutorial: true,
  //       title: 'Comfort is the enemy',
  //       text: 'Time to learn about one of your main obstacles and what to do about it. As always, "Take Action" when you are done reading!',
  //       type: 'simple'
  //     });
  //   }, 500);
  //
  //   sharedDataAddNewGoal.guide.conceptComfortChallenge = false;
  //   sharedDataAddNewGoal.guide.mainComfortChallenge = false;
  //   sharedDataAddNewGoal.guide.comfortChallenge = true;
  //   sharedDataAddNewGoal.cGuide.check();
  // }

  $scope.goBack = function () {
    history.go(-1);
  }
};