var CtrlMiniReview = function ($scope, $rootScope, pageManager, sharedDataAddNewGoal, cGetStructured, cDailyPriority, cNote, time) {
  pageManager.setTitle('Mini Review');

  $scope.cGetStructured = cGetStructured;
  $scope.cNote = cNote;
  $scope.time = time;

  $scope.dailyPriorityLast = {
    task: '',
    reward: '',
    timestamp: $scope.time.get().format('x')
  };

  $scope.dailyFixLast = {
    appreciation: '',
    problem: '',
    fix: '',
    timestamp: $scope.time.get().format('x')
  };

  getLastDailyFix();

  $scope.taskAndHistoryMixed = $scope.cGetStructured.JSON;

  // Get last mGetStructured id any
  function getLastDailyFix() {
    // Load last cGetStructured if list is NOT empty and the last element is NOT done
    if(cGetStructured.JSON.length !== 0 &&
        isItToday(cGetStructured.JSON[cGetStructured.JSON.length-1].timestamp)) {
      $scope.dailyFixLast.appreciation = cGetStructured.JSON[cGetStructured.JSON.length-1].appreciation;
      $scope.dailyFixLast.problem = cGetStructured.JSON[cGetStructured.JSON.length-1].problem;
      $scope.dailyFixLast.fix = cGetStructured.JSON[cGetStructured.JSON.length-1].fix;
      $scope.dailyFixLast.isDoneTask = cGetStructured.JSON[cGetStructured.JSON.length-1].isDoneTask;
      $scope.dailyFixLast.timestamp = cGetStructured.JSON[cGetStructured.JSON.length-1].timestamp;
    }
  }

  $scope.saveFix = function(formDailyFix, form, condition) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    // If form is invalid
    if (!form.$valid) {
      return;
    }

    // If new Daily fix will be added (first fix ever || no fix for today yet)
    if(cGetStructured.JSON.length === 0 ||
        !isItToday(cGetStructured.JSON[cGetStructured.JSON.length-1].timestamp)) {

      var dailyFixToSave = {
        appreciation: $scope.dailyFixLast.appreciation,
        problem:      $scope.dailyFixLast.problem,
        fix:          $scope.dailyFixLast.fix,
        isDoneTask:   $scope.dailyFixLast.isDoneTask,
        timestamp:    $scope.dailyFixLast.timestamp
      };

      cGetStructured.add(dailyFixToSave, function (mDailyFix) {
        $rootScope.$emit('fix.completed', {});
        history.go(-1);
      });

    // If Daily fix will be updated (you already have a fix saved for today)
    } else if(cGetStructured.JSON.length !== 0 &&
        isItToday(cGetStructured.JSON[cGetStructured.JSON.length-1].timestamp)) {

      var mDailyFixLast = cGetStructured.JSON[cGetStructured.JSON.length-1];

      mDailyFixLast.appreciation =  $scope.dailyFixLast.appreciation;
      mDailyFixLast.problem =       $scope.dailyFixLast.problem;
      mDailyFixLast.fix =           $scope.dailyFixLast.fix;
      mDailyFixLast.isDoneTask =    $scope.dailyFixLast.isDoneTask;
      mDailyFixLast.timestamp =     $scope.dailyFixLast.timestamp;

      cGetStructured.check(function (mDailyFix) {
        history.go(-1);
      });
    }
  };

  $scope.formatDay = function (timestamp, timeOfDay) {
    var formatStr = typeof timeOfDay === 'undefined' ? "dddd MMM DD" : "dddd ["+timeOfDay+"] MMM DD";
    return moment(parseInt(timestamp)).format(formatStr);
  };

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  $scope.remove = function (id) {
    //if ($scope.isDisabledBtnDeleteNote) {
    //  return;
    //}

    $rootScope.$emit('alert', {
      title: 'DELETE',
      text: 'Do you really want to delete this review?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.cGetStructured.removeById('getStructured', id, function() {
          $scope.$apply(function () {});
        });
      }
    });
  };
};