define(function () {
  var directiveStepDateAndTimeSpent = function () {
    return {
      restrict: 'E',
      templateUrl: 'product/page/add-new-goal/partial/directive-step-date-and-time-spent-tmpl.html',
      scope: {
        mStep: '=',
        cStep: '=',
        trackTimeService: '='
      },
      controller: function ($scope) {
        $scope.init = function () {
          $scope.clockText = $scope.mStep.timeElapsedSecond;

          $rootScope.$on('directiveStepDateAndTimeSpent.set', function(evt, msg) {
            if ($scope.mStep.id == msg.idMStep) {
              $scope.clockText = msg.timeElapsed;
              $scope.$apply(function() {});
            }
          });

          $rootScope.$on('trackTimeService.timerStarted', function(evt, msg) {
            if ($scope.mStep.id == msg.idMStep) {
              $scope.currentListCTrackRecord = $scope.trackTimeService.getCurrentListCTrackRecord($scope.mStep);
              $scope.$apply(function () {});
              $scope.$parent.isVisible = false;
              $scope.$parent.$apply(function () {});
            }
          });
        };

        $scope.formatDate = function (timestampStr) {
          return moment(parseInt(timestampStr)).format('ddd, MMMM Do');
        };
        
        $scope.isVisibleArchiveIcon = function () {
          $scope.currentListCTrackRecord = $scope.trackTimeService.getCurrentListCTrackRecord($scope.mStep);
          var length = $scope.currentListCTrackRecord.length;
          return $scope.currentListCTrackRecord.length > 0 && $scope.currentListCTrackRecord[length-1].timeEnd !== 'timestamp';
        };

        $scope.isTracking = function () {
          return  $scope.trackTimeService.getIDStepInProgress() === $scope.mStep.id &&
                  $scope.trackTimeService.isTracking();
        };

        $scope.pauseResume = function () {
          // If the tracked step in Track Time Service is the current one
          if ($scope.isTracking()) {
            $scope.trackTimeService.stop($scope.mStep);
            $scope.trackTimeService.check($scope.mStep, $scope.cStep, $scope);
          } else if ($scope.mStep.status === 'inProgress') {
            $scope.trackTimeService.start($scope.mStep);
            $scope.trackTimeService.check($scope.mStep, $scope.cStep, $scope);
          }
        };

        function addZero(digit) {
          if (digit == 0) {
            return '00';
          } else if (digit < 10) {
            return '0'+digit;
          } else {
            return digit;
          }
        };

        $scope.formatTime = function(timeElapsedSec) {
          if (parseInt(timeElapsedSec) === -1) {
            return '00:00:00';
          }

          var timeElapsedMin, timeElapsedMinRem, timeElapsedSecRem, timeElapsedHour;
          timeElapsedMin = Math.floor((timeElapsedSec) / 60),
            timeElapsedMinRem = timeElapsedMin % 60,
            timeElapsedSecRem = (timeElapsedSec) % 60,
            timeElapsedHour = Math.floor(timeElapsedMin / 60);

          var timeElapsed = '';

          if (timeElapsedHour) {
            timeElapsed += addZero(timeElapsedHour);
          } else {
            timeElapsed += '00'
          }

          if (timeElapsedMinRem) {
            timeElapsed += ':' + addZero(timeElapsedMinRem);
          } else {
            timeElapsed += ':00'
          }

          timeElapsed += ':' + addZero(timeElapsedSecRem);

          return timeElapsed;
        }

        $scope.init();
      }
    };
  };

  return directiveStepDateAndTimeSpent;
});