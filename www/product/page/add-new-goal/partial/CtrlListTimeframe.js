var CtrlListTimeframe = function ($scope, cUserGoal, sharedDataAddNewGoal, pageManager) {
  var init = function () {
    pageManager.setTitle('Time Allocation');

    if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
      $rootScope.$emit('alert', {
        title: 'No project selected',
        text: 'Please create or select a project than you can allocate Time and Place.',
        type: 'simple',
        fnOk: function () {
          location.hash = '#/home';
        }
      });

      return;
    }

    $scope.model = {};
    $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
    $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;
//    $scope.model.cUserGoal = cUserGoal;
//    $scope.model.mUserGoal = cUserGoal.getById(1);
    $scope.model.cTimeAndLocation = $scope.model.mUserGoal.cTimeAndLocation;

    if ($scope.model.cTimeAndLocation.JSON.length === 0) {
      $scope.addEmptyTimeAndLocation();
    }

  };

  $scope.removeTimeAndLocation = function (id) {
    $scope.model.cTimeAndLocation.removeById('timeAndLocation', id, function() {
      $scope.$apply(function () {});
    })
  };

  $scope.addEmptyTimeAndLocation = function () {
    $scope.model.cTimeAndLocation.add({
      day: 'Monday',
      fromHour: '9',
      fromMin: '0',
      toHour: '10',
      toMin: '0',
      location: ''
    }, function () {
      $scope.$apply(function () {});
    });
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  init();
};