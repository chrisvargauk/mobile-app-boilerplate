var CtrlListStepDetailed = function ($scope, sharedDataAddNewGoal, pageManager, cStep, time, trackTimeService, $q, setting) {
  function compareAscending(a,b) {
    if (a.idOrder < b.idOrder)
      return -1;
    else if (a.idOrder > b.idOrder)
      return 1;
    else
      return 0;
  }

  function compareDescending(a,b) {
    if (a.idOrder < b.idOrder)
      return 1;
    else if (a.idOrder > b.idOrder)
      return -1;
    else
      return 0;
  }

  function compareId(a,b) {
    if (a.id < b.id)
      return -1;
    else if (a.id > b.id)
      return 1;
    else
      return 0;
  }

  var init = function (dontUpdateUserGoal) {
    pageManager.setTitle('Steps');

    if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
      location.hash = '#/add-new-goal/introduction';
      return;
    }

    $scope.time = time;
    $scope.trackTimeService = trackTimeService;
    if (!dontUpdateUserGoal) {
      $scope.model = {};
      $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
      $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;
      $scope.model.cStep = cStep;
    }

    $scope.model.mEpicRoot = $scope.model.mUserGoal.cEpic.JSON[0];
    
    $scope.epicToSelectAsDestianation = true;
    $scope.stepToMove = {
      listMStepWrapped: []
    };

    $scope.showPopupForReward = true;
    $scope.showPopupForComlexity = true;
    $scope.showPopupForPriority = true;

    $scope.model.mStepListAllInMUserGoal = [];
    $scope.model.cStepListAllInMUserGoal = [];
    $scope.model.mUserGoal.cEpic.JSON.forEach(function (mEpic) {
      mEpic.cStep.JSON.forEach(function (mStep) {
        // collect up all steps for further use
        $scope.model.mStepListAllInMUserGoal.push(mStep);

        // Don't show reward popup if user obviously used it. (for given User Goal only)
        if(mStep.reward !== '') {
          $scope.showPopupForReward = false;
        }

        if(parseInt(mStep.complexity) !== -1) {
          $scope.showPopupForComlexity = false;
        }

        if(parseInt(mStep.priority) !== 1) {
          $scope.showPopupForPriority = false;
        }
      });
      // collect up all step collection for further use
      $scope.model.cStepListAllInMUserGoal.push(mEpic.cStep);
    });

    $scope.isVisibleStepDone = setting.valueOf('stepDetailedIsVisibleStepDone');
    $scope.sortOrder = setting.valueOf('stepDetailedSortOrder');

    $scope.$apply(function () {});
  };

  $scope.removeStep = function (id, mEpic) {
    $rootScope.$emit('alert', {
      title: 'DELETE STEP',
      text: 'Are you sure you want to delete this Step?',
      type: 'cancelOk',
      fnOk: function () {
        mEpic = mEpic || $scope.model.mEpicRoot;
        trackTimeService.removeTrackRecordByIdStep(id, function () {
          mEpic.cStep.removeById('step', id, function() {
            $scope.$apply(function () {});
          })
        });
      }
    });
  };

  $scope.removeEpic = function (id) {
    $rootScope.$emit('alert', {
      title: 'DELETE EPIC?',
      text: 'Are you sure you want to delete this Epic?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.model.mUserGoal.cEpic.removeById('epic', id, function() {
          $scope.$apply(function () {});
        })
      }
    });
  };

  $scope.getNextIdOrder = function (cStep) {
    var idOrder = 0;
    cStep.JSON.forEach(function (mStep) {
      if (parseInt(mStep.idOrder) >= idOrder) {
        idOrder = parseInt(mStep.idOrder) + 1;
      }
    });

    return idOrder;
  };

  $scope.addEmptyStep = function (mEpic, scope, idOrderStepSelected, position) {
    mEpic = mEpic || $scope.model.mEpicRoot;

    // Find the next idOrder
    var idOrder = $scope.getNextIdOrder(mEpic.cStep);

    var emptyStepToBeAdded = {
      description: '',
      reward: '',
      idOrder: idOrder
    };

    if (typeof idOrderStepSelected !== 'undefined') {
      mEpic.cStep.JSON.map(function (mStep) {
        return mStep;
      }).sort(compareDescending).forEach(function (mStep) {
        if (position !== 'below' && parseInt(mStep.idOrder) >= parseInt(idOrderStepSelected)) {
          var idOrderBucket = mStep.idOrder;
          mStep.idOrder = emptyStepToBeAdded.idOrder;
          emptyStepToBeAdded.idOrder = idOrderBucket;
        }
      });
    }

    mEpic.cStep.check(function () {
      mEpic.cStep.add(emptyStepToBeAdded, function (mStepNew) {
        if (scope) {
          scope.$parent.isVisibleMenuStep = false;
        }

        mEpic.cStep.JSON.forEach(function (mStep) {
          mStep.idOrder = parseInt(mStep.idOrder);
        });

        $scope.$apply(function () {
          cursorTo('step-' + mStepNew.id);
        });
      });
    });
  };

  $scope.addEmptyEpic = function (scope, idOrderStepSelected) {
    // Find the next idOrder
    var idOrder = $scope.getNextIdOrder($scope.model.mUserGoal.cEpic);

    var emptyEpicToBeAdded = {
      description: '',
      type: 'other',
      cStep: [],
      idOrder: idOrder
    };

    var emptyStepToBeAdded = {
      description: '',
      reward: ''
    };

    if (typeof idOrderStepSelected !== 'undefined') {
      $scope.model.mUserGoal.cEpic.JSON.map(function (mEpic) {
        return mEpic;
      }).sort(compareDescending).forEach(function (mEpic) {
        if (parseInt(mEpic.idOrder) >= parseInt(idOrderStepSelected)) {
          var idOrderBucket = mEpic.idOrder;
          mEpic.idOrder = emptyEpicToBeAdded.idOrder;
          emptyEpicToBeAdded.idOrder = idOrderBucket;
        }
      });
    }

    $scope.model.mUserGoal.cEpic.check(function () {
      $scope.model.mUserGoal.cEpic.add(emptyEpicToBeAdded, function (JSONEpic, cEpic) {
        cEpic.JSON.forEach(function (mEpic) {
          mEpic.idOrder = parseInt(mEpic.idOrder);
          
          if(mEpic.id !== JSONEpic.id) {
            return;
          }

          mEpic.cStep.add(emptyStepToBeAdded, function () {
            $scope.$apply(function () {
              cursorTo('epic-'+JSONEpic.id);
            });
          });

          if (scope) {
            scope.$parent.isVisibleMenuEpic = false;
          }
        });

        $scope.$apply(function () {});
      });
    });
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    sharedDataAddNewGoal.goalJustCreated = false;
    location.hash = '#/goal-details/'+idUserGoal;
  };

  $scope.moveUp = function (id, mEpic) {
    mEpic = mEpic || $scope.model.mEpicRoot;
    var cStep = mEpic.cStep;

    var mStepPrev;
    cStep.JSON.sort(compareAscending).forEach(function (mStep) {
      if(mStep.id !== id) {
        mStepPrev = mStep;
        return;
      }

      var idOrderBucket = mStep.idOrder;
      mStep.idOrder = mStepPrev.idOrder;
      mStepPrev.idOrder = idOrderBucket;
    });

    cStep.JSON.sort(compareId);
    cStep.check(function () {});
  };

  $scope.moveUpEpic = function (id) {
    var cEpic = $scope.model.mUserGoal.cEpic;

    var mEpicPrev;
    cEpic.JSON.sort(compareAscending).forEach(function (mEpic) {
      if(mEpic.id !== id) {
        mEpicPrev = mEpic;
        return;
      }

      var idOrderBucket = mEpic.idOrder;
      mEpic.idOrder = mEpicPrev.idOrder;
      mEpicPrev.idOrder = idOrderBucket;
    });

    cEpic.JSON.sort(compareId);
    cEpic.check(function () {});
  };

  $scope.moveDown = function (id, mEpic) {
    mEpic = mEpic || $scope.model.mEpicRoot;
    var cStep = mEpic.cStep;

    var mStepPrev;
    cStep.JSON.sort(compareDescending).forEach(function (mStep) {
      if(mStep.id !== id) {
        mStepPrev = mStep;
        return;
      }

      var idOrderBucket = mStep.idOrder;
      mStep.idOrder = mStepPrev.idOrder;
      mStepPrev.idOrder = idOrderBucket;
    });

    cStep.JSON.sort(compareId);
    cStep.check(function () {});
  };

  $scope.moveDownEpic = function (id) {
    var cEpic = $scope.model.mUserGoal.cEpic;

    var mEpicPrev;
    cEpic.JSON.sort(compareDescending).forEach(function (mEpic) {
      if(mEpic.id !== id) {
        mEpicPrev = mEpic;
        return;
      }

      var idOrderBucket = mEpic.idOrder;
      mEpic.idOrder = mEpicPrev.idOrder;
      mEpicPrev.idOrder = idOrderBucket;
    });

    cEpic.JSON.sort(compareId);
    cEpic.check(function () {});
  };

  $scope.selectStepToMove = function (mStep, scopeStep, cStep) {
    scopeStep.isVisibleMenuStep = false;

    if (!scopeStep.isSelected) {
      $scope.stepToMove.listMStepWrapped.push({
        mStep: mStep,
        cStepRelated: cStep
      });
      scopeStep.isSelected = true;
    } else {
      var index = -1;
      $scope.stepToMove.listMStepWrapped.forEach(function (mStepWrappedIter, indexIter) {
        if (mStep.id === mStepWrappedIter.id) {
          index = indexIter;
        }
      });

      if (index !== -1) {
        delete $scope.stepToMove.listMStepWrapped[index];
      }

      scopeStep.isSelected = false;
    }

    $scope.$apply(function () {});
  };

  $scope.moveStepToTopic = function (parentScope) {
    if (!$scope.stepToMove.listMStepWrapped.length) {
      $rootScope.$emit('alert', {
        title: 'No step selected.',
        text: 'Select any of the steps by clicking on the cut icon <i class="fa fa-cut"></i> to move them here. Then click this <i class="fa fa-paste"></i> icon again.',
        fnOk: function () {}
      });
    }

    // Change idLink of all selected steps to id of the Epic I want them to move to
    function iterateListMStepWrapped() {
      var mStepWrapped = $scope.stepToMove.listMStepWrapped.pop();
      mStepWrapped.mStep.idOrder  = $scope.getNextIdOrder($scope.model.cStep);
      mStepWrapped.mStep.idLink   = parentScope.mEpic.id;
      // Update current mStep in DB
      mStepWrapped.cStepRelated.check(function () {
        // Reload source of up to date idOrders for mSteps
        $scope.model.cStep.reload(function () {
          // if there is more mStep left to move
          if ($scope.stepToMove.listMStepWrapped.length) {
            iterateListMStepWrapped();
          } else {
            // Reload user goal to reflect moved steps
            $scope.model.cUserGoal.reload(function (cUserGoal) {
              $scope.model.cUserGoal = cUserGoal;
              $scope.model.mUserGoal = cUserGoal.getById($scope.model.mUserGoal.id);

              // Reload page to reflect changes in data
              init(true)
            });
          }
        });
      });
    }

    iterateListMStepWrapped();
  };

  $scope.setStatusStep = function (step, status, scope, cStep) {
    if (status === 'inProgress') {
      // If you want to move from Backlog to In Progress but there is already a task in there
      if ($scope.model.mStepListAllInMUserGoal.filter(function(mStep){
          return mStep.status === 'inProgress';
        }).length) {
        $rootScope.$emit('alert', {
          title: 'Already working on one!',
          type: 'simple',
          text: 'Finish that one first or move it back to other steps.',
          fnOk: function () {}
        });

        return;
      }

      if (parseInt(step.timestampStart) === -1) {
        step.timestampStart = $scope.time.get().format('x');
      }
    }

    if (status === 'done') {
      step.isDone = true;
      step.timestampDone = parseInt($scope.time.get().format('x'));

      if (step.reward) {
        $rootScope.$emit('alert', {
          title: 'Good Job',
          text: 'Now reward yourself: ' + step.reward,
          fnOk: function () {}
        });
      }
    }

    step.status = status;

    if (scope) {
      scope.$parent.isVisibleMenuStep = false;
    }

    var listOfDeferred = [];
    step.status = status;
    $scope.model.cStepListAllInMUserGoal.forEach(function (cStep) {
      var deferred = $q.defer();
      listOfDeferred.push(deferred.promise);
      cStep.check(function () {
        deferred.resolve();
      });
    });

    $q.all(listOfDeferred).then(function () {
      if (status === 'inProgress') {
        initTimeTracking();
        trackTimeService.start(step);
      }

      if (status === 'done') {
        trackTimeService.stop(step);
      }

      if (status === 'backlog' || status === 'blocked') {
        trackTimeService.stop(step);
      }

      // Update time elapsed on the step that status has changed
      trackTimeService.check(step, cStep, scope);
    });

  };

  $scope.goToRewards = function () {
    location.hash = '#/add-new-goal/rewards';
  };

  $scope.setComplexity = function (mStep, complexity, cStep) {
    if (mStep.status !== 'backlog') {
      $rootScope.$emit('alert', {
        title: "CAN’T BE CHANGED!",
        type: 'simple',
        text: "Difficulty can’t be changed once you’ve started working on a step. <br><span class='reasoning'>It is important that you don’'t adjust it to get the estimated end date of the project right.</span>"
      });

      return;
    }

    if(complexity === 4) {
      $rootScope.$emit('alert', {
        isTutorial: true,
        title: 'High Difficulty',
        type: 'simple',
        text: 'When a step seems too difficult, try to break it down into smaller steps; then it’s easier to measure progress.'
      });
    } else {
      if ($scope.showPopupForComlexity) {
        $rootScope.$emit('alert', {
          isTutorial: true,
          title: 'Difficulty',
          type: 'simple',
          text: 'Estimate how hard it is to finish these steps. One star is easy; four is very hard.'
        });

        $scope.showPopupForComlexity = false;
      }
    }

    // Set complexity back to default if complexity one symbol is tapped twice
    if (complexity === 1 && parseInt(mStep.complexity) === complexity) {
      mStep.complexity = -1;
    } else {
      mStep.complexity = complexity;
    }

    cStep.check();
  };

  $scope.setPriority = function (mStep, cStep) {
    if (mStep.status === 'done') {
      $rootScope.$emit('alert', {
        title: "CAN’T BE CHANGED!",
        type: 'simple',
        text: "Priority can’t be changed once you’ve completed a step."
      });

      return;
    }

    if ($scope.showPopupForPriority) {
      $rootScope.$emit('alert', {
        isTutorial: true,
        title: 'Priority',
        type: 'simple',
        text: 'Mark the important task. Blue is not important; red is urgent.'
      });

      $scope.showPopupForPriority = false;
    }

    mStep.priority = parseInt(mStep.priority) + 1;

    if (mStep.priority > 4) {
      mStep.priority = 1;
    }

    cStep.check();
  };

  $scope.addReward = function (scope, mStep) {
    if (mStep && mStep.status === 'done') {
      $rootScope.$emit('alert', {
        title: "o_0 ???",
        type: 'simple',
        text: "Why would you add a reward to a step that is already completed?"
      });

      return;
    }

    if ($scope.showPopupForReward) {
      $rootScope.$emit('alert', {
        isTutorial: true,
        title: 'Treat Yourself',
        text: "Take a look at your milestones and come up with a reward for every milestone you’ve got. It’s all about how to trick yourself into the right behaviour. When you reach a milestone, you reward yourself with a prize. Prize can be big or small; it’s up to you, but it has to be inspiring. <a href=\"#/concept/rewards\" class=\"btn-at-bottom\">Read more.</a>",
        type: 'simple'
      });
    }

    // Don't show related popup all the time
    $scope.showPopupForReward = false;

    scope.isVisibleReward = true;
    scope.isVisibleMenuStep = false;
  };

  $scope.addCost = function (scope, mStep) {
    scope.isVisibleCost = true;
    scope.isVisibleMenuStep = false;
  };

  function getStepInProgress() {
    var mStepInProgress;
    $scope.model.mUserGoal.cEpic.JSON.forEach(function (mEpic) {
      mEpic.cStep.JSON.forEach(function (mStep) {
        if (mStep.status === 'inProgress') {
          mStepInProgress = mStep;
        }
      });
    });

    return mStepInProgress;
  }

  // Initialize
  function initTimeTracking() {
    var mStepInProgress = getStepInProgress();
    if (mStepInProgress) {
      trackTimeService.init(mStepInProgress, function () {});
    }
  };

  $scope.formatDate = function (timestampStr) {
    return moment(parseInt(timestampStr)).format('ddd, MMMM Do');
  };
  
  $scope.toggleShowStepDone = function () {
    $scope.isVisibleStepDone = !$scope.isVisibleStepDone;
    setting.set('stepDetailedIsVisibleStepDone', $scope.isVisibleStepDone);
  };

  $scope.toggleStepSortOrderByPriority = function () {
    if ($scope.sortOrder === '-priority') {
      $scope.sortOrder = 'idOrder';
    } else {
      $scope.sortOrder = '-priority';
    }
    setting.set('stepDetailedSortOrder', $scope.sortOrder);
  };

  init();
  initTimeTracking();
};