var CtrlListStep = function ($scope, sharedDataAddNewGoal, pageManager) {
  function compareAscending(a,b) {
    if (a.idOrder < b.idOrder)
      return -1;
    else if (a.idOrder > b.idOrder)
      return 1;
    else
      return 0;
  }

  function compareDescending(a,b) {
    if (a.idOrder < b.idOrder)
      return 1;
    else if (a.idOrder > b.idOrder)
      return -1;
    else
      return 0;
  }

  function compareId(a,b) {
    if (a.id < b.id)
      return -1;
    else if (a.id > b.id)
      return 1;
    else
      return 0;
  }

  var init = function () {
    pageManager.setTitle('Steps');

    if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
      location.hash = '#/add-new-goal/introduction';
      return;
    }

    $scope.model = {};
    $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
    $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;

    $scope.model.mEpicRoot = $scope.model.mUserGoal.cEpic.JSON[0];

    if ($scope.model.mEpicRoot.cStep.JSON.length === 0) {
      $scope.addEmptyStep();
    }
  };

  $scope.removeStep = function (id) {
    $scope.model.mEpicRoot.cStep.removeById('step', id, function() {
      $scope.$apply(function () {});
    })
  };

  $scope.addEmptyStep = function () {
    $scope.model.mEpicRoot.cStep.add({
      description: '',
      reward: ''
    }, function (JSONStep, cStep) {
        // what is this fix for??
        cStep.JSON.forEach(function (mStep) {
          if(mStep.id !== JSONStep.id) {
            return;
          }

          mStep.idOrder = mStep.id+'';
          cStep.check(function () {
            $scope.$apply(function () {
              setTimeout(function () {
                scrollToElement();
              }, 0)
            });
          });
        });
    });
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    sharedDataAddNewGoal.goalJustCreated = true;
    location.hash = '#/goal-details/'+idUserGoal;
  };

  $scope.moveUp = function (id) {
    var cStep = $scope.model.mEpicRoot.cStep;

    //console.log('move up', id);
    //console.log('cStep before ' + cStep.JSON[0].description, cStep.JSON[0].idOrder);
    //console.log('cStep before ' + cStep.JSON[1].description, cStep.JSON[1].idOrder);
    //console.log('cStep before ' + cStep.JSON[2].description, cStep.JSON[2].idOrder);

    var mStepPrev;
    cStep.JSON.sort(compareAscending).forEach(function (mStep) {
      if(mStep.id !== id) {
        mStepPrev = mStep;
        return;
      }

      var idOrderBucket = mStep.idOrder;
      mStep.idOrder = mStepPrev.idOrder;
      mStepPrev.idOrder = idOrderBucket;
    });

    cStep.JSON.sort(compareId);
    cStep.check(function () {
      //console.log('cStep after ' + cStep.JSON[0].description, cStep.JSON[0].idOrder);
      //console.log('cStep after ' + cStep.JSON[1].description, cStep.JSON[1].idOrder);
      //console.log('cStep after ' + cStep.JSON[2].description, cStep.JSON[2].idOrder);
    });

    //$scope.$apply(function () {});
  };

  $scope.moveDown = function (id) {
    var cStep = $scope.model.mEpicRoot.cStep;

    console.log('move down', id);
    //console.log('cStep before ' + cStep.JSON[0].description, cStep.JSON[0].idOrder);
    //console.log('cStep before ' + cStep.JSON[1].description, cStep.JSON[1].idOrder);
    //console.log('cStep before ' + cStep.JSON[2].description, cStep.JSON[2].idOrder);

    var mStepPrev;
    cStep.JSON.sort(compareDescending).forEach(function (mStep) {
      if(mStep.id !== id) {
        mStepPrev = mStep;
        return;
      }

      var idOrderBucket = mStep.idOrder;
      mStep.idOrder = mStepPrev.idOrder;
      mStepPrev.idOrder = idOrderBucket;
    });

    cStep.JSON.sort(compareId);
    cStep.check(function () {
      //console.log('--');
      //console.log('cStep after ' + cStep.JSON[0].description, cStep.JSON[0].idOrder);
      //console.log('cStep after ' + cStep.JSON[1].description, cStep.JSON[1].idOrder);
      //console.log('cStep after ' + cStep.JSON[2].description, cStep.JSON[2].idOrder);
    });

    //$scope.$apply(function () {});
  };

  init();
};