var CtrlDefinition = function ($scope, $stateParams, sharedDataAddNewGoal, pageManager) {
  pageManager.setTitle('Definition');

  $scope.isStatusEditing = $stateParams.status === 'edit';
  console.log('$scope.isStatusEditing', $scope.isStatusEditing);

  if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
    location.hash = '#/add-new-goal/introduction';
    return;
  }

  $scope.model = {};
  $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
  $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;

  $scope.goBack = function () {
    history.go(-1);
  }
};