define(function () {
  var directivetrackTimeArchiveList = function () {
    return {
      restrict: 'E',
      templateUrl: 'product/page/add-new-goal/partial/directive-track-time-archive-list-tmpl.html',
      scope: {
        mStep: '=',
        cStep: '=',
        trackTimeService: '=',
      },
      controller: function ($scope) {
        $scope.getDiff = function (trackRecord) {
          var diff = trackRecord.timeEnd !== 'timestamp' ?
                        Math.round((parseInt(trackRecord.timeEnd) - parseInt(trackRecord.timeStart))/1000) :
                        -1;

          return diff;
        };
        
        $scope.formatDate = function (timestampStr) {
          return moment(parseInt(timestampStr)).format('ddd, MMMM Do');
        };

        $scope.convertToHours = function (second) {
          return Math.round10((parseInt(second)/3600), -2);
        };

        $scope.update = function (value, trackRecord, mStep) {
          var millisecond = value * 3600 * 1000;
          trackRecord.timeEnd = parseInt(trackRecord.timeStart) + millisecond;

          if (typeof $scope.$parent.$parent !== 'undefined') {
            var mEpic = $scope.$parent.$parent.model.mEpicRoot;
          } else {
            var mEpic = $scope.$parent.$parent.$parent.mEpic;
          }

          $scope.trackTimeService.check(mStep, $scope.cStep, $scope.$parent.$parent.$parent);
        };
      }
    };
  };

  return directivetrackTimeArchiveList;
});