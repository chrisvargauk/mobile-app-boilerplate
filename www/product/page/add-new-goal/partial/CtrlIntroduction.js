var CtrlIntroduction = function ($scope, sharedDataAddNewGoal, cUserGoal, pageManager, $rootScope, cNote, time) {
  pageManager.setTitle('Create New Project');

  $scope.init = function () {
    sharedDataAddNewGoal.cUserGoal = cUserGoal;
    $scope.cNote = cNote;
    $scope.time = time;
    $scope.listIdNoteSelected = [];

    $scope.unrelatedNoteList = $scope.cNote.JSON.filter(function (mNote) {
      return parseInt(mNote.idUserGoal) === -1;
    });

    // If there is no note that isn't allocated to any other note, go to next step automatically
    if (!$scope.unrelatedNoteList.length) {
      $scope.createNewGoal();
    }
  };

  $scope.goToNextPage = function () {
    location.hash = '#/add-new-goal/definition';
  };

  $scope.createNewGoal = function () {
    // Apply restriction if demo version
    //if (sharedDataAddNewGoal.cUserGoal.JSON.length > 0) {
    //  console.log('trial block');
    //
    //  $rootScope.$emit('alert', {
    //    title: 'Demo Version',
    //    text: 'In demo version you are limited to have one plan only. If you are happy with our product please consider purchasing the full version.',
    //    type: 'simple',
    //    fnOk: function () {
    //      location.hash = '#/home';
    //    }
    //  });
    //
    //  return;
    //}

    cUserGoal.add({
      name: '',
      description: '',
      tipVisible: true,
      cReason: [],
      cEpic: [],
      cTimeAndLocation: [],
      cTrackRecord: [],
      timestamp: time.get().format('x')
    }, function(model) {
      sharedDataAddNewGoal.mUserGoal = model;

      model.cEpic.add({
        type: 'root',
        cStep: [],
      }, function () {
        // Add Plan id to Notes to make them to be related
        $scope.listIdNoteSelected.forEach(function (idNote) {
          var mNote = $scope.cNote.getById(idNote);
          mNote.idUserGoal = sharedDataAddNewGoal.mUserGoal.id;
        });

        // Update notes in db
        $scope.cNote.check(function () {
          $scope.goToNextPage();
        });
      });
    });
  };

  $scope.html2text = function (html) {
    return html.replace(/<\/?[^>]+(>|$)/g, " ").replace('&nbsp;', '');
  };

  $scope.selectNote = function (idNote) {
    // sharedDataAddNewGoal.mNote = cNote.getById(idNote);
    // location.hash = '#/note-add/'+idNote;

    // If note is not selected yet
    if ($scope.listIdNoteSelected.indexOf(idNote) === -1) {
      $scope.listIdNoteSelected.push(idNote);
    } else {
      delete $scope.listIdNoteSelected[$scope.listIdNoteSelected.indexOf(idNote)];
    }
  };

  $scope.isSelected = function (idNote) {
    return $scope.listIdNoteSelected.indexOf(idNote) !== -1;
  };

  $scope.init();
};