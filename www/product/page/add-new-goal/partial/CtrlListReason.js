var CtrlReason = function ($scope, sharedDataAddNewGoal, pageManager) {
  console.log('Reason Controller is loaded.');

  var init = function () {
    pageManager.setTitle('Inspiration');

    if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
      if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
        $rootScope.$emit('alert', {
          title: 'No project selected',
          text: 'Please create or select a project than you can add inspiration.',
          type: 'simple',
          fnOk: function () {
            location.hash = '#/home';
          }
        });

        return;
      }

      return;
    }

    $scope.model = {};
    $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
    $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;

    if ($scope.model.mUserGoal.cReason.JSON.length === 0) {
      $scope.addEmptyReason();
    }
  };

  $scope.removeReason = function (id) {
    $scope.model.mUserGoal.cReason.removeById('reason', id, function() {
      $scope.$apply(function () {});
    })
  };

  $scope.addEmptyReason = function () {
    $scope.model.mUserGoal.cReason.add({
      description: ''
    }, function () {
      $scope.$apply(function () {});
    });
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  init();

};