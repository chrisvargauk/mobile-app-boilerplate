define(['Collection'], function () {
  window.deferredFnDailyPriority = collectionFactory.getDeferredFn('cDailyPriority', {
    type: 'dailyPriority',
    //filter: ' timestamp > 0 ORDER BY timestamp DESC',
    default: {
      task: '',
      reward: '',
      isDoneTask: false,
      timestamp: ''
    }
  });

  window.deferredFnGetStructured = collectionFactory.getDeferredFn('cGetStructured', {
    type: 'getStructured',
    //filter: ' timestamp > 0 ORDER BY timestamp DESC',
    default: {
      appreciation: '',
      problem: '',
      fix: '',
      isDoneTask: true,
      timestamp: ''
    }
  });

  window.deferredFnAchievement= collectionFactory.getDeferredFn('cAchievement', {
    type: 'achievement',
    default: {
      type: '',
      timestamp: ''
    },
    debug: false
  });

  window.deferredFnRewardAndSkill= collectionFactory.getDeferredFn('cRewardAndSkill', {
    type: 'rewardAndSkill',
    default: {
      credit: '0',
      ip: '0'
    },
    debug: false
  });

  window.deferredFnReason = collectionFactory.getDeferredFn('cReason', {
    type: 'reason',
    default: {
      description: 'description of reason'
    }
  });

  window.deferredFnEpic = collectionFactory.getDeferredFn('cEpic', {
    type: 'epic',
    default: {
      description: 'description of epic',
      type: 'other',
      cStep: 'collectionType(@)step',
      idOrder: '-1'
    }
  });

  window.deferredFnStep = collectionFactory.getDeferredFn('cStep', {
    type: 'step',
    default: {
      description: 'description of step',
      reward: 'related reward',
      cost: '',
      isDone: false,
      status: 'backlog',
      timestampStart: '-1',
      timestampDone: '-1',
      timeElapsedSecond: '-1',
      idOrder: '-1',
      complexity: '-1',
      priority: '1'
    }
  });

  window.deferredFnTimeAndLocation = collectionFactory.getDeferredFn('cTimeAndLocation', {
    type: 'timeAndLocation',
    default: {
      day: 'none',
      fromHour: '0',
      fromMin: '0',
      toHour: '0',
      toMin: '0',
      location: 'none'
    }
  });

  window.deferredFnTrackRecord = collectionFactory.getDeferredFn('cTrackRecord', {
    type: 'trackRecord',
    default: {
      idStep: '-1',
      timeStart: 'timestamp',  // timestamp
      timeEnd: 'timestamp',     // timestamp
      weekOfYear: '-1',
      dayOfWeek: '-1'
    }
  });

  window.deferredFnUserGoal = collectionFactory.getDeferredFn('cUserGoal', {
    type: 'userGoal',
    default: {
      name: 'Name of the Goal',
      description: 'Short Description',
      tipVisible: true,
      cReason: 'collectionType(@)reason',
      cEpic: 'collectionType(@)epic',
      cTimeAndLocation: 'collectionType(@)timeAndLocation',
      cTrackRecord: 'collectionType(@)trackRecord',
      timestamp: '-1',
      cReview: 'collectionType(@)review'
    },
    debug: false
  });

  window.deferredFnNote = collectionFactory.getDeferredFn('cNote', {
    type: 'note',
    default: {
      name: '',
      description: '',
      timestamp: '-1',
      timestampUpdate: '-1',
      indexIdea: '-1',
      idComfortChallenge: '-1',
      idUserGoal: '-1',
      ctrUpdate: '-1'
    },
    debug: false
  });

  window.deferredFncComfortChallenge = collectionFactory.getDeferredFn('cComfortChallenge', {
    type: 'comfortChallenge',
    default: {
      name: '',
      shortDescription: '',
      description: '',
      timestampStart: '-1',
      timestampEnd: '-1',
      duration: '-1',
      difficulty: '-1',
      status: 'locked' // 'locked', 'unlocked', 'started', 'declined', 'completed'
    },
    debug: false
  });

  window.deferredFncGuide = collectionFactory.getDeferredFn('cGuide', {
    type: 'guide',
    default: {
      main: true,
      mainGetStructuredPreview: false,
      classroomMain: true,
      classroomIntellectualProperty: false,
      conceptHabitAndRoutine: true,
      conceptIntellectualProperty: true,
      getStructuredMain: true,
      noteList: true,
      noteListNoteAdded: true,
      mainComfortChallenge: false,
      classroomComfortChallenge: false,
      comfortChallenge: true,
      comfortChallengeAccepted: false,
      goalList: true,
      conceptComfortChallenge: true,
      mainPlan: false
    },
    debug: false
  });

  window.deferredFnReview = collectionFactory.getDeferredFn('cReview', {
    type: 'review',
    default: {
      scheduled: '',
      wentWrong: '',
      whatDifferent: '',
      isDone: false
    },
    debug: false
  });

  window.deferredFnConcept = collectionFactory.getDeferredFn('cConcept', {
    type: 'concept',
    default: {
      title: '',
      description: '',
      link: '',
      free: true,
      price: '',
      purchased: false
    },
    debug: false
  });
});