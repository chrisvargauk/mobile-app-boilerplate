define(function () {
    return function () {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'product/page/dev/directive-manage-tables-tmpl.html',
            controller: function ($scope) {
                console.log('Manage tables is loaded');

                $scope.listNameCollection = [];

                $scope.delete = function (name) {
                    console.log('delete', name);
                    Collection.prototype.deleteWebSQL(name, function () {
                        console.log(name + ' deleted');
                        listCollectionTypes(function () {
                            $rootScope.$emit('alert', {
                                title: 'Restart App Now!',
                                text: 'One or more MySQL Tables have been deleted, restart the app NOW!',
                                type: 'simple'
                            });
                        });
                    });
                };

                function listCollectionTypes(callback) {
                    $scope.listNameCollection = [];

                    websql.doesTableExist('collectionType', function (doesTableExist) {
                        if (!doesTableExist) {
                            return;
                        }

                        websql.run("SELECT * FROM 'collectionType';", function (item) {
                            $scope.listNameCollection.push(item.nameCollection);
                        }, function () {
                            $scope.$apply(function () {});

                            if (typeof callback === 'function') {
                                callback();
                            }
                        });
                    });
                }

                listCollectionTypes();
            }
        }
    }
});