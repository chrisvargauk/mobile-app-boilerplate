var CtrlDev = function ($scope, pageManager, sharedDataAddNewGoal, cGetStructured, cDailyPriority, cNote, time) {
  pageManager.setTitle('Dev');

  $scope.time = time;
  $scope.cDailyPriority = cDailyPriority;
  $scope.cGetStructured = cGetStructured;
  $scope.cNote = cNote;
  console.log('time', time.get().format('x'));

  // Settings: Set default values for tut settings
  //$scope.isDisabledListItemHabitAndRoutine = false;
  //$scope.isHighlitedListItemHabitAndRoutine = false;
  //$scope.isDisabledListItemIntellectualProperty = false;
  //$scope.isHighlitedListItemIntellectualProperty = false;
  //$scope.isDisabledListItemComfortChallenge = false;
  //$scope.isHighlitedListItemComfortChallenge = false;
  //$scope.isDisabledListItemOther1 = false;
  //$scope.isHighlitedListItemOther1 = false;
  //$scope.isDisabledListItemOther2 = false;
  //$scope.isHighlitedListItemOther2 = false;
  //$scope.isDisabledBtnBack = false;
  //$scope.isHighlitedBtnBack = false;

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  $scope.addXDaysStreak = function (typeHabitStreak, ctr, isLeaveOutToday) {
    console.log('ctr', ctr);

    if (ctr === 0 && isLeaveOutToday) {
      $rootScope.$emit('alert', {
        title: 'Complete',
        text: '..',
        type: 'simple'
      });
    } else {
      switch (typeHabitStreak) {
        case 'cDailyPriority': var defaultObj = {
            timestamp: moment().subtract(ctr, 'day').format('x'),
            task: moment().subtract(ctr, 'day').format('dd'),
            isDoneTask: true
          };
          break;
        case 'cGetStructured': var defaultObj = {
            appreciation: moment().subtract(ctr, 'day').format('dd'),
            problem: moment().subtract(ctr, 'day').format('dd'),
            fix: moment().subtract(ctr, 'day').format('dd'),
            isDoneTask: true,
            timestamp: moment().subtract(ctr, 'day').format('x')
          };
          break;
        case 'cNote': var defaultObj = {
            name: moment().subtract(ctr, 'day').format('dd'),
            description: moment().subtract(ctr, 'day').format('dd'),
            timestamp: moment().subtract(ctr, 'day').format('x'),
            timestampUpdate: moment().subtract(ctr, 'day').format('x'),
            indexIdea: '-1',
            ctrUpdate: '1'
          };
          break;
      }

      $scope[typeHabitStreak].add(defaultObj, function () {
        if (ctr === 0) {
          $rootScope.$emit('alert', {
            title: 'Complete',
            text: '..',
            type: 'simple'
          });
        } else {
          $scope.addXDaysStreak(typeHabitStreak, --ctr, isLeaveOutToday);
        }
      });
    }
  };

  $scope.resetApp = function () {
    window.resetApp();
    //alert('Restart the app');
  };

  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.classroomMain) {
  //
  //  // Disable & Highlight buttons for tutorial
  //  highlightMenu('disabled');
  //  $scope.isDisabledListItemHabitAndRoutine = false;
  //  $scope.isHighlitedListItemHabitAndRoutine = true;
  //  $scope.isDisabledListItemIntellectualProperty = true;
  //  $scope.isHighlitedListItemIntellectualProperty = false;
  //  $scope.isDisabledListItemComfortChallenge = true;
  //  $scope.isHighlitedListItemComfortChallenge = false;
  //  $scope.isDisabledListItemOther1 = true;
  //  $scope.isHighlitedListItemOther1 = false;
  //  $scope.isDisabledListItemOther2 = true;
  //  $scope.isHighlitedListItemOther2 = false;
  //  $scope.isDisabledBtnBack = true;
  //  $scope.isHighlitedBtnBack = false;
  //
  //  // Turn off completed Tut.
  //  sharedDataAddNewGoal.guide.classroomMain = false;
  //  // Turn on next Tut.
  //  sharedDataAddNewGoal.guide.conceptHabitAndRoutine = true;
  //  // Save Tut. changes
  //  sharedDataAddNewGoal.cGuide.check();
  //
  //  $rootScope.$emit('alert', {
  //    isTutorial: true,
  //    title: 'Welcome to Classroom',
  //    text: 'You can learn here what new ideas you have to install and what old ideas you have to get rid of to thrive as an entrepreneur. Now Select the first concept: Habits & Daily Routine',
  //    type: 'simple'
  //  });
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.classroomIntellectualProperty) {
  //
  //  // Disable & Highlight buttons for tutorial
  //  highlightMenu('disabled');
  //  $scope.isDisabledListItemHabitAndRoutine = true;
  //  $scope.isHighlitedListItemAndRoutine = false;
  //  $scope.isDisabledListItemIntellectualProperty = false;
  //  $scope.isHighlitedListItemIntellectualProperty = true;
  //  $scope.isDisabledListItemComfortChallenge = true;
  //  $scope.isHighlitedListItemComfortChallenge = false;
  //  $scope.isDisabledListItemOther1 = true;
  //  $scope.isHighlitedListItemOther1 = false;
  //  $scope.isDisabledListItemOther2 = true;
  //  $scope.isHighlitedListItemOther2 = false;
  //  $scope.isDisabledBtnBack = true;
  //  $scope.isHighlitedBtnBack = false;
  //
  //  $rootScope.$emit('alert', {
  //    isTutorial: true,
  //    title: 'Welcome back to Classroom',
  //    text: 'Come back here whenever you are ready to learn new, practical concepts and start applying them. Now take a look at the second one.',
  //    type: 'simple'
  //  });
  //
  //  // Turn off completed Tut.
  //  sharedDataAddNewGoal.guide.classroomIntellectualProperty = false;
  //  // Turn on next Tut.
  //  sharedDataAddNewGoal.guide.conceptIntellectualProperty = true;
  //  // Save Tut. changes
  //  sharedDataAddNewGoal.cGuide.check();
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.classroomComfortChallenge) {
  //
  //  highlightMenu('disabled');
  //
  //  $scope.isDisabledListItemIntellectualProperty = true;
  //  $scope.isHighlitedListItemIntellectualProperty = false;
  //  $scope.isDisabledListItemComfortChallenge = false;
  //  $scope.isHighlitedListItemComfortChallenge = true;
  //  $scope.isDisabledListItemOther1 = true;
  //  $scope.isHighlitedListItemOther1 = false;
  //  $scope.isDisabledListItemOther2 = true;
  //  $scope.isHighlitedListItemOther2 = false;
  //  $scope.isDisabledBtnBack = true;
  //  $scope.isHighlitedBtnBack = false;
  //
  //  sharedDataAddNewGoal.guide.classroomComfortChallenge = false;
  //  sharedDataAddNewGoal.guide.conceptComfortChallenge = true;
  //  sharedDataAddNewGoal.cGuide.check();
  //}
};