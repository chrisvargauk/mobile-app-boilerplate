// # Create new note or load existing one
function establishNote (cNote, $scope, $stateParams, time) {
  $scope.cNote = cNote;

  // New Empty Note
  if($stateParams.id === 'new') {
    $scope.noteCurrent = {
      name: '',
      description: '',
      timestamp: time.get().format('x')
    };

    // If there is suggested idea to show
    if(typeof $stateParams.indexCategory !== 'undefined' &&
       typeof $stateParams.indexIdea     !== 'undefined' ) {
      $scope.noteCurrent.indexIdea = $stateParams.indexCategory+'/'+$stateParams.indexIdea;
    }

    // New Note for Comfort Challenge
    if(typeof $stateParams.idComfortChallenge !== 'undefined' &&
        typeof $stateParams.nameComfortChallenge !== 'undefined') {
      $scope.noteCurrent.name = $stateParams.nameComfortChallenge + ' comfort challenge';
      $scope.noteCurrent.idComfortChallenge = $stateParams.idComfortChallenge;
    }

    // New Note for Plan
    if(typeof $stateParams.idUserGoal !== 'undefined' &&
       typeof $stateParams.nameUserGoal !== 'undefined') {
    $scope.noteCurrent.name = $stateParams.nameUserGoal + ' Note';
    $scope.noteCurrent.idUserGoal = $stateParams.idUserGoal;
  }

  // If it's an existing note
  } else if (typeof $stateParams.id !== 'undefined') {
    $scope.noteCurrent = angular.copy( $scope.cNote.getById($stateParams.id) );
  }
}

var CtrlNoteAdd = function ($scope, $stateParams, time, sharedDataAddNewGoal, pageManager, listIdea, listIdeaFull, cNote) {
  pageManager.setTitle('Add/Edit Note');

  console.log('sharedDataAddNewGoal', sharedDataAddNewGoal);

  // # Create new note or load existing one
  establishNote (cNote, $scope, $stateParams, time);

  document.querySelector('.editor-content').innerHTML = $scope.noteCurrent.description;
  console.log('$scope.noteCurrent', $scope.noteCurrent);


  // # Slider
  $scope.isSliderActive = false;

  $scope.listIdea = listIdea;
  $scope.listIdeaFull = listIdeaFull;
  $scope.indexIdea = typeof $stateParams.indexIdea !== 'undefined' ? parseInt($stateParams.indexIdea) : -1;
  $scope.indexCategory = typeof $stateParams.indexCategory !== 'undefined' ? parseInt($stateParams.indexCategory) : -1;
  $scope.showIdea = function () {
    return $scope.indexIdea !== -1;
  };

  if ($scope.showIdea()) {
    $scope.noteCurrent.name = $scope.listIdeaFull[$scope.indexCategory].suggestionList[$scope.indexIdea].title;
  }


  // # Editor
  document.querySelector('.editor-content').addEventListener('click', function() {
    resetAllButton();
    setButtonState();
  }, false);

  function setFocusToEditor() {
    document.querySelector('.editor-content').focus()
  }

  function setButtonState(node) {
    var parentNode = node ? node.parentNode : window.getSelection().focusNode.parentNode;
    console.log(parentNode.nodeName);

    switch(parentNode.nodeName) {
      case 'B':
        $scope.buttonMap.bold = true;
        setButtonState(parentNode);
        console.log('bold', parentNode);
        break;
      case 'I':
        $scope.buttonMap.italic = true;
        setButtonState(parentNode);
        console.log('italic');
        break;
      case 'U':
        $scope.buttonMap.underline = true;
        setButtonState(parentNode);
        console.log('underline');
        break;
      case 'H1':
        $scope.buttonMap.h1 = true;
        setButtonState(parentNode);
        console.log('h1');
        break;
      case 'H2':
        $scope.buttonMap.h2 = true;
        setButtonState(parentNode);
        console.log('h2');
        break;
      case 'LI':
        if (parentNode.parentNode.nodeName === 'UL') {
          console.log('ul');
          $scope.buttonMap.ul = true;
        }
        if (parentNode.parentNode.nodeName === 'OL') {
          console.log('ol');
          $scope.buttonMap.ol = true;
        }
        setButtonState(parentNode.parentNode);
        console.log('ol/ul');
        break;
      case 'DIV':
        $scope.$apply(function () {});
        console.log('div');
        break;
    }
  }

  function resetAllButton() {
    Object.keys($scope.buttonMap).forEach(function (key) {
      $scope.buttonMap[key] = false;
    });
  }

  $scope.buttonMap = {
    undo: false,
    redo: false,
    bold: false,
    italic: false,
    underline: false,
    ul: false,
    ol: false,
    h1: false,
    h2: false
  };



  $scope.formatUndo = function () {
    document.execCommand('undo',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatRedo = function () {
    document.execCommand('redo',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatBold = function () {
    document.execCommand('bold',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatItalic = function () {
    document.execCommand('italic',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatUnderline = function () {
    document.execCommand('underline',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatUnorderedList = function () {
    document.execCommand('insertUnorderedList',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatOrderedList = function () {
    document.execCommand('insertOrderedList',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatInsertImage = function () {
    document.execCommand('insertImage',false,'module/quote/img/timothy-ferriss.jpg');
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };

  $scope.formatHeading1 = function () {
    var parentNode = window.getSelection().focusNode.parentNode;
    if (parentNode.nodeName === 'H1') {
      var parentNodeReplacement = document.createElement('DIV');
      parentNodeReplacement.appendChild( window.getSelection().focusNode );
      parentNode.parentNode.replaceChild(parentNodeReplacement, parentNode);
    } else {
      document.execCommand('formatBlock',false,'<h1>');
      setFocusToEditor();
      resetAllButton();
      setButtonState();
    }
  };

  $scope.formatHeading2 = function () {
    var parentNode = window.getSelection().focusNode.parentNode;
    if (parentNode.nodeName === 'H2') {
      var parentNodeReplacement = document.createElement('DIV');
      parentNodeReplacement.appendChild( window.getSelection().focusNode );
      parentNode.parentNode.replaceChild(parentNodeReplacement, parentNode);
    } else {
      document.execCommand('formatBlock',false,'<h2>');
      setFocusToEditor();
      resetAllButton();
      setButtonState();
    }
  };

  $scope.formatRemoveFormat = function () {
    document.execCommand('removeFormat',false,null);
    setFocusToEditor();
    resetAllButton();
    setButtonState();
  };


  $scope.save = function () {
    function goBackToSourcePage(mNoteOriginal) {
      if (typeof mNoteOriginal.idComfortChallenge !== 'undefined' && mNoteOriginal.idComfortChallenge !== '-1') {
        location.hash = '#/comfort-challenge-detail/'+mNoteOriginal.idComfortChallenge;
        return;
      }

      if (typeof mNoteOriginal.idUserGoal !== 'undefined' && mNoteOriginal.idUserGoal !== '-1') {
        location.hash = '#/goal-details/'+mNoteOriginal.idUserGoal;
        return;
      }

      location.hash = '#/note-list';
    }
    // If document is empty dont save it but dont bother the user either
    if ($scope.noteCurrent.name === '' &&
        typeof $scope.noteCurrent.description === 'undefined') {
      goBackToSourcePage({
        idComfortChallenge: $stateParams.idComfortChallenge,
        idUserGoal: $stateParams.idUserGoal
      });
      return;
    }

    // Update time is recorded when save button is taped
    $scope.noteCurrent.timestampUpdate = time.get().format('x');
    console.log('update ts:', $scope.noteCurrent.timestampUpdate);
    console.log('update day:', moment(parseInt($scope.noteCurrent.timestampUpdate)).format('ddd'));

    // If its a new note
    if($stateParams.id === 'new') {
      // Updated Ctr is 0 if its a new document
      $scope.noteCurrent.ctrUpdate = 0;

      $scope.cNote.add($scope.noteCurrent, function (model) {
        $rootScope.$emit('note.created', {});
        goBackToSourcePage($scope.noteCurrent);
      });

    // If its a note update
    } else if (typeof $stateParams.id !== 'undefined') {
      var mNoteOriginal = $scope.cNote.getById($stateParams.id);

      // The directive for editor we are using turns empty string to undefined, revert that here
      if (typeof $scope.noteCurrent.description === 'undefined') {
        $scope.noteCurrent.description === '';
      }

      // If document hasn't changed
      if (mNoteOriginal.name === $scope.noteCurrent.name &&
          mNoteOriginal.description === $scope.noteCurrent.description) {
        goBackToSourcePage(mNoteOriginal);
        return;
      }

      mNoteOriginal.name = $scope.noteCurrent.name;
      mNoteOriginal.description = $scope.noteCurrent.description;
      mNoteOriginal.timestampUpdate = $scope.noteCurrent.timestampUpdate;
      mNoteOriginal.idComfortChallenge = $scope.noteCurrent.idComfortChallenge;
      mNoteOriginal.idUserGoal = $scope.noteCurrent.idUserGoal;
      // update save counter
      mNoteOriginal.ctrUpdate = parseInt($scope.noteCurrent.ctrUpdate) + 1;

      $scope.cNote.check(function() {
        $rootScope.$emit('note.updated', {});
        goBackToSourcePage(mNoteOriginal);
      });
    }
  };

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    // Check whether document is changed or not
    if($stateParams.id === 'new') {
      var mNoteOriginal = {
        name: '',
        description: ''
      };
    } else if (typeof $stateParams.id !== 'undefined') {
      var mNoteOriginal = $scope.cNote.getById($stateParams.id);
    } else if (typeof $stateParams.idComfortChallenge !== 'undefined') {
      var mNoteOriginal = {
        name: '',
        description: ''
      };
    }


    // If document has changed
    if (mNoteOriginal.name !== $scope.noteCurrent.name ||
        (mNoteOriginal.description !== $scope.noteCurrent.description &&
        typeof $scope.noteCurrent.description !== 'undefined') ) {
      $rootScope.$emit('alert', {
        title: 'DISCARD CHANGES?',
        text: 'Note has changed, are you sure you want to discard those changes?',
        type: 'cancelOk',
        fnOk: function () {
          if (page !== 'back') {
            location.hash = page;
          } else {
            console.log('history.go(-1);');
            history.go(-1);
          }
        }
      });
    } else {
      if (page !== 'back') {
        location.hash = page;
      } else {
        console.log('history.go(-1);');
        history.go(-1);
      }
    }

  };

};