var CtrlNoteSuggestionList = function ($scope, sharedDataAddNewGoal, $rootScope, pageManager, listIdea, listIdeaFull, cNote) {
  pageManager.setTitle('Note Suggestions');

  $scope.listIdea = listIdea;
  $scope.listIdeaFull = listIdeaFull;
  $scope.isIdeaDropdownActive = false;
  $scope.listIdea = listIdea;
  $scope.cNote = cNote;

  $scope.isDisabledBtnCreateNewNote = false;
  $scope.isHighlitedBtnCreateNewNote = false;
  $scope.isDisabledBtnEditNote = false;
  $scope.isHighlitedBtnEditNote = false;
  $scope.isDisabledBtnDeleteNote = false;
  $scope.isHighlitedBtnDeleteNote = false;

  $scope.addNewNote = function (indexCategory, indexIdea) {
    if (typeof indexIdea !== "undefined") {
      location.hash = '#/note-add/new/'+indexCategory+'/'+indexIdea;
    } else {
      location.hash = '#/note-add/new';
    }

  };

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  // Check list of ideas, how many of them is complete
  $scope.indexIdeaList = [];
  $scope.getIndexIdeaList = function () {
    $scope.indexIdeaList = $scope.cNote.JSON.map(function(mNote) {
      return mNote.indexIdea+'';
    });
  };
  $scope.getIndexIdeaList();


  $scope.isIdeaDone = function (indexCategory, indexIdea) {
    return $scope.indexIdeaList.indexOf(indexCategory+'/'+indexIdea) !== -1;
  };
};