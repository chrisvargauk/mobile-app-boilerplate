define(function () {
    return [
        {
            title: 'Make a 80/20 analysis.',
            description: 'List 20% of the pain points in your life (relationships, job, any activities) that drive you mad. Then you can think of a list of actions to eliminate them.'
        },
        {
            title: 'What groups do you belong to?',
            description: 'Write it down, how would you described the groups you belong to. Also write a list of' +
            ' annoying problems they\'ve got. Then write a list of potential solutions that can be productised.'
        },
        {
            title: 'Summarise the news you read.',
            description: 'Every day you read the news, and few days later you don\'t remember any of it. Summarise' +
            ' the most important ones every day and give new titles to them. Yes, it\'s an effort, but builds a habit.'
        },
        {
            title: 'Being an expert is overrated.',
            description: 'Take any topic and research it. Buy some books, read related' +
            ' blogs and start taking notes here, anything that makes you better then 80% of the population. Turn this' +
            ' research into a DVD and start selling it on Amazon or eBay.'
        }
    ];
});