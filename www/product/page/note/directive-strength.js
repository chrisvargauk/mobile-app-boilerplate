define(function () {
  return function () {
    return {
      restrict: 'E',
      scope: {
        strength: '='
      },
      templateUrl: 'product/page/note/directive-strength-tmpl.html',
      controller: function ($scope) {
        console.log('strength is loaded');
        $scope.getStrength = function (ext) {
          var ext = ext || '';

          if (ext === '%') {
            return $scope.strength * 10 + ext;
          } else {
            return $scope.strength;
          }
        }
      }
    }
  }
});