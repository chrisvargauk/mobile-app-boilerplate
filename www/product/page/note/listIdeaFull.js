define(function () {
    return [
        {
            categoryName: 'Take a step back to get the big picture',
            suggestionList: [
                {
                    title: 'Make a 80-20 analysis',
                    description: 'List 20% of the pain points in your life (relationships, job, any activities) that drive you mad. Then you can think of a list of actions to eliminate them.'
                },
                {
                    title: 'Batching Activities',
                    description: 'List your daily tasks. Which ones could be batched to save time and be more effective? For example, read e-mails once a day at 11:00 a.m., and deal with invoices and mails once a week on Friday.'
                },
                {
                    title: 'What can you outsource in your life?',
                    description: 'Think of what to eliminate and what to delegate in your life. Remember, even if you are the best at something, that doesn’t mean it’s efficient for you to do it.'
                },
                {
                    title: 'Plan your holidays in advance.',
                    description: 'Putting down when exactly you are going to go for a holiday keeps you focused because you know what to wait for, which makes you push twice as hard when the holiday approaches because you want to feel relaxed about work when you are on a holiday.'
                }
            ]
        },
        {
            categoryName: 'Train for adding value',
            suggestionList: [
                {
                    title: 'To-Do List',
                    description: 'List all the things you what to accomplish today. Return to this list every day; add new tasks and remove old ones.'
                },
                {
                    title: 'Morning Routine',
                    description: 'To avoid decision fatigue, script your morning routine as if you have handed it over to someone—he could actually do it step-by-step. Avoiding unnecessary decisions first thing in the morning leaves you with more decision point to spend later in the day. Keep adjusting this routine if necessary.'
                },
                {
                    title: 'Take note of the best news you read.',
                    description: 'Every day you read the news, and few days later, you don’t remember any of it. Summarise the most important ones every day and give new titles to them. Yes, it’s an effort, but it builds a good habit, and that is the basis of important things to come.'
                }
            ]
        },
        {
            categoryName: 'Shifting to business-related topics',
            suggestionList: [
                {
                    title: 'What groups do you belong to?',
                    description: 'How would you describe the groups that you belong to, and what are their specifics? Also, write a list of their annoying problems. Then list potential solutions.'
                },
                {
                    title: 'Being an expert is overrated.',
                    description: 'Take any topic and research it. Buy some books, read related blogs and start taking notes here—anything that makes you better than 80% of the population. Turn this research into a DVD and start selling it on Amazon or eBay. '
                }
            ]
        }
    ];
});