var CtrlNoteList = function ($scope, sharedDataAddNewGoal, $rootScope, pageManager, listIdea, listIdeaFull, cNote, time) {
  pageManager.setTitle('Notes');

  $scope.isIdeaDropdownActive = false;
  $scope.listIdea = listIdea;
  $scope.listIdeaFull = listIdeaFull;

  $scope.isDisabledBtnCreateNewNote = false;
  $scope.isHighlitedBtnCreateNewNote = false;
  $scope.isDisabledBtnEditNote = false;
  $scope.isHighlitedBtnEditNote = false;
  $scope.isDisabledBtnDeleteNote = false;
  $scope.isHighlitedBtnDeleteNote = false;

  highlightMenu('none');

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  sharedDataAddNewGoal.cNote = cNote;
  $scope.cNote = cNote;
  $scope.time = time;

  // Check list of ideas, how many of them is complete
  $scope.indexIdeaList = [];
  $scope.getIndexIdeaList = function () {
    $scope.indexIdeaList = $scope.cNote.JSON.map(function(mNote) {
      return mNote.indexIdea+'';
    });
  };
  $scope.getIndexIdeaList();


  $scope.isIdeaDone = function (indexCategory, indexIdea) {
    return $scope.indexIdeaList.indexOf(indexCategory+'/'+indexIdea) !== -1;
  };

  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.noteList
  //) {
  //
  //  $scope.isDisabledBtnCreateNewNote = false;
  //  $scope.isHighlitedBtnCreateNewNote = true;
  //  $scope.isDisabledBtnEditNote = false;
  //  $scope.isHighlitedBtnEditNote = false;
  //  $scope.isDisabledBtnDeleteNote = false;
  //  $scope.isHighlitedBtnDeleteNote = false;
  //  $scope.isDisabledBtnMenu = true;
  //  $scope.isHighlitedBtnMenu = false;
  //
  //  highlightMenu('none');
  //
  //  $rootScope.$emit('alert', {
  //    isTutorial: true,
  //    title: 'IP is the home of your Ideas',
  //    text: 'You need to capture ideas on a regular basis to manufacture IP. Later you will turn the best of these ideas into Plans. Now go ahead and take a note on an idea that\'s on your mind for a while now, the fact that you are using this app proves you\'ve got some :)',
  //    type: 'simple'
  //  });
  //
  //  sharedDataAddNewGoal.guide.noteList = false;
  //  sharedDataAddNewGoal.cGuide.check();
  //}
  //
  //if (sharedDataAddNewGoal.guide &&
  //    sharedDataAddNewGoal.guide.noteListNoteAdded &&
  //    cNote.JSON.length === 1
  //) {
  //  $scope.isDisabledBtnCreateNewNote = false;
  //  $scope.isHighlitedBtnCreateNewNote = false;
  //  $scope.isDisabledBtnEditNote = false;
  //  $scope.isHighlitedBtnEditNote = false;
  //  $scope.isDisabledBtnDeleteNote = false;
  //  $scope.isHighlitedBtnDeleteNote = false;
  //  $scope.isDisabledBtnMenu = false;
  //  $scope.isHighlitedBtnMenu = true;
  //
  //  highlightMenu('main');
  //
  //  $rootScope.$emit('alert', {
  //    isTutorial: true,
  //    title: 'Great!',
  //    text: 'Every time you have a new idea or would like to improve one, come back here and save it / upgrade it. Over time you will have enough IP captured to start turning it to a product. ..feel free to hang around and create more IP or go back to Hub.',
  //    type: 'simple'
  //  });
  //
  //  sharedDataAddNewGoal.guide.noteListNoteAdded = false;
  //  sharedDataAddNewGoal.guide.mainComfortChallenge = 'active';
  //  sharedDataAddNewGoal.cGuide.check();
  //}

  $scope.addNewNote = function (indexCategory, indexIdea) {
    if ($scope.isDisabledBtnCreateNewNote) {
      return;
    }

    if (typeof indexIdea !== "undefined") {
      location.hash = '#/note-add/new/'+indexCategory+'/'+indexIdea;
    } else {
      location.hash = '#/note-add/new';
    }
  };

  $scope.removeNote = function (id) {
    if ($scope.isDisabledBtnDeleteNote) {
      return;
    }

    $rootScope.$emit('alert', {
      title: 'DELETE',
      text: 'Do you really want to delete this IP?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.cNote.removeById('note', id, function() {
          $scope.getIndexIdeaList();
          $scope.$apply(function () {});
        });
      }
    });
  };

  $scope.goToNoteDetail = function (idNote) {
    if ($scope.isDisabledBtnEditNote) {
      return;
    }

    sharedDataAddNewGoal.mNote = cNote.getById(idNote);
    location.hash = '#/note-add/'+idNote;
  };

  $scope.html2text = function (html) {
    return html.replace(/<\/?[^>]+(>|$)/g, " ").replace('&nbsp;', '');
  };
};