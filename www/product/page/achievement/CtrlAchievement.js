var CtrlAchievement = function ($scope, pageManager, sharedDataAddNewGoal, cAchievement, cRewardAndSkill) {
  pageManager.setTitle('Achievements');

  $scope.cAchievement = cAchievement;
  $scope.cRewardAndSkill = cRewardAndSkill;
  $scope.profile = cRewardAndSkill.JSON[0];

  $scope.formatDate = function (date) {
    return moment(parseInt(date)).format("dddd MMM DD");
  };

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };
};