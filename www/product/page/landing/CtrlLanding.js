var CtrlLanding = function ($scope, pageManager, cUserGoal, time) {
  pageManager.setTitle('Entrepreneurs Planner');

  $scope.cUserGoal = cUserGoal;
  $scope.time = time;

  $rootScope.$emit('starting-app', {});
};