var CtrlTrackRecord = function ($scope, cUserGoal, $stateParams, sharedDataAddNewGoal, pageManager, cTrackRecord, time) {
  console.log('Ctrl Track Record is loaded');

  pageManager.setTitle('History');

  $scope.model = {};
  $scope.model.cUserGoal = cUserGoal;
  $scope.model.mUserGoal = cUserGoal.getById($stateParams.indexUserGoal);
  $scope.model.cTrackRecord = cTrackRecord;

  var listCStep = $scope.model.mUserGoal.cEpic.JSON.map(function (mEpic) {
    return mEpic.cStep;
  });

  var listIDStepCurrent = [];
  listCStep.forEach(function (cStep) {
    return cStep.JSON.forEach(function (mStep) {
      listIDStepCurrent.push(parseInt(mStep.id));
    });
  });

  var listTrackRecord = $scope.model.cTrackRecord.JSON.filter(function (mTrackRecord) {
    return listIDStepCurrent.indexOf(parseInt(mTrackRecord.idStep)) !== -1;
  });

  $scope.formatTime = function(timeElapsedSec) {
    var timeElapsedMin, timeElapsedMinRem, timeElapsedSecRem, timeElapsedHour;
    timeElapsedMin = Math.floor((timeElapsedSec) / 60),
      timeElapsedMinRem = timeElapsedMin % 60,
      timeElapsedSecRem = (timeElapsedSec) % 60,
      timeElapsedHour = Math.floor(timeElapsedMin / 60);

    var timeElapsed = '';

    if (timeElapsedHour) {
      timeElapsed += addZero(timeElapsedHour);
    } else {
      timeElapsed += '00'
    }

    if (timeElapsedMinRem) {
      timeElapsed += ':' + addZero(timeElapsedMinRem);
    } else {
      timeElapsed += ':00'
    }

    timeElapsed += ':' + addZero(timeElapsedSecRem);

    return timeElapsed;
  };

  var recordPrev = {},
      firstRecord = time.get().format('x');
      weekCurrent = -1;
  $scope.model.listTrackRecordMergeSameDays = [];
  listTrackRecord.forEach(function (record) {
    if (firstRecord > record.timeStart) {
      firstRecord = record.timeStart;
    }

    if (recordPrev.dayOfWeek === record.dayOfWeek &&
        weekCurrent === record.weekOfYear
    ) {
      recordPrev.duration = (parseInt(recordPrev.duration) + (parseInt(record.timeEnd) - parseInt(record.timeStart)))+'';
    } else {
      recordPrev = {
        timeStart: record.timeStart,
        timeEnd: record.timeEnd,
        dayOfWeek: record.dayOfWeek,
        weekOfYear: record.weekOfYear,
        duration: (parseInt(record.timeEnd) - parseInt(record.timeStart))+''
      };

      weekCurrent = record.weekOfYear;
      $scope.model.listTrackRecordMergeSameDays.push(recordPrev);
    }
  });

  var lastModay = moment(parseInt(firstRecord)).startOf('isoweek').day('monday').format('x'),
      today = time.get().format('x'),
      ctrMonday = lastModay;

  $scope.model.listWeek = [];
  while (ctrMonday < today) {
    var ctrModayNext = moment(parseInt(ctrMonday)).add(7, 'days').format('x');

    var listTrackRecordCurrentWeek = $scope.model.listTrackRecordMergeSameDays.filter(function (record) {
      return  record.timeStart > ctrMonday &&
              record.timeStart < ctrModayNext;
    });

    listTrackRecordCurrentWeek.reverse();

    var week = {
      start: ctrMonday,
      end: ctrModayNext,
      label: moment(parseInt(ctrMonday)).format('MMMM Do') + ' - ' + moment(parseInt(ctrModayNext)).format('YYYY MMMM Do'),
      listTrackRecordCurrentWeek: listTrackRecordCurrentWeek
    };

    $scope.model.listWeek.push(week);

    ctrMonday = ctrModayNext;
  }
  $scope.model.listWeek.reverse();

  $scope.getDuration = function (duration) {
    if (duration === 'NaN') {
      return 'In Progress';
    }

    return $scope.formatTime(parseInt(duration/1000));
  };

  $scope.getDate = function (timestamp) {
    return moment.unix(parseInt(timestamp/1000)).format('MMMM Do');
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  function addZero(digit) {
    if (digit < 10) {
      return '0'+digit;
    } else {
      return digit;
    }
  }

};