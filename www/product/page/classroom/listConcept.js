define(function () {
  return [
    {
      title: 'Habits & Daily Routine',
      description: '"People Are Habit Machines"<br>- Naval Ravikant',
      link: '#/concept/habit-and-routine'
    },
    {
      title: 'Intellectual Property',
      description: '"Every empire builder needs to endlessly explore ideas, projects, goals and targets and keep track of important stories." - Daniel Priestley',
      link: '#/concept/intellectual-property',
      free: false,
      price: 25,
      purchased: true
    },
    {
      title: 'Comfort Challenge',
      description: '"What you really need lays just outside your comfort zone." - Unknown',
      link: '#/concept/comfort-challenge',
      free: false,
      price: 50,
      purchased: false
    },
    {
      title: "Don't Wait for All the Lights to Turn Green",
      description: 'One of the biggest mistakes you can make is to wait for the right moment to start. ..that moment will never come!',
      free: false,
      price: 25,
      purchased: true
    },
    {
      title: 'Functional vs. Valuable Work',
      description: '"If you work hard that means you are doing something functional not something valuable." - Daniel Priestley',
      free: false,
      price: 50,
      purchased: false
    }
  ];
});