var CtrlClassroom = function ($scope, pageManager, sharedDataAddNewGoal, time, cConcept, listConcept, cRewardAndSkill) {
  pageManager.setTitle('Classroom');

  $scope.time = time;
  $scope.cConcept = cConcept;
  $scope.listConcept = listConcept;
  $scope.cRewardAndSkill = cRewardAndSkill;
  $scope.mRewardAndSkill = $scope.cRewardAndSkill.JSON[0];

  console.log(cRewardAndSkill);

  // If there is no comfort challenge in the database database will be hydrated.
  if (cConcept.JSON.length === 0) {
    cConcept.addArray($scope.listConcept, function () {
      $scope.$apply(function () {});
    });
  }

  // Settings: Set default values for tut settings
  $scope.isDisabledListItemHabitAndRoutine = false;
  $scope.isHighlitedListItemHabitAndRoutine = false;
  $scope.isDisabledListItemIntellectualProperty = false;
  $scope.isHighlitedListItemIntellectualProperty = false;
  $scope.isDisabledListItemComfortChallenge = false;
  $scope.isHighlitedListItemComfortChallenge = false;
  $scope.isDisabledListItemOther1 = false;
  $scope.isHighlitedListItemOther1 = false;
  $scope.isDisabledListItemOther2 = false;
  $scope.isHighlitedListItemOther2 = false;
  $scope.isDisabledBtnBack = false;
  $scope.isHighlitedBtnBack = false;

  $scope.goToPageIf = function (condition, page) {
    // Condition is "isDisabledX"
    if ($scope[condition]) {
      return;
    }

    console.log('$scope['+condition+']', $scope[condition]);

    if (page !== 'back') {
      location.hash = page;
    } else {
      console.log('history.go(-1);');
      history.go(-1);
    }

  };

  $scope.purchaseConcept = function (mConcept) {
    mConcept.purchased = true;
    $scope.mRewardAndSkill.credit -= mConcept.price;

    $scope.cConcept.check(function () {
      $scope.cRewardAndSkill.check(function () {
        $rootScope.$emit('alert', {
          title: "IT'S YOURS NOW",
          text: 'Have a good time reading it!',
          type: 'simple'
        });

        $rootScope.$emit('profile.credit.deducted', {});
      });
    });
  };

  $scope.goToLink = function (mConcept) {
    if (mConcept.free || mConcept.purchased) {
      location.hash = mConcept.link;
    } else {
      // console.log(parseInt(mConcept.price) + ' < ' + parseInt($scope.mRewardAndSkill.credit));
      $rootScope.$emit('alert', {
        title: 'Get it for ' + mConcept.price + ' CRD',
        text: 'This concept costs ' + mConcept.price + ' CRD to read. Would you like to buy it?',
        type: 'cancelOk',
        fnOk: function () {
          if (parseInt($scope.mRewardAndSkill.credit) < parseInt(mConcept.price)) {
            setTimeout(function () {
              $rootScope.$emit('alert', {
                title: "NOT ENOUGH CREDIT",
                text: 'You don\'t have enough credit: ' + $scope.mRewardAndSkill.credit + ' CRD. To earn more CRD: create IP or do Daily Priority and Daily Review or complete a Comfort Challenge.',
                type: 'simple'
              });
            }, 1000);
          } else {
            $scope.purchaseConcept(mConcept);
          }
        }
      });
    }
  };

  // if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.classroomMain) {
  //
  //   // Disable & Highlight buttons for tutorial
  //   highlightMenu('disabled');
  //   $scope.isDisabledListItemHabitAndRoutine = false;
  //   $scope.isHighlitedListItemHabitAndRoutine = true;
  //   $scope.isDisabledListItemIntellectualProperty = true;
  //   $scope.isHighlitedListItemIntellectualProperty = false;
  //   $scope.isDisabledListItemComfortChallenge = true;
  //   $scope.isHighlitedListItemComfortChallenge = false;
  //   $scope.isDisabledListItemOther1 = true;
  //   $scope.isHighlitedListItemOther1 = false;
  //   $scope.isDisabledListItemOther2 = true;
  //   $scope.isHighlitedListItemOther2 = false;
  //   $scope.isDisabledBtnBack = true;
  //   $scope.isHighlitedBtnBack = false;
  //
  //   // Turn off completed Tut.
  //   sharedDataAddNewGoal.guide.classroomMain = false;
  //   // Turn on next Tut.
  //   sharedDataAddNewGoal.guide.conceptHabitAndRoutine = true;
  //   // Save Tut. changes
  //   sharedDataAddNewGoal.cGuide.check();
  //
  //   $rootScope.$emit('alert', {
  //     isTutorial: true,
  //     title: 'Welcome to Classroom',
  //     text: 'You can learn here what new ideas you have to install and what old ideas you have to get rid of to thrive as an entrepreneur. Now Select the first concept: Habits & Daily Routine',
  //     type: 'simple'
  //   });
  // }
  //
  // if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.classroomIntellectualProperty) {
  //
  //   // Disable & Highlight buttons for tutorial
  //   highlightMenu('disabled');
  //   $scope.isDisabledListItemHabitAndRoutine = true;
  //   $scope.isHighlitedListItemAndRoutine = false;
  //   $scope.isDisabledListItemIntellectualProperty = false;
  //   $scope.isHighlitedListItemIntellectualProperty = true;
  //   $scope.isDisabledListItemComfortChallenge = true;
  //   $scope.isHighlitedListItemComfortChallenge = false;
  //   $scope.isDisabledListItemOther1 = true;
  //   $scope.isHighlitedListItemOther1 = false;
  //   $scope.isDisabledListItemOther2 = true;
  //   $scope.isHighlitedListItemOther2 = false;
  //   $scope.isDisabledBtnBack = true;
  //   $scope.isHighlitedBtnBack = false;
  //
  //   $rootScope.$emit('alert', {
  //     isTutorial: true,
  //     title: 'Welcome back to Classroom',
  //     text: 'Come back here whenever you are ready to learn new, practical concepts and start applying them. Now take a look at the second one.',
  //     type: 'simple'
  //   });
  //
  //   // Turn off completed Tut.
  //   sharedDataAddNewGoal.guide.classroomIntellectualProperty = false;
  //   // Turn on next Tut.
  //   sharedDataAddNewGoal.guide.conceptIntellectualProperty = true;
  //   // Save Tut. changes
  //   sharedDataAddNewGoal.cGuide.check();
  // }
  //
  // if (sharedDataAddNewGoal.guide &&
  //     sharedDataAddNewGoal.guide.classroomComfortChallenge) {
  //
  //   highlightMenu('disabled');
  //
  //   $scope.isDisabledListItemIntellectualProperty = true;
  //   $scope.isHighlitedListItemIntellectualProperty = false;
  //   $scope.isDisabledListItemComfortChallenge = false;
  //   $scope.isHighlitedListItemComfortChallenge = true;
  //   $scope.isDisabledListItemOther1 = true;
  //   $scope.isHighlitedListItemOther1 = false;
  //   $scope.isDisabledListItemOther2 = true;
  //   $scope.isHighlitedListItemOther2 = false;
  //   $scope.isDisabledBtnBack = true;
  //   $scope.isHighlitedBtnBack = false;
  //
  //   sharedDataAddNewGoal.guide.classroomComfortChallenge = false;
  //   sharedDataAddNewGoal.guide.conceptComfortChallenge = true;
  //   sharedDataAddNewGoal.cGuide.check();
  // }
};