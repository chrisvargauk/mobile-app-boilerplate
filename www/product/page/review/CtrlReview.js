var CtrlReview = function ($scope, cUserGoal, pageManager, $stateParams) {
  pageManager.setTitle('Review');

  $scope.model = {};
  $scope.model.cUserGoal = cUserGoal;
  $scope.model.mUserGoal = cUserGoal.getById($stateParams.idUserGoal);
  $scope.model.cReview = $scope.model.mUserGoal.cReview;
  $scope.model.mReview = $scope.model.cReview.getById($stateParams.idReview);

  var listReviewEarlier = [];
  // List earlier Reviews
  $scope.model.cReview.JSON.forEach(function (mReviewIter) {
    if (parseInt(mReviewIter.scheduled) < parseInt($scope.model.mReview.scheduled)) {
      listReviewEarlier.push(mReviewIter);
    }
  });

  var mReviewLastOne = {
    scheduled: 0
  };
  // Find the latest Review among the earlier ones?
  listReviewEarlier.forEach(function (mReviewIter) {
    if (parseInt(mReviewLastOne.scheduled) < parseInt(mReviewIter.scheduled)) {
      mReviewLastOne = mReviewIter;
    }
  });

  $scope.model.mReviewLastOne = mReviewLastOne;

  console.log('$stateParams.idReview', $stateParams.idReview);
  console.log('mReview', $scope.model.mReview);

  $scope.back = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  $scope.done = function (idUserGoal) {
    $scope.model.mReview.isDone = true;
    $scope.model.cReview.check();
    location.hash = '#/goal-details/'+idUserGoal;
  };

  $scope.goToReviewDetail = function (idReview) {
    // sharedDataAddNewGoal.mNote = cNote.getById(idNote);
    location.hash = '#/review/'+$scope.model.mUserGoal.id+'/'+idReview;
  };

  $scope.formatDate = function (timestampStr) {
    return moment(parseInt(timestampStr)).format('LLLL');
  };

  function autofocus() {
    setTimeout(function () {
      document.querySelector('textarea').focus();
    }, 500);
  }

  autofocus();
};