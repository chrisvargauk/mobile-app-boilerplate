define(function () {
  var directiveProgressbar = function () {
    return {
      restriction: 'E',
      templateUrl: 'product/progressbar/progressbar-tmpl.html',
      scope: {
        mUserGoal: '='
      },
      controller: function ($scope) {
        $scope.noStepDone = 0;
        var complexityDone = 0;
        var complecityTotal = 0;
        $scope.mUserGoal.cEpic.JSON.forEach(function (mEpic) {
          mEpic.cStep.JSON.forEach(function (mStep) {
            var comlexity = parseInt(mStep.complexity);
            if (mStep.status === 'done' &&
              comlexity !== -1) {
              complexityDone += comlexity;
            }

            if (comlexity !== -1) {
              complecityTotal += comlexity;
            }
          });
        });

        $scope.getBackgroundColor = function (index) {
          if ((index * 10) < $scope.barHeight) {
            return '#3399ff';
          } else {
            return 'none';
          }
        };

        if (complecityTotal) {
          $scope.barHeight = Math.round(complexityDone / (complecityTotal / 100));
        } else {
          $scope.barHeight = 0;
        }
      }
    };

  };

  return directiveProgressbar;
});
