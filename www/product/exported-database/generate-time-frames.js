var format = "YYYY-MM-DD HH:mm",
  timeFrames = [
    {
      start: '2015-04-05 9:30',
      end: '2015-04-05 11:40',
      isRead: false
    },
    {
      start: '2015-04-06 9:00',
      end: '2015-04-06 11:30',
      isRead: false
    },
    {
      start: '2015-04-07 9:00',
      end: '2015-04-07 12:30',
      isRead: false
    },
    {
      start: '2015-04-08 9:10',
      end: '2015-04-08 12:00',
      isRead: false
    },
    {
      start: '2015-04-09 9:10',
      end: '2015-04-09 12:00',
      isRead: false
    },

    {
      start: '2015-04-12 9:30',
      end: '2015-04-12 11:40',
      isRead: false
    },
    {
      start: '2015-04-13 9:00',
      end: '2015-04-13 11:30',
      isRead: false
    },
    {
      start: '2015-04-14 9:00',
      end: '2015-04-14 12:30',
      isRead: false
    },
    {
      start: '2015-04-15 9:10',
      end: '2015-04-15 12:00',
      isRead: false
    },
    {
      start: '2015-04-16 9:10',
      end: '2015-04-16 12:00',
      isRead: false
    },

    {
      start: '2015-04-19 9:30',
      end: '2015-04-19 11:40',
      isRead: false
    },
    {
      start: '2015-04-20 9:00',
      end: '2015-04-20 11:30',
      isRead: false
    },
    {
      start: '2015-04-21 9:00',
      end: '2015-04-21 12:30',
      isRead: false
    },
    {
      start: '2015-04-22 9:10',
      end: '2015-04-22 12:00',
      isRead: false
    },
    {
      start: '2015-04-23 9:10',
      end: '2015-04-23 12:00',
      isRead: false
    },

    {
      start: '2015-04-26 9:30',
      end: '2015-04-26 11:40',
      isRead: false
    },
    {
      start: '2015-04-27 9:00',
      end: '2015-04-27 11:30',
      isRead: false
    },
    {
      start: '2015-04-28 9:00',
      end: '2015-04-28 12:30',
      isRead: false
    },
    {
      start: '2015-04-29 9:10',
      end: '2015-04-29 12:00',
      isRead: false
    },
    {
      start: '2015-04-30 9:10',
      end: '2015-04-30 12:00',
      isRead: false
    },

    {
      start: '2015-05-03 9:00',
      end: '2015-05-03 12:30',
      isRead: false
    },
    {
      start: '2015-05-04 9:10',
      end: '2015-05-04 12:00',
      isRead: false
    },
    {
      start: '2015-05-05 9:10',
      end: '2015-05-05 12:00',
      isRead: false
    },
    {
      start: '2015-05-06 9:30',
      end: '2015-05-06 11:40',
      isRead: false
    },
    {
      start: '2015-05-07 9:00',
      end: '2015-05-07 11:30',
      isRead: false
    },
    {
      start: '2015-05-08 9:00',
      end: '2015-05-08 12:30',
      isRead: false
    },
    {
      start: '2015-05-09 9:10',
      end: '2015-05-09 12:00',
      isRead: false
    },

    {
      start: '2015-05-10 9:10',
      end: '2015-05-10 12:00',
      isRead: false
    },
    {
      start: '2015-05-11 9:30',
      end: '2015-05-11 11:40',
      isRead: false
    },
    {
      start: '2015-05-12 9:00',
      end: '2015-05-12 11:30',
      isRead: false
    },
    {
      start: '2015-05-15 9:00',
      end: '2015-05-15 12:30',
      isRead: false
    },
    {
      start: '2015-05-16 9:10',
      end: '2015-05-16 12:00',
      isRead: false
    },
    {
      start: '2015-05-16 9:10',
      end: '2015-05-16 12:00',
      isRead: false
    },

    {
      start: '2015-05-17 9:30',
      end: '2015-05-17 11:40',
      isRead: false
    },
    {
      start: '2015-05-18 9:00',
      end: '2015-05-18 11:30',
      isRead: false
    },
    {
      start: '2015-05-21 9:00',
      end: '2015-05-21 12:30',
      isRead: false
    },
    {
      start: '2015-05-22 9:10',
      end: '2015-05-22 12:00',
      isRead: false
    },
    {
      start: '2015-05-23 9:10',
      end: '2015-05-23 12:00',
      isRead: false
    },

    {
      start: '2015-05-24 9:30',
      end: '2015-05-24 11:40',
      isRead: false
    },
    {
      start: '2015-05-25 9:00',
      end: '2015-05-25 11:30',
      isRead: false
    },
    {
      start: '2015-05-28 9:00',
      end: '2015-05-28 12:30',
      isRead: false
    },
    {
      start: '2015-05-29 9:10',
      end: '2015-05-29 12:00',
      isRead: false
    },
    {
      start: '2015-05-30 9:10',
      end: '2015-05-30 12:00',
      isRead: false
    },


    {
      start: '2015-05-31 9:30',
      end: '2015-05-31 11:40',
      isRead: false
    },
    {
      start: '2015-06-01 9:00',
      end: '2015-06-01 11:30',
      isRead: false
    },
    {
      start: '2015-06-02 9:00',
      end: '2015-06-02 11:30',
      isRead: false
    },
    {
      start: '2015-06-03 9:00',
      end: '2015-06-03 11:30',
      isRead: false
    },
    {
      start: '2015-06-04 9:00',
      end: '2015-06-04 11:30',
      isRead: false
    },

    {
      start: '2015-06-07 9:00',
      end: '2015-06-07 11:30',
      isRead: false
    },
  ],
  week = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
  ];

var cTrackRecord;
var cUserGoal = new Collection({
  type: 'userGoal',
  callback: function () {
    cTrackRecord = cUserGoal.getById(2).cTrackRecord;
    cTrackRecord.emptyWebSQL(undefined, generateTimeFramse);
  }
});

function generateTimeFramse() {
  timeFrames.forEach(function (timeFrame) {
    if (timeFrame.isRead) {
      return;
    }

    timeFrame.isRead = true;

    cTrackRecord.add({
      timeStart:  moment(timeFrame.start, format).format('x'),
      timeEnd:    moment(timeFrame.end, format).format('x'),
      weekOfYear: moment(timeFrame.end, format).isoWeek(),
      dayOfWeek:  week[moment(timeFrame.end, format).day()]
    }, generateTimeFramse);
  });
}