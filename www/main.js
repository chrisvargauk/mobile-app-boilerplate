require.config({
  paths: {
    'ng': 'lib/vendor/angular',
    'ngSanitize': 'lib/vendor/angular-sanitize',
    'underscore': 'lib/vendor/underscore',
    'uiRouter': 'lib/vendor/angular-ui-router',
    'moment': 'lib/vendor/moment',
    'websql': "lib/websql",
    'Collection': 'lib/angularjs-collection',
    'onsen': "lib/vendor/onsen/js/onsenui",
    'pageManager': 'module/sliding-menu/page-manager',
    'CtrlApp': 'ctrl-app',
    'directiveAlert': 'module/alert/directive-alert',
    'directiveTime': 'module/time/directive-time',
    'directiveNgSelect': 'module/ng-select/directive-ng-select',
    'directiveProgressbar': 'product/progressbar/directive-progressbar',
    'directiveSchedule': 'product/page/home/directive-schedule',
    'directiveGetStructuredWidget': 'module/get-structured-widget/directive-get-structured-widget',
    'directiveMiniReviewWidget': 'module/mini-review-widget/directive-mini-review-widget',
    'deferredListCollection': 'product/page/add-new-goal/partial/deferred-list-collection',
    'directiveQuote': 'module/quote/directive-quote',
    'directiveHabitStreak': 'module/habit-streak/directive-habit-streak',
    'directiveProfileWidget': 'module/profile-widget/directive-profile-widget',
    'directiveRewardPopup': 'module/reward-popup/directive-reward-popup',
    'directiveStrength': 'product/page/note/directive-strength',
    'directiveStepDateAndTime': 'product/page/add-new-goal/partial/directive-step-date-and-time-spent',
    'directiveTrackTimeArchiveList': 'product/page/add-new-goal/partial/directive-track-time-archive-list',
    'directiveEstimate': 'product/page/goal-detail/directive-estimate',

    'directiveManageTables': 'product/page/dev/directive-manage-tables',

    'profileService': 'module/profile-srv/profile-srv',
    'timeService': 'module/time-srv/time-srv',
    'settingService': 'module/setting-srv/setting-srv',
    'trackTimeService': 'module/track-time-service/track-time-srv',

    'CtrlHome': 'product/page/home/CtrlHome',
    'CtrlIntroduction': 'product/page/add-new-goal/partial/CtrlIntroduction',
    'CtrlListReason': 'product/page/add-new-goal/partial/CtrlListReason',
    'CtrlDefinition': 'product/page/add-new-goal/partial/CtrlDefinition',
    'CtrlListStep': 'product/page/add-new-goal/partial/CtrlListStep',
    'CtrlListStepDetailed': 'product/page/add-new-goal/partial/CtrlListStepDetailed',
    'CtrlListTimeframe': 'product/page/add-new-goal/partial/CtrlListTimeframe',
    'CtrlGoalDetail': 'product/page/goal-detail/CtrlGoalDetail',
    'CtrlTrackRecord': 'product/page/track-record/CtrlTrackRecord',
    'CtrlReview': 'product/page/review/CtrlReview',
    'CtrlSetting': 'product/page/setting/CtrlSetting',
    'CtrlAbout': 'product/page/about/CtrlAbout',
    'CtrlConceptIntroduction': 'product/page/concept/CtrlConceptIntroduction',
    'CtrlConceptReasons': 'product/page/concept/CtrlConceptReasons',
    'CtrlConceptRewards': 'product/page/concept/CtrlConceptRewards',
    'CtrlConceptSepofspace': 'product/page/concept/CtrlConceptSepofspace',
    'CtrlConceptSteps': 'product/page/concept/CtrlConceptSteps',
    'CtrlConceptTimeframe': 'product/page/concept/CtrlConceptTimeframe',
    'CtrlConceptComfortChallenge': 'product/page/concept/CtrlConceptComfortChallenge',
    'CtrlConceptHabitAndRoutine': 'product/page/concept/CtrlConceptHabitAndRoutine',
    'CtrlClassroom': 'product/page/classroom/CtrlClassroom',
    'CtrlComfortChallenge': 'product/page/comfort-challenge/CtrlComfortChallenge',
    'CtrlComfortChallengeDetail': 'product/page/comfort-challenge/CtrlComfortChallengeDetail',
    'CtrlNoteList': 'product/page/note/CtrlNoteList',
    'CtrlNoteSuggestionList': 'product/page/note/CtrlNoteSuggestionList',
    'CtrlNoteAdd': 'product/page/note/CtrlNoteAdd',
    'CtrlIntellectualProperty': 'product/page/concept/CtrlIntellectualProperty',
    'CtrlMain': 'product/page/main/CtrlMain',
    'CtrlGetStructured': 'product/page/get-structured/CtrlGetStructured',
    'CtrlMiniReview': 'product/page/mini-review/CtrlMiniReview',
    'CtrlAchievement': 'product/page/achievement/CtrlAchievement',
    'CtrlDev': 'product/page/dev/CtrlDev',
    'CtrlDaily': 'product/page/daily/CtrlDaily',
    'CtrlLanding': 'product/page/landing/CtrlLanding',

    'listIdea': 'product/page/note/listIdea',
    'listIdeaFull': 'product/page/note/listIdeaFull',
    'listComfortChallenge': 'product/page/comfort-challenge/listComfortChallenge',
    'listConcept': 'product/page/classroom/listConcept',

    'util': 'product/util',

    'resolveReward': 'product/page/add-new-goal/partial/resolveReward',

    'scenarioRunner': 'lib/scenarioRunner',
    'testRunnerScenario': 'test/bd/test-runner-scenario',
    'testRunnerUnit': 'test/unit-test/test-runner-unit',
    'CtrlTestRunner': 'www/test/bd/CtrlTestRunner'
  }
  //waitSeconds: 0
});

define(['app',
      //'testRunnerScenario',
      //'testRunnerUnit'
  ], function () {
    'use strict';

    angular.bootstrap(document, ['app']);
});