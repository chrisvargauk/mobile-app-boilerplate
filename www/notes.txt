# Compile Sass on Mac
sass --watch --debug-info --scss --sourcemap ./www:./www/stylesheet/css

# Compile Sass on Linux
sass --watch --debug-info --scss ./:./stylesheet/css --cache-location ./stylesheet/.sass-cache

# PhoneGap
https://build.phonegap.com

If you need guestures back, uncomments the following lines in onsenui.js
window.FastClick = FastClick;

localStorage.userPropCredit = '0';
websql.emptyTable('collectionType')
websql.deleteTable('collectionType')
websql.deleteTable('c_userGoal')
websql.deleteTable('c_trackRecord')
websql.deleteTable('c_timeAndLocation')
websql.deleteTable('c_step')
websql.deleteTable('c_review')
websql.deleteTable('c_reason')
websql.deleteTable('c_note')
websql.deleteTable('c_guide')
websql.deleteTable('c_comfortChallenge')
websql.deleteTable('c_getStructured')
websql.deleteTable('c_dailyPriority')

Todo:
- Time Allocation
  Allocate time and place for working on your goal. Weekly basis like
  Weekly Time-frames
  "Monday 19:00 - 21:00 - 2h"
  "Wednesday 19:00 - 21:00 - 2h"
  "Sunday 16:00 - 20:00 - 4h"
  ----------------------
  Total time allocated: 8h

  Goal details page
  - Then Start stop button needs to be added.
  - Time spent tracking section
    - Progress per time alloc could be shown

  Home Page
  - Time spent tracking section
      - Progress per time alloc could be shown. Total Weekly Time Alloc / Time Spent
- Other:
    - Take off border-bottom from ons-list
    - Make Sure methods are added to Controllers not to scope
- Home page
    -
- Goal Details Page
