define(function ($http) {
  return function($q, $rootScope) {
    var deferred = $q.defer(),
        tickCallback,
        mStepCurrent,
        cTrackRecord,
        cUserGoal;

    var model = {
          tracking: false,
          mStepCurrent: undefined,
          cTrackRecord: undefined,
    };

    var listWeek = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
    ];

    var getCurrentTrackRecord = function (idStep) {
      var mTrackRecordLast, mTrackRecordPrev;

      if (typeof idStep === 'undefined') {
        console.error('idStep is undefined. You need to provide it!');
        return {};
      }

      if (model.listTrackRecordCurrent.length > 0) {
        mTrackRecordLast = model.listTrackRecordCurrent[(model.listTrackRecordCurrent.length - 1)];
      }

      if (model.listTrackRecordCurrent.length > 1) {
        mTrackRecordPrev = model.listTrackRecordCurrent[(model.listTrackRecordCurrent.length - 2)];
      }

      return {
        last: mTrackRecordLast,
        prev: mTrackRecordPrev
      };
    };

    function calcTimeElapsedTotalBase() {
      // Add up the time user spent on give task
      var timeElapsedTotalBase = 0;
      model.listTrackRecordCurrent.forEach(function (mTrackRecord) {
        if (mTrackRecord.timeEnd !== "timestamp") {
          var diff = Math.round(mTrackRecord.timeEnd / 1000 - mTrackRecord.timeStart / 1000);
          timeElapsedTotalBase += parseInt(diff);
          // console.log('diff', diff);
        }
      });

      return timeElapsedTotalBase;
    }

    function getIsoDay() {
      return (timeService.get().day() + 6) % 7;
    }

    function calcTotalTimeElapsed(trackRecordForTotal) {
      var timeElapsedSec,
          timeElapsedTotal = 0;
      
      if (trackRecordForTotal.last) {
        timeElapsedSec = Math.round((parseInt(timeService.get().format('x')) - parseInt(trackRecordForTotal.last.timeStart)) / 1000)
      } else if (trackRecordForTotal.prev) {
        timeElapsedSec = Math.round((parseInt(timeService.get().format('x')) - parseInt(trackRecordForTotal.prev.timeStart)) / 1000)
      }

      if (trackRecordForTotal.last.timeEnd === 'timestamp') {
        timeElapsedTotal += model.timeElapsedTotalBase + timeElapsedSec;
      } else {
        timeElapsedTotal += model.timeElapsedTotalBase;
      }

      return timeElapsedTotal;
    }

    var timer = function () {
      // track time only on Goal Details view
      if (location.hash.indexOf('goal-details') === -1 &&
          location.hash.indexOf('steps-detailed') === -1
      ) {
        return;
      }

      var trackRecordCurrent = getCurrentTrackRecord(model.mStepCurrent.id);
      var timeElapsed;

      if (typeof trackRecordCurrent.last === 'undefined' &&
        typeof trackRecordCurrent.prev === 'undefined') {
        console.log('No time spent on current step yet');
      } else {
        timeElapsed = calcTotalTimeElapsed(trackRecordCurrent);

        // console.log('timeElapsed', timeElapsed);

        if (tickCallback) {
          tickCallback(timeElapsed, model.tracking);
        }

        $rootScope.$emit('directiveStepDateAndTimeSpent.set', {
          idMStep: model.mStepCurrent.id,
          timeElapsed: timeElapsed
        });
      }

      if (model.tracking) {
        model.setTimeout = setTimeout(timer, 1000);
      }

      return timeElapsed;
    };

    // Initialize default variables
    var setDefaults = function (mStepCurrentInput) {
      model = {};
      model.tracking = false;
      model.mStepCurrent = mStepCurrentInput;
      // list of track records for current steps
      model.listTrackRecordCurrent = cTrackRecord.JSON.filter(function (mTrackRecord) {
        return parseInt(mTrackRecord.idStep) === parseInt(mStepCurrentInput.id);
      });
      model.trackRecordLastTwo = getCurrentTrackRecord(mStepCurrentInput.id);
      model.timeElapsedTotalBase = calcTimeElapsedTotalBase();
    };

    var init = function (mStepCurrentInput, tickCallbackInput) {
      if (typeof mStepCurrentInput === 'undefined') {
        console.error('idStep is undefined. You need to provide it!');
        return;
      }

      clearTimeout(model.setTimeout);

      tickCallback = tickCallbackInput;

      setDefaults(mStepCurrentInput);

      if (model.trackRecordLastTwo.last && model.trackRecordLastTwo.last.timeEnd === 'timestamp') {
        model.tracking = true;
      } else {
        model.tracking = false;
      }

      timer();
    };

    var start = function (mStepCurrentInput) {
      if (typeof mStepCurrentInput === 'undefined') {
        console.error('idStep is undefined. You need to provide it!');
        return;
      }

      setDefaults(mStepCurrentInput);

      // If there is no running tracking
      if ( (model.trackRecordLastTwo && typeof model.trackRecordLastTwo.last === 'undefined') ||  // No time period is not recorded yet
           (model.trackRecordLastTwo && model.trackRecordLastTwo.last.timeEnd !== 'timestamp')    // Last time period is complete
      ) {
        // New time period tracking obj
        var mTrackRecordCurrent = {
          idStep: mStepCurrentInput ? mStepCurrentInput.id : '-1',
          timeStart: timeService.get().format('x'),
          timeEnd: 'timestamp',
          weekOfYear: timeService.get().isoWeek(),
          dayOfWeek: listWeek[(getIsoDay())]
        };

        cTrackRecord.add(mTrackRecordCurrent, function () {
          setDefaults(mStepCurrentInput);
          model.tracking = true;
          timer();

          $rootScope.$emit('trackTimeService.timerStarted', {
            idMStep: model.mStepCurrent.id
          });
        });
      } else {
          model.tracking = true;
          timer();

          $rootScope.$emit('trackTimeService.timerStarted', {
            idMStep: model.mStepCurrent.id
          });
      }
    };

    var stop = function (mStepCurrentInput) {
      if (typeof mStepCurrentInput === 'undefined') {
        console.error('idStep is undefined. You need to provide it!');
        return;
      }

      var trackRecordLastTwo = getCurrentTrackRecord(mStepCurrentInput.id);

      if (trackRecordLastTwo && typeof trackRecordLastTwo.last === 'undefined') {
        console.warn("Unexpected case: trackRecordLastTwo.last === 'undefined'");
        return;
      }

      trackRecordLastTwo.last.timeEnd = timeService.get().format('x');
      cTrackRecord.check(function () {
        setDefaults(mStepCurrentInput);
        model.tracking = false;
      });
    };

    var getTrackRecordListByStepId = function (idStep) {
      return cTrackRecord.JSON.filter(function (mTrackRecord) {
        return parseInt(mTrackRecord.idStep) === parseInt(idStep);
      });
    };

    var check = function (mStepCurrentInput, cStep, scope, callback) {
      cTrackRecord.check(function () {
        setDefaults(mStepCurrentInput);
        var timeElapsed = timer();

        mStepCurrentInput.timeElapsedSecond = timeElapsed;

        if (typeof cStep.check === 'function') {
          cStep.check(function () {
            if (scope) {
              scope.$apply(function() {});
            }

            if (callback) {
              callback();
            }
          });
        } else {
          var ctrCheckInProgress = 0,
              ctrCheckDone = 0;
          cStep.forEach(function (cStepItem) {
            cStepItem.check(function () {
              if (scope) {
                scope.$apply(function() {});
              }

              ctrCheckDone += 1;

              if (callback && ctrCheckDone === ctrCheckInProgress) {
                callback();
              }
            });
            ctrCheckInProgress++;
          });
        }
      });
    };
    
    var getCurrentListCTrackRecord = function (mStepCurrentInput) {
      return cTrackRecord.JSON.filter(function (mTrackRecord) {
        return parseInt(mTrackRecord.idStep) === parseInt(mStepCurrentInput.id);
      });
    };

    var getIDStepInProgress = function () {
      if (model.mStepCurrent) {
        return model.mStepCurrent.id;  
      }
    };
    
    var isTracking = function () {
      return model.tracking;
    };

    var getTotalComplexityOfCompleted = function (idUserGoal, callback) {
      var mUserGoal;

      function init() {
        if (!idUserGoal) {
          console.warn('Track Time Servie: getTotalComplexityOfCompleted(..): idUserGoal is undefined');
          return;
        }

        mUserGoal = cUserGoal.getById(idUserGoal);

        // Look through all epics for step collections
        var mStepList = [];
        mUserGoal.cEpic.JSON.forEach(function (mEpic) {
          mStepList = mStepList.concat(mEpic.cStep.JSON);
        });

        // Look through all mSteps for total complexity of completed
        var totalComplexityOfCompleted = 0,
            totalSecondSpentOnCompleted = 0,
            totalComplexityOfNotCompleted = 0;
            costTotal = 0;
            costCompleted = 0;
        mStepList.map(function (mStep) {
          var complexity = parseInt(mStep.complexity);
          var timeElapsedSecond = parseInt(mStep.timeElapsedSecond);

          if (mStep.status === 'done' &&
              complexity !== -1) 
          {
            totalComplexityOfCompleted += complexity;

            if (timeElapsedSecond !== -1) {
              totalSecondSpentOnCompleted += timeElapsedSecond;
            }
          } else if (complexity !== -1) {
            totalComplexityOfNotCompleted += complexity;
          }

          // Cost calc
          var cost = parseInt(mStep.cost);
          if (mStep.status === 'done') {

            if (cost > 0) {
              costCompleted += cost;
            }
          }

          if (cost > 0) {
            costTotal += cost;
          }
        });

        if (callback) {
          var velocityPerSecond = totalComplexityOfCompleted/totalSecondSpentOnCompleted;

          callback({
            totalComplexityOfCompleted: totalComplexityOfCompleted,
            totalSecondSpentOnCompleted: totalSecondSpentOnCompleted,
            velocityPerSecond: totalComplexityOfCompleted/totalSecondSpentOnCompleted,
            totalComplexityOfNotCompleted: totalComplexityOfNotCompleted,
            deadlineInSecond: totalComplexityOfNotCompleted/velocityPerSecond,
            costTotal: costTotal,
            costCompleted: costCompleted,
          });
        }
      };

      cUserGoal.reload(function (cUserGoalInput) {
        cUserGoal = cUserGoalInput;
        console.log('cUserGoal REloaded', cUserGoal);

        init();
      });
    };

    var removeTrackRecordByIdStep = function (id, callback) {
      var listDeferred = [];
      cTrackRecord.JSON.forEach(function (mTrackRecord) {
        if ( parseInt(mTrackRecord.idStep) == parseInt(id)) {
          var deferred = $q.defer();
          listDeferred.push(deferred);
          cTrackRecord.removeById('trackRecord', mTrackRecord.id, function () {
            deferred.resolve();
          })
        }
      });
      $q.all(listDeferred).then(callback);
    };

    var api = {
      init: init,
      start: start,
      stop: stop,
      getCurrentTrackRecord: getCurrentTrackRecord,
      getTrackRecordListByStepId: getTrackRecordListByStepId,
      check: check,
      getCurrentListCTrackRecord: getCurrentListCTrackRecord,
      getIDStepInProgress: getIDStepInProgress,
      isTracking: isTracking,
      getTotalComplexityOfCompleted: getTotalComplexityOfCompleted,
      removeTrackRecordByIdStep: removeTrackRecordByIdStep
    };

    collectionFactory.get('cTrackRecord', {
      type: 'trackRecord',
      callback: function (cTrackRecordInput) {
        cTrackRecord = cTrackRecordInput;
        console.log('cTrackRecord loaded', cTrackRecord);

        collectionFactory.get('cUserGoalInTrackTimeSrv', {
          type: 'userGoal',
          callback: function (cUserGoalInput) {
            cUserGoal = cUserGoalInput;
            console.log('cUserGoal loaded', cUserGoal);

            deferred.resolve(api);
          },
          debug: false
        });
      },
      debug: false
    });

    return {
      promise: deferred.promise
    }
  }
});