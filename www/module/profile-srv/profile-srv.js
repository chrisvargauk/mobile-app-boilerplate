define(function ($http) {
    //console.log('$$$ reward and skill system loaded');

    return function($q, $rootScope) {
        console.log('Profile srv instantiated');

        var deferredTotal = $q.defer();
        var deferredRewardAndSkill = $q.defer();
        var deferredAchievement = $q.defer();
        var mProfile, cAchievement, cRewardAndSkill;

        var reward = {
                credit: {
                    noteCreated:                500,
                    noteUpdated:                200,
                    taskCompleted:              1000,
                    fixCompleted:               500,
                    comfortChallengeCompleted:  2000,
                    userGoalCreated:            5000,
                    userGoalStepCompleted:      1000,
                    userGoalTimeLogged:         1000,
                    userGoalGoalCompleted:      10000,
                    appStartedLastTime:         100
                },
                ip: {
                    noteCreated: 1,
                    noteUpdated: 1
                },
                achievement: {
                    'habitStreak.longestStreak.dailyPriority.three':      3000,
                    'habitStreak.longestStreak.dailyPriority.seven':      5000,
                    'habitStreak.longestStreak.dailyPriority.twoWeeks':   7000,
                    'habitStreak.longestStreak.dailyPriority.thirty':     10000,
                    'habitStreak.longestStreak.getStructured.three':      3000,
                    'habitStreak.longestStreak.getStructured.seven':      5000,
                    'habitStreak.longestStreak.getStructured.twoWeeks':   7000,
                    'habitStreak.longestStreak.getStructured.thirty':     10000,
                    'habitStreak.longestStreak.note.three':      3000,
                    'habitStreak.longestStreak.note.seven':      5000,
                    'habitStreak.longestStreak.note.twoWeeks':   7000,
                    'habitStreak.longestStreak.note.thirty':     10000
                }
        };

        function convertEvtHabitStreak (evtName, msg) {
            console.log('evt', msg.value);

            if (msg.value === 3) {
                achievementProcessor.push(evtName + '.three');
            }

            if (msg.value === 7) {
                achievementProcessor.push(evtName + '.seven');
            }

            if (msg.value === 14) {
                achievementProcessor.push(evtName + '.twoWeeks');
            }

            if (msg.value === 30) {
                achievementProcessor.push(evtName + '.thirty');
            }
        }

        var achievementProcessor = {
            queue: [],
            locked: false,
            push: function (achievement) {
                this.queue.push(achievement);
                this.processQueue();
            },
            processQueue: function () {
                var that = this;
                if (that.locked) {
                    return;
                }

                var achievement = that.queue.shift();

                // Evaluate achievement when data is ready
                var achievementList = _.pluck(cAchievement.JSON, 'type');
                if(achievementList.indexOf(achievement) !== -1) {
                    return;
                }

                that.locked = true;
                cAchievement.add({
                    type: achievement,
                    timestamp: moment().format('x')
                }, function () {
                    console.log('Profile Srv: '+achievement+' SAVED');

                    $rootScope.$emit('achievement.saved', {
                        type: achievement
                    });

                    that.locked = false;
                    if (that.queue.length > 0) {
                        that.processQueue();
                    }
                });
            }
        };

        collectionFactory.get('cRewardAndSkill', {
            type: 'rewardAndSkill',
            default: {
                credit: '0',
                ip: '0',
                quoteSavedLastTimeTimestamp: '0',
                appStartedFirstTime: '-1',
                appStartedLastTime: '-1',
                tipGoalDetailLastTime: '-1',
                ctrTipGoalDetails: '0'
            },
            callback: function (cRewardAndSkillInput) {
                //console.log('$$$ rewardAndSkill', cRewardAndSkillInput);
                cRewardAndSkill = cRewardAndSkillInput;

                if (cRewardAndSkill.JSON.length === 0) {
                    cRewardAndSkill.add({
                        credit: '0',
                        ip: '0'
                    }, function (mProfileInput) {
                        mProfile = mProfileInput;
                        deferredRewardAndSkill.resolve({});
                        //console.log('$$$ profile', profile);
                    });
                } else {
                    mProfile = cRewardAndSkill.JSON[0];
                    //console.log('$$$ profile', profile);
                    deferredRewardAndSkill.resolve({});
                }
            },
            debug: false
        });

        collectionFactory.get('cAchievement', {
            type: 'achievement',
            default: {
                type: '',
                timestamp: ''
            },
            callback: function (cAchievementInput) {
                cAchievement = cAchievementInput;
                deferredAchievement.resolve({});
            }
        });

        // # Event Handlers
        $rootScope.$on('note.created', function(evtName, msg) {
            // If type of Note is "quote"
            if (msg.type === 'quote') {
                var timestampLastSavedDayStart = moment(moment( parseInt(mProfile.quoteSavedLastTimeTimestamp) )
                      .format('YYYY-MM-DD')+' 00:00')
                      .format('x');

                var timestampTodayDayStart = moment(timeService.get()
                      .format('YYYY-MM-DD')+' 00:00')
                      .format('x');

                // If quote was saved last time today than dont give credit for it
                if (timestampLastSavedDayStart !== timestampTodayDayStart) {
                    mProfile.credit = parseInt(mProfile.credit) + reward.credit.noteCreated;
                    mProfile.ip = parseInt(mProfile.ip) + reward.ip.noteCreated;
                    mProfile.quoteSavedLastTimeTimestamp = timeService.get().format('x');

                    cRewardAndSkill.check(function () {
                        $rootScope.$emit('profile.ip.added', {
                            amount: reward.ip.noteCreated
                        });

                        $rootScope.$emit('profile.credit.added', {
                            amount: reward.credit.noteCreated
                        });
                    });
                }

            // If type of Note is something else
            } else {
                mProfile.credit = parseInt(mProfile.credit) + reward.credit.noteCreated;
                mProfile.ip = parseInt(mProfile.ip) + reward.ip.noteCreated;

                cRewardAndSkill.check(function () {
                    $rootScope.$emit('profile.ip.added', {
                        amount: reward.ip.noteCreated
                    });

                    $rootScope.$emit('profile.credit.added', {
                        amount: reward.credit.noteCreated
                    });
                });
            }
        });

        $rootScope.$on('note.updated', function(evtName, msg) {
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.noteUpdated;
            mProfile.ip = parseInt(mProfile.ip) + reward.ip.noteUpdated;

            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.ip.added', {
                    amount: reward.ip.noteUpdated
                });

                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.noteUpdated
                });
            });
        });

        $rootScope.$on('task.completed', function(evtName, msg) {
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.taskCompleted;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.taskCompleted
                });
            });
        });

        $rootScope.$on('fix.completed', function(evtName, msg) {
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.fixCompleted;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.fixCompleted
                });
            });
        });

        $rootScope.$on('comfortChallenge.completed', function(evtName, msg) {
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.comfortChallengeCompleted;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.comfortChallengeCompleted
                });
            });
        });

        $rootScope.$on('habitStreak.longestStreak.dailyPriority', function(evt, msg) {
            console.log('Evt received: habitStreak.longestStreak.dailyPriority', evt);
            convertEvtHabitStreak(evt.name, msg);
        });

        $rootScope.$on('habitStreak.longestStreak.getStructured', function(evt, msg) {
            console.log('Evt received: habitStreak.longestStreak.getStructured', evt);
            convertEvtHabitStreak(evt.name, msg);
        });

        $rootScope.$on('habitStreak.longestStreak.note', function(evt, msg) {
            console.log('Evt received: habitStreak.longestStreak.note', evt);
            convertEvtHabitStreak(evt.name, msg);
        });

        $rootScope.$on('achievement.saved', function(evtName, evt) {
            console.log('achievement.saved', evt);

            mProfile.credit = parseInt(mProfile.credit) + reward.achievement[evt.type];
            cRewardAndSkill.check(function () {
                $rootScope.$emit('newAchievement', {
                    type: evt.type
                });

                $rootScope.$emit('profile.credit.added', {
                    amount: reward.achievement[evt.type]
                });
            });
        });

        $rootScope.$on('starting-app', function(evtName, evt) {
            console.log('starting-app', evt);

            // If App is started first time
            if (parseInt(mProfile.appStartedLastTime) === -1) {
                mProfile.appStartedLastTime = timeService.get().format('x');
                mProfile.appStartedFirstTime = timeService.get().format('x');
                cRewardAndSkill.check();
                return;
            }

            // If its not today
            if (!isItToday(mProfile.appStartedLastTime) ) {
                $rootScope.$emit('alert', {
                    title: '<span class="fa fa-birthday-cake"></span> ' + reward.credit.appStartedLastTime + ' CRD.',
                    text: '<i>"Eighty percent of success is showing up." - Woody Allen</i>',
                    type: 'simple',
                    fnOk: function () {
                            mProfile.appStartedLastTime = timeService.get().format('x');
                            mProfile.credit = parseInt(mProfile.credit) + reward.credit.appStartedLastTime;

                            cRewardAndSkill.check(function () {
                                $rootScope.$emit('profile.credit.added', {
                                    amount: reward.credit.appStartedLastTime
                                });
                            });
                    }
                });
            }

            var timestampStartedFirstTimeDayStart = moment(moment( parseInt(mProfile.appStartedFirstTime) )
                .format('YYYY-MM-DD')+' 00:00')
                .format('x');

            var timestampTodayDayStart = moment(timeService.get()
                .format('YYYY-MM-DD')+' 00:00')
                .format('x');

            var timestampExpireDateDayStart = moment(parseInt(timestampStartedFirstTimeDayStart)).add(14, 'days').format('x');

            // if (timestampExpireDateDayStart <= timestampTodayDayStart) {
            //     var dAppExpiredScreen = document.querySelector('.app-expired-screen').classList.add('active');
            //     if (dAppExpiredScreen) {
            //         dAppExpiredScreen.classList.add('active');
            //     }
            // }
        });

        $rootScope.$on('goal-details-view', function(evtName, evt) {
            console.log('goal-details-view', evt);

            // If App is started first time
            if (parseInt(mProfile.tipGoalDetailLastTime) === -1) {
                mProfile.tipGoalDetailLastTime = timeService.get().format('x');
                cRewardAndSkill.check();
                return;
            }

            var listTipGoalDetail = [
                {
                    title: "Don’t reinvent the wheel.",
                    text: "Think about someone who has done what you are about to do and talk to him or her first."
                },
                {
                    title: "Efficient or effective",
                    text: "Even if you’re the best at something, it doesn’t mean you are the right person to do it. Think through whether any of the steps can be outsourced."
                },
                {
                    title: "Friend or customer",
                    text: "Your friends are not necessarily your ideal customers. Find a scalable way of getting feedback."
                },
                {
                    title: "No e-mail in the mornings",
                    text: "E-mail is a to-do list that someone else put together for you. Never read e-mails before you’ve got your most important task done."
                },
                {
                    title: "Keep battling complexity",
                    text: "At four stars complexity, one step could take four to five times longer than others. Please break them down into steps with lower complexity to get a more accurate estimated deadline."
                },
            ];

            // If its today
            if (!isItToday(mProfile.tipGoalDetailLastTime) ) {
                // If ctr passed the last item in the list, then reset ctr
                if (typeof listTipGoalDetail[parseInt(mProfile.ctrTipGoalDetails)] === 'undefined') {
                    mProfile.ctrTipGoalDetails = 0;
                }

                $rootScope.$emit('alert', {
                    isTutorial: true,
                    title: listTipGoalDetail[parseInt(mProfile.ctrTipGoalDetails)].title,
                    text: listTipGoalDetail[parseInt(mProfile.ctrTipGoalDetails)].text,
                    type: 'simple',
                    fnOk: function () {
                        mProfile.tipGoalDetailLastTime = timeService.get().format('x');
                        mProfile.ctrTipGoalDetails = parseInt(mProfile.ctrTipGoalDetails) + 1;

                        cRewardAndSkill.check();
                    }
                });
            }
        });

        $rootScope.$on('userGoal.created', function(evt, msg) {
            console.log('Evt received: userGoal.created', evt);
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.userGoalCreated;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.userGoalCreated
                });
            });
        });

        $rootScope.$on('userGoal.stepCompleted', function(evt, msg) {
            console.log('Evt received: userGoal.created', evt);
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.userGoalStepCompleted;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.userGoalStepCompleted
                });
            });
        });

        $rootScope.$on('userGoal.timeLogged', function(evt, msg) {
            console.log('Evt received: userGoal.created', evt);
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.userGoalTimeLogged;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.userGoalTimeLogged
                });
            });
        });

        $rootScope.$on('userGoal.goalCompleted', function(evt, msg) {
            console.log('Evt received: userGoal.created', evt);
            mProfile.credit = parseInt(mProfile.credit) + reward.credit.userGoalGoalCompleted;
            cRewardAndSkill.check(function () {
                $rootScope.$emit('profile.credit.added', {
                    amount: reward.credit.userGoalGoalCompleted
                });
            });
        });

        $q.all([deferredRewardAndSkill.promise, deferredAchievement.promise]).then(function(datas){
            deferredTotal.resolve({});
        });

        return {
            promise: deferredTotal.promise
        }
    }
});