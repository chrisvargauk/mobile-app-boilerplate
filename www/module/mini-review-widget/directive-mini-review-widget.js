define(function () {
    return function () {
        return {
            restrict: 'E',
            scope: {
                cGetStructured: '=',
                time: '='
            },
            templateUrl: 'module/mini-review-widget/directive-mini-review-widget-tmpl.html',
            controller: function ($scope) {
                $scope.mDailyFixLatest = $scope.cGetStructured.JSON[$scope.cGetStructured.JSON.length-1];

                $scope.formatDate = function (timestamp) {
                    var timeStmp = moment(parseInt(timestamp));

                    if (!timeStmp.isValid()) {
                        return '';
                    }

                    return moment(parseInt(timestamp)).format('dddd');
                };

                $scope.goToPageIf = function (condition, page) {
                    // Condition is "isDisabledX"
                    if ($scope[condition]) {
                        return;
                    }

                    if (page !== 'back') {
                        location.hash = page;
                    } else {
                        history.go(-1);
                    }
                };

                $scope.isShowDefaultMsgFix = function () {
                    if (typeof($scope.mDailyFixLatest) == 'undefined') {
                        return true;
                    }

                    if ($scope.mDailyFixLatest.isDoneTask) {
                        var timestampDailyFixLatest = moment(moment( parseInt($scope.mDailyFixLatest.timestamp ))
                                .format('YYYY-MM-DD')+' 00:00')
                            .format('x');

                        var timestampToday = moment($scope.time.get()
                                .format('YYYY-MM-DD')+' 00:00')
                            .format('x');

                        if (timestampDailyFixLatest !== timestampToday) {
                            return true;
                        }
                    }

                    return false;
                }
            }
        }
    };
});