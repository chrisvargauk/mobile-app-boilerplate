define(function () {
    //console.log('!!!!! reward popup is loaded');

    return function () {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'module/reward-popup/directive-reward-popup-tmpl.html',
            controller: function ($scope) {
                $scope.isActiveReward = false;
                $scope.isActiveAchievement = false;
                $scope.queue = [];
                $scope.msg = '';

                $scope.pop = function (type) {
                    if ($scope.queue.length === 0 ||
                        $scope.isActiveReward ||
                        $scope.isActiveAchievement) {
                        return;
                    }

                    var listItem = $scope.queue.shift();
                    $scope.msg = listItem.msg;
                    type = listItem.type;

                    if (type === 'reward') {
                        $scope.isActiveReward = true;
                    }

                    if (type === 'achievement') {
                        $scope.isActiveAchievement = true;
                    }

                    $scope.$apply(function () {});

                    // If it is an Achievement, dont close the popup
                    if (type === 'achievement') {
                        return;
                    }

                    setTimeout(function () {
                        $scope.close(type);
                    }, 1500);
                };

                $scope.close = function (type) {
                    if (type === 'reward') {
                        $scope.isActiveReward = false;
                    }

                    if (type === 'achievement') {
                        $scope.isActiveAchievement = false;
                    }

                    $scope.$apply(function() {});

                    setTimeout(function() {
                        $scope.pop(type);
                    }, 300);
                };

                $rootScope.$on('profile.credit.added', function(evtName, msg) {
                    //console.log('reward popup: profile.credit.added ');
                    $scope.queue.push({
                        type: 'reward',
                        msg: msg.amount + ' CRD Earned!'
                    });

                    // Pop msg only if it is inactive
                    if(!$scope.isActiveReward) {
                        $scope.pop('reward');
                    }
                });

                $rootScope.$on('profile.ip.added', function(evtName, msg) {
                    //console.log('reward popup: profile.ip.added ');
                    $scope.queue.push({
                        type: 'reward',
                        msg: msg.amount + ' Note Raised!'
                    });

                    // Pop msg only if it is inactive
                    if(!$scope.isActiveReward) {
                        $scope.pop('reward');
                    }
                });

                $rootScope.$on('newAchievement', function(evtName, msg) {
                    // Daily Focus
                    if (msg.type === 'habitStreak.longestStreak.dailyPriority.three') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '3 Days Daily Priority Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.dailyPriority.seven') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '7 Days Daily Priority Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.dailyPriority.twoWeeks') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: 'Two Weeks Daily Priority Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.dailyPriority.thirty') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '30 Days Daily Priority Habit Streak'
                        });
                    }


                    // Mini Reviews
                    if (msg.type === 'habitStreak.longestStreak.getStructured.three') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '3 Days Mini Review Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.getStructured.seven') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '7 Days Mini Review Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.getStructured.twoWeeks') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: 'Two Weeks Mini Review Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.getStructured.thirty') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '30 Days Mini Review Habit Streak'
                        });
                    }


                    // IP
                    if (msg.type === 'habitStreak.longestStreak.note.three') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '3 Days Note Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.note.seven') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '7 Days Note Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.note.twoWeeks') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: 'Two Weeks Note Habit Streak'
                        });
                    }

                    if (msg.type === 'habitStreak.longestStreak.note.thirty') {
                        $scope.queue.push({
                            type: 'achievement',
                            msg: '30 Days Note Habit Streak'
                        });
                    }

                    // Pop msg only if it is inactive
                    if(!$scope.isActiveAchievement) {
                        $scope.pop('achievement');
                    }
                });
            }
        };
    };
});
