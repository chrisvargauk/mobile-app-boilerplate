define(function ($http) {
    return function($q, $rootScope) {
        var deferredTime = $q.defer(),
            cTime, mTime, modify = {
                year: 0,
                month: 0,
                day: 0,
                hour: 0,
                min: 0
            };

        var api = {
            get: get,
            reset:      function () { reset(); },
            addYear:    function () { add(1, 'year') },
            addMonth:   function () { add(1, 'month') },
            addDay:     function () { add(1, 'day') },
            addHour:    function () { add(1, 'hour') },
            addMin:     function () { add(1, 'min') },
            subYear:    function () { subtract(1, 'year') },
            subMonth:   function () { subtract(1, 'month') },
            subDay:     function () { subtract(1, 'day') },
            subHour:    function () { subtract(1, 'hour') },
            subMin:     function () { subtract(1, 'min') }
        };

        // Todo: remove before prod
        window.api = api;

        collectionFactory.get('cTime', {
            type: 'time',
            default: {
                year: '0',
                month: '0',
                day: '0',
                hour: '0',
                min: '0'
            },
            callback: function (cTimeInput) {
                //console.log('$$$ rewardAndSkill', cRewardAndSkillInput);
                cTime = cTimeInput;

                if (cTime.JSON.length === 0) {
                    cTime.add({}, function (mTimeInput) {
                        mTime = mTimeInput;
                        api._mTime = mTime;
                        deferredTime.resolve(api);
                        window.timeService = api;
                    });
                } else {
                    mTime = cTimeInput.JSON[0];
                    api._mTime = mTime;
                    deferredTime.resolve(api);
                    window.timeService = api;
                }
            },
            debug: false
        });

        //$q.all([deferredRewardAndSkill.promise, deferredAchievement.promise]).then(function(datas){
        //    deferredTime.resolve({});
        //});

        function get() {
            var time = moment();

            if (mTime.year > 0) {
                time.add(mTime.year, 'year');
            }

            if (mTime.month > 0) {
                time.add(mTime.month, 'month');
            }

            if (mTime.day > 0) {
                time.add(mTime.day, 'day');
            }

            if (mTime.hour > 0) {
                time.add(mTime.hour, 'hour');
            }

            if (mTime.min > 0) {
                time.add(mTime.min, 'minute');
            }


            if (mTime.year < 0) {
                time.subtract(-mTime.year, 'year');
            }

            if (mTime.month < 0) {
                time.subtract(-mTime.month, 'month');
            }

            if (mTime.day < 0) {
                time.subtract(-mTime.day, 'day');
            }

            if (mTime.hour < 0) {
                time.subtract(-mTime.hour, 'hour');
            }

            if (mTime.min < 0) {
                time.subtract(-mTime.min, 'minute');
            }

            return time;

        }

        function logTime() {
            console.log(get().format('YYYY MM dd hh:mm'));
        }

        function add(amount, type) {
            switch (type) {
                case 'year':    mTime.year  = parseInt(mTime.year)  + amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'month':   mTime.month = parseInt(mTime.month) + amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'day':     mTime.day   = parseInt(mTime.day)   + amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'hour':    mTime.hour  = parseInt(mTime.hour)  + amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'min':     mTime.min  = parseInt(mTime.min)  + amount;
                    cTime.check(function () { logTime() });
                    break;

            }
        };

        function subtract(amount, type) {
            switch (type) {
                case 'year':    mTime.year  = parseInt(mTime.year)  - amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'month':   mTime.month = parseInt(mTime.month) - amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'day':     mTime.day   = parseInt(mTime.day)   - amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'hour':    mTime.hour  = parseInt(mTime.hour)  - amount;
                    cTime.check(function () { logTime() });
                    break;
                case 'min':     mTime.min  =  parseInt(mTime.min)  -  amount;
                    cTime.check(function () { logTime() });
                    break;

            }
        };

        function reset() {
            mTime.year = mTime.month = mTime.day = mTime.hour = mTime.min = 0;
            cTime.check(function () {
                logTime();
            });
        }

        return {
            promise: deferredTime.promise
        }
    }
});