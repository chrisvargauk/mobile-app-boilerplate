define(function () {
  return function () {
    return {
      restrict: 'E',
      scope: {
        cList: '=',
        icon: '@',
        time: '='
      },
      templateUrl: 'module/habit-streak/directive-habit-streak-tmpl.html',
      controller: function controller($scope) {
        //console.log('@@@ Habit Streak loaded');
        $scope.nextAchievement = 3;

        $scope.getDay = function (day) {
          return $scope.streakList[day];
        };

        //var dateList = $scope.cList.JSON.map(function (dailyPriority) {
        //  var isDoneTask  = typeof dailyPriority.isDoneTask === 'undefined' ? true : dailyPriority.isDoneTask;
        //  var timestampDayStart = moment(moment( parseInt(dailyPriority.timestamp ))
        //                            .format('YYYY-MM-DD')+' 00:00')
        //                            .format('x') + '|' + isDoneTask;
        //
        //  return timestampDayStart;
        //});

        var dateList = [];
        $scope.cList.JSON.forEach(function (dailyPriority) {
          var isDoneTask  = typeof dailyPriority.isDoneTask === 'undefined' ? true : dailyPriority.isDoneTask;

          var timestampDayStart = moment(moment( parseInt(dailyPriority.timestamp ))
                .format('YYYY-MM-DD')+' 00:00')
              .format('x') + '|' + isDoneTask;

          dateList.push(timestampDayStart);

          if (typeof dailyPriority.timestampUpdate === 'undefined') {
            return;
          }

          var timestampUpdate = moment(moment( parseInt(dailyPriority.timestampUpdate ))
                .format('YYYY-MM-DD')+' 00:00')
              .format('x') + '|' + isDoneTask;

          dateList.push(timestampUpdate);
        });

        console.log('dateList', dateList);

        $scope.streakList = {};
        $scope.longestStreak = 0;
        var currentStreak = 0;
        for (var i=0; i < 31; i++) {
          var timestampDay = moment($scope.time.get()
                              .format('YYYY-MM-DD')+' 00:00')
                              .subtract(i, 'day')
                              .format('x');

          $scope.streakList['day'+i] = {
            isDone: dateList.indexOf(timestampDay+'|true') !== -1,
            timestamp: timestampDay,
            day: moment( parseInt(timestampDay) ).format('dd')
          };

          // Calc longest streak
          // todo: this counts up to 30 days, if longer wont show it - There is also a performance issue here
          if ($scope.streakList['day'+i].isDone) {
            currentStreak++;
          } else {
            if (currentStreak > $scope.longestStreak) {
              $scope.longestStreak = currentStreak;
            }

            currentStreak = 0;
          }
        }

        //Calc next achievement
        if ($scope.longestStreak < 3) {
          $scope.nextAchievement = 3;
        }
        if (2 < $scope.longestStreak && $scope.longestStreak < 7) {
          $scope.nextAchievement = 7;
        }
        if (6 < $scope.longestStreak && $scope.longestStreak < 14) {
          $scope.nextAchievement = 14;
        }
        if (13 < $scope.longestStreak && $scope.longestStreak < 30) {
          $scope.nextAchievement = 30;
        }
        if (29 < $scope.longestStreak) {
          $scope.nextAchievement = "..";
        }

        console.log('Evt emitted: habitStreak.longestStreak.'+$scope.cList.nameCollection);
        $rootScope.$emit('habitStreak.longestStreak.'+$scope.cList.nameCollection, {
          value: $scope.longestStreak
        });

        // Let other directives to trigger this directive to be rerendered
        $rootScope.$on('cList.change', function () {
          controller($scope);
          $scope.$apply(function () {});
        });
      }
    };
  };
});
