define(function () {
  return function () {
    return {
      restrict: 'E',
      scope: {
        cNote: '=',
        idQuote: '=',
        time: '='
      },
      templateUrl: 'module/quote/directive-quote-tmpl.html',
      controller: function ($scope) {
        $scope.cQuote = {
          JSON: [
            {
              text: "If you win the morning, you win the day.",
              from: "Timothy Ferriss",
              img: 'timothy-ferriss.jpg',
              isShown: false
            },
            {
              text: "If you are inspired you will create an inspired vision, if you are fearful you will crate a vision mitigating your fear.",
              from: "Daniel Priestley",
              img: 'daniel-priestley.jpg',
              isShown: false
            },
            {
              text: "If more information was the answer, we would be all millionars with perfect abs.",
              from: "Derek Sivers",
              img: 'derek-sivers.jpg',
              isShown: false
            },
            {
              text: "There is a predictable cycle that turns ideas into remarkably valuable creations.",
              from: "Daniel Priestley",
              img: 'daniel-priestley.jpg',
              isShown: false
            },
            {
              text: "The world is meant to be jailbroken.",
              from: "Eric Weinstein",
              img: 'ericweinstein.jpg',
              isShown: false
            },
            {
              text: "We live in a world where many people complicate to profit. If what you do is simple then you feel like you are dispensable.",
              from: "Timothy Ferriss",
              img: 'timothy-ferriss.jpg',
              isShown: false
            },
            {
              text: "Either I will find a way, or I will make one.",
              from: "Philip Sidney",
              img: 'philip-sidney.jpg',
              isShown: false
            },
            {
              text: "Even if things are going horribly wrong, I just choose to be confident.",
              from: "Derek Sivers",
              img: 'derek-sivers.jpg',
              isShown: false
            },
            {
              text: "75% of success is staying calm. The rest you figure out.",
              from: "Sam Kass",
              img: 'sam-kass.jpg',
              isShown: false
            },
            {
              text: "To me, ‘busy’ implies that the person is out of control of their life.",
              from: "Derek Sivers",
              img: 'derek-sivers.jpg',
              isShown: false
            },
            {
              text: "No project survives first contact with customers.",
              from: "Steve Blank",
              img: 'steve-blank.jpg',
              isShown: false
            },
            {
              text: "Learning to ignore things is one of the great paths to inner peace.",
              from: "Robert J. Sawyer",
              img: 'robert-j-sawyer.jpg',
              isShown: false
            },
            {
              text: "What you do today is important because you are exchanging a day of your life for it.",
              from: "Unknown",
              img: 'quote.jpeg',
              isShown: false
            },
            {
              text: "If you must play, decide on three things at the start: the rules of the game, the stakes, and the quitting time.",
              from: "chinese proverbs",
              img: 'chinese-proverbs.jpg',
              isShown: false
            },
            {
              text: "If you win the morning you win the day.",
              from: "Timothy Ferriss",
              img: 'timothy-ferriss.jpg',
              isShown: false
            },
            {
              text: "What we usually consider as impossible are simply engineering problems... there's no law of physics preventing them.",
              from: "Michio Kaku",
              img: 'michio-kaku.jpg',
              isShown: false
            },
            {
              text: "Your level of success will seldom exceed the level of your personal development.. Success is something you attract by the person you've become.",
              from: "Jim Rohn",
              img: 'jim-rohn.jpg',
              isShown: false
            },
            {
              text: "You cannot grow without risk. Period.",
              from: "Jordan Wirsz",
              img: 'jordan-wirsz.jpg',
              isShown: false
            },
            {
              text: "Be the change you need to see in the world.",
              from: "Mahatma Gandhi",
              img: 'mahatma-gandhi.jpg',
              isShown: false
            },
            {
              text: "Discipline equals freedom.",
              from: "John “Jocko” Willink",
              img: 'john-jocko-willink.jpg',
              isShown: false
            },
            {
              text: "Lose an hour in the morning, and you will be all day hunting for it.",
              from: "Richard Whately",
              img: 'richard-whately.jpg',
              isShown: false
            },
            {
              text: "Successful people get what they want but they also want what they have.",
              from: "Timothy Ferriss",
              img: 'timothy-ferriss.jpg',
              isShown: false
            },
            {
              text: "You can finish something by dropping it. Reduce the mental baggage you carry.",
              from: "Unknown",
              img: 'quote.jpeg',
              isShown: false
            },
            {
              text: "Nothing taste as good as health feels.",
              from: "Peter Voogd",
              img: 'peter-voogd.jpg',
              isShown: false
            },
            {
              text: "Success Is Going from Failure to Failure Without Losing Your Enthusiasm.",
              from: "Winston Churchill",
              img: 'winston-churchill.jpg',
              isShown: false
            },
            {
              text: "Setting goals is the first step in turning the invisible into the visible.",
              from: "Tony Robbins",
              img: 'tony-robbins.jpg',
              isShown: false
            }
          ]
        };

        $scope.getQuoteCtr = function () {
          return parseInt($scope.time.get().format('D')) % $scope.cQuote.JSON.length;
        };

        $scope.saveQuote = function (id) {
          console.log('Quote Saved');
          var id = typeof id === 'undefined' ? $scope.getQuoteCtr() : id;

          $scope.cNote.add({
            name: 'Quote from '+$scope.cQuote.JSON[id].from,
            description: '“'+$scope.cQuote.JSON[id].text +'“ - '+$scope.cQuote.JSON[id].from,
            timestamp: $scope.time.get().format('x')
          }, function(model) {
            $scope.creditManagement.add(5);

            $rootScope.$emit('alert', {
              title: 'QUOTE SAVED',
              text: 'Quote is saved to your notes.',
              type: 'simple',
              fnOk: function () {
                // Let other services know, like Profile Srv
                $rootScope.$emit('note.created', {
                  type: 'quote'
                });
              }
            });
          });
        };



        $scope.getImgUrl = function (id) {
          var id = typeof id === 'undefined' ? $scope.getQuoteCtr() : id;
          return 'module/quote/img/' + $scope.cQuote.JSON[id].img;
        };

        $scope.getQuote = function (id) {
          var id = typeof id === 'undefined' ? $scope.getQuoteCtr() : id;
          return $scope.cQuote.JSON[id];
        };

        $scope.creditManagement = {
          add: function (amount) {
            $scope.userPropCredit = $scope.userPropCredit + amount;
            localStorage.userPropCredit = $scope.userPropCredit;
          }
        };
      }
    }
  }
});