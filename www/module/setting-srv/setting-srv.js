define(function ($http) {
  return function($q, $rootScope) {
    var deferredSetting = $q.defer(),
        cSetting;

    var api = {
      valueOf: valueOf,
      set: set
    };

    collectionFactory.get('cSetting', {
      type: 'setting',
      default: {
        key: '',
        value: ''
      },
      callback: function (cSettingInput) {
        cSetting = cSettingInput;

        var deferredStepDetailedIsVisibleStepDone = $q.defer();
        if (valueOf('stepDetailedIsVisibleStepDone') === null) {
          cSetting.add({
            key: 'stepDetailedIsVisibleStepDone',
            value: true,
          }, function () {
            deferredStepDetailedIsVisibleStepDone.resolve();
          });
        } else {
          deferredStepDetailedIsVisibleStepDone.resolve();
        }

        var deferredStepDetailedSortOrder = $q.defer();
        if (valueOf('stepDetailedSortOrder') === null) {
          cSetting.add({
            key: 'stepDetailedSortOrder',
            value: 'idOrder',
          }, function () {
            deferredStepDetailedSortOrder.resolve();
          });
        } else {
          deferredStepDetailedSortOrder.resolve();
        }

        $q.all([deferredStepDetailedIsVisibleStepDone, deferredStepDetailedSortOrder])
          .then(function () {
            deferredSetting.resolve(api);
          });

        // Todo: remove before prod
        window.settingAPI = api;
      },
      debug: false
    });

    function valueOf(key) {
      var value = null;

      cSetting.JSON.forEach(function (setting) {
        if(setting.key === key) {
          value = setting.value;
        }
      });

      return value;
    }

    function set(key, valueNew, callback) {
      var isSuccessful = false;

      cSetting.JSON.forEach(function (setting) {
        if(setting.key === key) {
          setting.value = valueNew;
          isSuccessful = true;
        }
      });

      if (isSuccessful) {
        cSetting.check(callback);
      }

      return isSuccessful;
    }

    return {
      promise: deferredSetting.promise
    }
  }
});