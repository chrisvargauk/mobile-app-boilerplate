define(function () {
  var directiveAlert = function () {
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'module/alert/tmpl-alert.html',
      controller: function ($scope, $rootScope) {
        $scope.visible = false;
        $scope.text = 'Alert';
        $scope.type = 'simple'; // 'simple', 'cancelOk'
        $scope.isTutorial = false;

        $scope.fnCancel = function () {
          console.log('cancel');
        };

        $scope.fnOk = function () {
          console.log('ok');
        };

        $scope.cancel = function () {
          $scope.fnCancel();
        };

        $scope.ok = function () {
          $scope.fnOk();
        };

        $rootScope.$on('alert', function (event, msg) {
          $scope.visible = true;
          $scope.text = msg.text;
          $scope.type = msg.type || $scope.type;
          $scope.fnCancel = msg.fnCancel || function() {};
          $scope.fnOk = msg.fnOk || function() {};
          $scope.title = msg.title || $scope.title;
          $scope.isTutorial = msg.isTutorial || false;

          $scope.$apply(function(){});
        });

        window.$rootScope = $rootScope;
      },
      link: function (scope, element, attrs) {
        element.bind('click', function (evt) {
          scope.visible = false;
          scope.$apply(function () {
          });
        });
      }
    }
  };

  return directiveAlert;
});