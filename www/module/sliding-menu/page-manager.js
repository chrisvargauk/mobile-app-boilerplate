define(function () {
  return function () {
    var PageManager = function (option) {
      var that = this;

      that._default = {
        nameApp: ''
      };

      that.title = option.nameApp || that._default.nameApp;

      that.setTitle = function (title) {
        that.title = title;
      }
    };

    return new PageManager({
      nameApp: ''
    });
  };
});