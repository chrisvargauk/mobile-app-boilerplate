define(function () {
    return function () {
        return {
            restrict: 'E',
            scope: {
                cDailyPriority: '=',
                time: '='
            },
            templateUrl: 'module/get-structured-widget/directive-get-structured-widget-tmpl.html',
            controller: function ($scope) {
                $scope.mDailyPriorityLatest = $scope.cDailyPriority.JSON[$scope.cDailyPriority.JSON.length-1];

                $scope.formatDate = function (timestamp) {
                    var timeStmp = moment(parseInt(timestamp));

                    if (!timeStmp.isValid()) {
                        return '';
                    }

                    return moment(parseInt(timestamp)).format('dddd');
                };

                $scope.taskDone = function (condition) {
                    // Condition is "isDisabledX"
                    if ($scope[condition]) {
                        return;
                    }

                    // Do nothing if task is already done
                    if ($scope.mDailyPriorityLatest.isDoneTask) {
                        return;
                    }

                    var popupCheckTime = function () {
                        if ($scope.time.get().format('HH') > 11) {
                            setTimeout(function () {
                                $rootScope.$emit('alert', {
                                    title: '<span class="fa fa-gears"></span> Improve your strategy.',
                                    text: 'Next time, try to get your most important task done before 11:00 a.m.',
                                    type: 'simple',
                                    fnOk: function () {
                                        popupReward();
                                    }
                                });
                            }, 200);
                        } else {
                            popupReward();
                        }
                    };

                    var popupReward = function () {
                        if ($scope.mDailyPriorityLatest.reward !== '') {
                            setTimeout(function () {
                                $rootScope.$emit('alert', {
                                    title: 'Treat Yourself',
                                    text: $scope.mDailyPriorityLatest.reward,
                                    type: 'simple',
                                    fnOk: function () {
                                        $rootScope.$emit('task.completed', {});
                                    }
                                });
                            }, 200);
                        } else {
                            $rootScope.$emit('task.completed', {});
                        }
                    };

                    $rootScope.$emit('alert', {
                        title: 'Completed',
                        text: 'Have you finished the task?',
                        type: 'cancelOk',
                        fnOk: function () {
                            $scope.mDailyPriorityLatest.isDoneTask = true;
                            $scope.cDailyPriority.check(function () {
                                // Manually trigger the Habit Streak directive to rerender
                                $rootScope.$emit('cDailyPriority.change', {});
                            });

                            popupCheckTime();
                            $rootScope.$emit('cList.change', {});
                        }
                    });
                };

                $scope.goToPageIf = function (condition, page) {
                    console.log('Click');
                    // Condition is "isDisabledX"
                    if ($scope[condition]) {
                        return;
                    }

                    if (page !== 'back') {
                        location.hash = page;
                    } else {
                        history.go(-1);
                    }
                };

                $scope.isShowDefaultMsgTask = function () {
                    // If there is no task at all
                    if (typeof($scope.mDailyPriorityLatest) == 'undefined') {
                        return true;
                    }

                    if ($scope.mDailyPriorityLatest.isDoneTask) {
                        var timestampDailyPriorityLatest = moment(moment( parseInt($scope.mDailyPriorityLatest.timestamp ))
                                                            .format('YYYY-MM-DD')+' 00:00')
                                                            .format('x');

                        var timestampToday = moment($scope.time.get()
                                                .format('YYYY-MM-DD')+' 00:00')
                                                .format('x');

                        if (timestampDailyPriorityLatest !== timestampToday) {
                            return true;
                        }
                    }

                    // If task is empty string
                    return $scope.mDailyPriorityLatest.task == '';
                };
            }
        }
    };
});