define(function () {
    //console.log('%%%% profile widget is loaded');

    return function () {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'module/profile-widget/directive-profile-widget-tmpl.html',
            controller: function ($scope) {
                $scope.credit = 0;
                $scope.ip = 0;
                $scope.achievement = 0;

                $scope.reloadDb = function () {
                    collectionFactory.get('cRewardAndSkill', {
                        type: 'rewardAndSkill',
                        default: {
                            credit: '0',
                            ip: '0'
                        },
                        callback: function (cRewardAndSkillInput) {
                            //console.log('$$$ rewardAndSkill', cRewardAndSkillInput);
                            var cRewardAndSkill = cRewardAndSkillInput;

                            if (cRewardAndSkill.JSON.length === 0) {
                                console.error('rewardAndSkill is not loaded yet.');
                            } else {
                                $scope.credit = cRewardAndSkillInput.JSON[0].credit;
                                $scope.ip = cRewardAndSkillInput.JSON[0].ip;
                                $scope.$apply(function () {});
                            }
                        },
                        debug: false
                    });

                    collectionFactory.get('cAchievement', {
                        type: 'achievement',
                        default: {
                            type: '',
                            timestamp: ''
                        },
                        callback: function (cAchievement) {
                            if (cAchievement.JSON.length === 0) {
                                console.error('Achievement is not loaded yet.');
                            } else {
                                $scope.achievement = cAchievement.JSON.length;
                                $scope.$apply(function () {});
                            }
                        },
                        debug: false
                    });
                };

                $rootScope.$on('profile.credit.added', function(evtName, msg) {
                    $scope.reloadDb();
                });

                $rootScope.$on('profile.credit.deducted', function(evtName, msg) {
                    $scope.reloadDb();
                });

                $rootScope.$on('profile.ip.added', function(evtName, msg) {
                    $scope.reloadDb();
                });

                $rootScope.$on('newAchievement', function(evtName, msg) {
                    $scope.reloadDb();
                });

                $scope.reloadDb();
            }
        }
    }
});