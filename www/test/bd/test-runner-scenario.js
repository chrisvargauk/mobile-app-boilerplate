define([  '..//bd/scenario-add-new-goal'
  ], function() {
  var listArgument = arguments;

  window.runScenarioTest = function() {
    Array.prototype.slice.call(listArgument).forEach(function(scenario) {
      scenario();
    });
  };
});