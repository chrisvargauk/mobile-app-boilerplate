define(['scenarioRunner'], function() {
  scRunner.add('_generateReview - Generate Review for give day', function (sc) {
    var runner = new Collection.prototype.asyncRunner(),
        mReviewGenerated;

    runner.schedule(function(resolve) {
      var today = moment('2015-05-24'); // Sun
      isItTimeForReviewNew._generateReview(today, function (mReview) {
        mReviewGenerated = mReview;
        resolve();
      });
    });

    runner.schedule(function(resolve) {
      sc.test('If I pass in Sunday then a Review is generated and it is scheduled to Sunday.')
        .check(moment(parseInt(mReviewGenerated.scheduled)).day())
        .equalTo(0);
      resolve();
      sc.resolve();
    });

    runner.run();
  });

  scRunner.add('_isItSunday - Check whether give day is Sunday', function (sc) {

    var today = moment('2015-05-24'); // Sun
    sc.test('If I pass Sunday date object then is should return true.')
      .check(isItTimeForReviewNew._isItSunday(today))
      .equalTo(true);

    var today = moment('2015-05-23'); // Sat
    sc.test('If I pass Saturday date object then it should return false.')
      .check(isItTimeForReviewNew._isItSunday(today))
      .equalTo(false);

    sc.resolve();
  });

  scRunner.add('_revForToday - Check whether there is any review for give day', function (sc) {
    var runner = new Collection.prototype.asyncRunner();

    // Empty Review Collection
    runner.schedule(function(resolve) {
      Collection.prototype.emptyWebSQL('review', function () {
        resolve();
      });
    });

    // Check with Sunday Date Obj
    runner.schedule(function(resolve) {
      var today = moment('2015-05-24'); // Sun

      sc.test('If Review Collection is empty and I pass in Sunday date object, then it should return false.')
        .check(isItTimeForReviewNew._revForToday(today))
        .equalTo(false);

      resolve();
    });

    // Generate review for Sunday
    runner.schedule(function(resolve) {
      var today = moment('2015-05-24'); // Sun

      isItTimeForReviewNew._generateReview(today, function (mReview) {
        resolve();
      });
    });

    // Check with Sunday Date Obj
    runner.schedule(function(resolve) {
      var today = moment('2015-05-24'); // Sun

      sc.test('If Review Collection is NOT empty and I pass in Sunday date object, then it should return true.')
        .check(isItTimeForReviewNew._revForToday(today))
        .equalTo(false);

      resolve();
    });

    runner.run();

  });
});