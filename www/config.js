define(['deferredListCollection'], function () {

  function config($stateProvider, $urlRouterProvider) {
    // # Redirects
    $urlRouterProvider
      .when('', '/landing')
      .when('/add-new-goal', 'add-new-goal/introduction')
      .otherwise('/landing');

    // # Pages
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'product/page/home/home-tmpl.html',
        controller: CtrlHome,
        resolve: {
          cReason: window.deferredFnReason,
          cStep: window.deferredFnStep,
          cReview: window.deferredFnReview,
          cUserGoal: window.deferredFnUserGoal,
          cTrackRecord: window.deferredFnTrackRecord,
          timeService: function (timeService) {
            return timeService.promise;
          },
          dataFromService: function(profileService) {
            return profileService.promise;
          }
        }
      })

      .state('add-new-goal', {
        url: '/add-new-goal',
        templateUrl: 'product/page/add-new-goal/tmpl-add-new-goal.html'
      })
      .state('add-new-goal.introduction', {
        url: '/introduction',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-introduction.html',
        controller: CtrlIntroduction,
        resolve: {
          cReason: window.deferredFnReason,
          cEpic: window.deferredFnEpic,
          cStep: window.deferredFnStep,
          cUserGoal: window.deferredFnUserGoal,
          cTimeAndLocation: window.deferredFnTimeAndLocation,
          cTrackRecord: window.deferredFnTrackRecord,
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })
      .state('add-new-goal.definition', {
        url: '/definition',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-definition.html',
        controller: CtrlDefinition
      })
      .state('add-new-goal.definitionEdit', {
        url: '/definition/:status',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-definition.html',
        controller: CtrlDefinition
      })
      .state('add-new-goal.ListReason', {
        url: '/reasons',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-reason.html',
        controller: CtrlReason
      })
      .state('add-new-goal.listStep', {
        url: '/steps',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-step.html',
        controller: CtrlListStep
      })
      .state('add-new-goal.listStepDetailed', {
        url: '/steps-detailed',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-step-detailed.html',
        controller: CtrlListStepDetailed,
        resolve: {
          cStep: window.deferredFnStep,
          setting: function (settingService) {
            return settingService.promise
          },
          time: function (timeService) {
            return timeService.promise;
          },
          trackTimeService: function (trackTimeService) {
            return trackTimeService.promise;
          }
        }
      })
      .state('add-new-goal.listTimeframe', {
        url: '/timeframe',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-timeframe.html',
        controller: CtrlListTimeframe,
        resolve: {
          cReason: window.deferredFnReason,
          cStep: window.deferredFnStep,
          cUserGoal: window.deferredFnUserGoal,
          cTimeAndLocation: window.deferredFnTimeAndLocation,
          cTrackRecord: window.deferredFnTrackRecord
        }
      })

      .state('goal-detail', {
        url: '/goal-details/:indexUserGoal',
        templateUrl: 'product/page/goal-detail/tmpl-goal-detail.html',
        controller: CtrlGoalDetail,
        resolve: {
          cReason: window.deferredFnReason,
          cStep: window.deferredFnStep,
          cUserGoal: window.deferredFnUserGoal,
          cNote: window.deferredFnNote,
          cTrackRecord: window.deferredFnTrackRecord,
          trackTimeService: function (trackTimeService) {
            return trackTimeService.promise;
          },
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('track-record', {
        url: '/track-record/:indexUserGoal',
        templateUrl: 'product/page/track-record/tmpl-track-record.html',
        controller: CtrlTrackRecord,
        resolve: {
          cUserGoal: window.deferredFnUserGoal,
          cTrackRecord: window.deferredFnTrackRecord,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('review', {
        url: '/review/:idUserGoal/:idReview',
        templateUrl: 'product/page/review/tmpl-review.html',
        controller: CtrlReview,
        resolve: {
          cUserGoal: window.deferredFnUserGoal
        }
      })

      .state('setting', {
        url: '/setting',
        templateUrl: 'product/page/setting/tmpl-setting.html',
        controller: CtrlSetting
      })

      .state('about', {
        url: '/about',
        templateUrl: 'product/page/about/tmpl-about.html',
        controller: CtrlAbout
      })

      .state('concept-introduction', {
        url: '/concept/introduction',
        templateUrl: 'product/page/concept/concept-introduction-tmpl.html',
        controller: CtrlConceptIntroduction
      })

      .state('concept-reasons', {
        url: '/concept/reasons',
        templateUrl: 'product/page/concept/concept-reasons-tmpl.html',
        controller: CtrlConceptReasons
      })

      .state('concept-rewards', {
        url: '/concept/rewards',
        templateUrl: 'product/page/concept/concept-rewards-tmpl.html',
        controller: CtrlConceptRewards
      })

      .state('concept-sepofspace', {
        url: '/concept/sepofspace',
        templateUrl: 'product/page/concept/concept-sepofspace-tmpl.html',
        controller: CtrlConceptSepofspace
      })

      .state('concept-steps', {
        url: '/concept/steps',
        templateUrl: 'product/page/concept/concept-steps-tmpl.html',
        controller: CtrlConceptSteps
      })

      .state('concept-timeframe', {
        url: '/concept/timeframe',
        templateUrl: 'product/page/concept/concept-timeframe-tmpl.html',
        controller: CtrlConceptTimeframe
        //resolve: {
        //  cUserGoal: deferredFnUserGoal
        //}
      })

      .state('concept-intellectual-property', {
        url: '/concept/intellectual-property',
        templateUrl: 'product/page/concept/concept-intellectual-property-tmpl.html',
        controller: CtrlIntellectualProperty
      })

      .state('concept-comfort-challenge', {
        url: '/concept/comfort-challenge',
        templateUrl: 'product/page/concept/concept-comfort-challenge-tmpl.html',
        controller: CtrlConceptComfortChallenge
      })

      .state('concept-habit-and-routine', {
        url: '/concept/habit-and-routine',
        templateUrl: 'product/page/concept/concept-habit-and-routine-tmpl.html',
        controller: CtrlConceptHabitAndRoutine
      })

      .state('classroom', {
        url: '/classroom',
        templateUrl: 'product/page/classroom/classroom-tmpl.html',
        controller: CtrlClassroom,
        resolve: {
          cConcept: window.deferredFnConcept,
          cRewardAndSkill: window.deferredFnRewardAndSkill,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('comfort-challenge', {
        url: '/comfort-challenge',
        templateUrl: 'product/page/comfort-challenge/comfort-challenge-tmpl.html',
        controller: CtrlComfortChallenge,
        resolve: {
          cComfortChallenge: window.deferredFncComfortChallenge,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('comfort-challenge-detail', {
        url: '/comfort-challenge-detail/:indexComfortChallenge',
        templateUrl: 'product/page/comfort-challenge/comfort-challenge-detail-tmpl.html',
        controller: CtrlComfortChallengeDetail,
        resolve: {
          cComfortChallenge: window.deferredFncComfortChallenge,
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('note-list', {
        url: '/note-list',
        templateUrl: 'product/page/note/note-list-tmpl.html',
        controller: CtrlNoteList,
        resolve: {
          cNote: window.deferredFnNote,
          dataFromService: function(profileService) {
            return profileService.promise;
          },
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('note-suggestion-list', {
        url: '/note-suggestion-list',
        templateUrl: 'product/page/note/note-suggestion-list-tmpl.html',
        controller: CtrlNoteSuggestionList,
        resolve: {
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('note-add-idea', {
        url: '/note-add/:id/:indexCategory/:indexIdea',
        templateUrl: 'product/page/note/note-add-tmpl.html',
        controller: CtrlNoteAdd,
        resolve: {
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })
      
      // Add / Edit Comfort Challenge Note (id === 'new' when Adding, id === 2 e.g. when editing)
      .state('note-add-comfort-to-challenge', {
        url: '/note-add-comfort-to-challenge/:id/:idComfortChallenge/:nameComfortChallenge',
        templateUrl: 'product/page/note/note-add-tmpl.html',
        controller: CtrlNoteAdd,
        resolve: {
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('note-add-plan', {
        url: '/note-add-plan/:id/:idUserGoal/:nameUserGoal',
        templateUrl: 'product/page/note/note-add-tmpl.html',
        controller: CtrlNoteAdd,
        resolve: {
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('note-edit', {
        url: '/note-add/:id',
        templateUrl: 'product/page/note/note-add-tmpl.html',
        controller: CtrlNoteAdd,
        resolve: {
          cNote: window.deferredFnNote,
          dataFromService: function(profileService) {
            return profileService.promise;
          },
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('get-structured', {
        url: '/get-structured',
        templateUrl: 'product/page/get-structured/get-structured-tmpl.html',
        controller: CtrlGetStructured,
        resolve: {
          cGetStructured: window.deferredFnGetStructured,
          cDailyPriority: window.deferredFnDailyPriority,
          cNote: window.deferredFnNote,
          cUserGoal: window.deferredFnUserGoal,
          time: function (timeService) {
            return timeService.promise;
          },
          dataFromService: function(profileService) {
            return profileService.promise;
          }
        }
      })

      .state('mini-review', {
        url: '/mini-review',
        templateUrl: 'product/page/mini-review/mini-review-tmpl.html',
        controller: CtrlMiniReview,
        resolve: {
          cGetStructured: window.deferredFnGetStructured,
          cDailyPriority: window.deferredFnDailyPriority,
          cNote: window.deferredFnNote,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('achievement', {
        url: '/achievement',
        templateUrl: 'product/page/achievement/achievement-tmpl.html',
        controller: CtrlAchievement,
        resolve: {
          cAchievement: window.deferredFnAchievement,
          cRewardAndSkill: window.deferredFnRewardAndSkill,
        }
      })

      .state('dev', {
        url: '/dev',
        templateUrl: 'product/page/dev/dev-tmpl.html',
        controller: CtrlDev,
        resolve: {
          cGetStructured: window.deferredFnGetStructured,
          cDailyPriority: window.deferredFnDailyPriority,
          cNote: window.deferredFnNote,
          dataFromService: function(profileService) {
            return profileService.promise;
          },
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('main', {
        url: '/main',
        templateUrl: 'product/page/main/main-tmpl.html',
        controller: CtrlMain,
        resolve: {
          cComfortChallenge: window.deferredFncComfortChallenge,
          cGuide: window.deferredFncGuide,
          cGetStructured: window.deferredFnGetStructured,
          cDailyPriority: window.deferredFnDailyPriority,
          cNote: window.deferredFnNote,
          cUserGoal: window.deferredFnUserGoal,
          dataFromService: function(profileService) {
            return profileService.promise;
          },
          cTrackRecord: window.deferredFnTrackRecord,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('daily', {
        url: '/daily',
        templateUrl: 'product/page/daily/daily-tmpl.html',
        controller: CtrlDaily,
        resolve: {
          cDailyPriority: window.deferredFnDailyPriority,
          cNote: window.deferredFnNote,
          cComfortChallenge: window.deferredFncComfortChallenge,
          cUserGoal: window.deferredFnUserGoal,
          cGetStructured: window.deferredFnGetStructured,
          cTrackRecord: window.deferredFnTrackRecord,
          dataFromService: function(profileService) {
            return profileService.promise;
          },
          time: function (timeService) {
            return timeService.promise;
          }
        }
      })

      .state('landing', {
        url: '/landing',
        templateUrl: 'product/page/landing/landing-tmpl.html',
        controller: CtrlLanding,
        resolve: {
          cUserGoal: window.deferredFnUserGoal,
          time: function (timeService) {
            return timeService.promise;
          }
        }
      });
  }

  config.$inject=['$stateProvider', '$urlRouterProvider'];

  return config;
});